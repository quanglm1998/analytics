/*********************************************************
 * File       : analytics.h
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Tue 28 Feb 2017 10:11:27 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_H
#define DTL_ANALYTICS_H

#define ANALYTICS_VERSION_MARJOR 1
#define ANALYTICS_VERSION_MINOR 0
#define ANALYTICS_VERSION_PATCH 0

#include "analytics/common.h"
#include "analytics/ml.h"
#include "analytics/dt.h"
#include "analytics/fm.h"
#include "analytics/gbt.h"
#include "analytics/hmm.h"
#include "analytics/knn.h"
#include "analytics/lasso.h"
#include "analytics/lsr.h"
#include "analytics/mlp.h"
#include "analytics/rr.h"
#include "analytics/svr.h"

#endif
