/*********************************************************
 * File       : common.h
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 02 Mar 2017 11:43:32 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_COMMON_H
#define DTL_ANALYTICS_COMMON_H

#include <exception>
#include <string>
#include <sstream>

#include "analytics/def.h"
#include "analytics/logging.h"
#include "analytics/random.h"

namespace analytics {

// Common exception class for error reporting
class AnalyticsError : public std::exception {
 public:
  explicit AnalyticsError(const std::string &msg) : msg_(msg) {}

  const char *what() const noexcept {
    return msg_.c_str();
  }

 private:
  std::string msg_;
};

#define ANALYTICS_THROW(message) { \
  std::ostringstream oss; \
  oss << message; \
  throw AnalyticsError(oss.str()); \
}

}

#endif
