#ifndef DTL_ANALYTICS_SVR_H
#define DTL_ANALYTICS_SVR_H

#include <assert.h>
#include <memory>
#include <string>
#include <unordered_map>
#include <random>
#include <utility>
#include <algorithm>
#include "analytics/ml.h"
#include "analytics/random.h"

namespace analytics {

template <typename real = float>
class SVR : public Regressor<real> {
 public:
  SVR()
      : type_("l1"),
        C_(0.1),
        p_(0.01),
        eps_(0.001),
        num_iterations_(10),
        gen(1) {}

  SVR(std::string type, real C, real p, real eps, Mat<real> w0, int m, int l,
      int iter, int num_iterations, int active_size, ivec index, Col<real> w,
      real d, real G, real H, real gmax_old, real gmax_new, real gnorm1_new,
      real gnorm1_init, Col<real> beta, Col<real> qd, real lambda,
      real upper_bound, Random gen) : type_(type), C_(C), p_(p), eps_(eps),
      w0_(w0), m_(m), l_(l), iter_(iter), num_iterations_(num_iterations),
      active_size_(active_size), index_(index), w_(w), d_(d), G_(G), H_(H),
      gmax_old_(gmax_old), gmax_new_(gmax_new), gnorm1_new_(gnorm1_new),
      gnorm1_init_(gnorm1_init), beta_(beta), qd_(qd), lambda_(lambda),
      upper_bound_(upper_bound), gen(gen) {}

  void set_C(real C) {
    C_ = C;
  }
  void set_p(real p) {
    p_ = p;
  }
  void set_eps(real eps) {
    eps_ = eps;
  }
  void set_num_iterations(int num_iterations) {
    num_iterations_ = num_iterations;
  }
  const Mat<real> &weight() const {
    return w0_;
  }

  std::string type() const { return type_; }
  real C() const { return C_; }
  real p() const { return p_; }
  real eps() const { return eps_; }
  Mat<real> w0() const { return w0_; }
  int m() const { return m_; }
  int l() const { return l_; }
  int iter() const { return iter_; }
  int num_iterations() const { return num_iterations_; }
  int active_size() const { return active_size_; }
  ivec index() const { return index_; }
  Col<real> w() const { return w_; }
  real d() const { return d_; }
  real G() const { return G_; }
  real H() const { return H_; }
  real gmax_old() const { return gmax_old_; }
  real gmax_new() const { return gmax_new_; }
  real gnorm1_new() const { return gnorm1_new_; }
  real gnorm1_init() const { return gnorm1_init_; }
  Col<real> beta() const { return beta_; }
  Col<real> qd() const { return qd_; }
  real lambda() const { return lambda_; }
  real upper_bound() const { return upper_bound_; }

 protected:
  void FitImpl(const Mat<real> &train, const Mat<real> &targets) override;

  void PredictImpl(const Mat<real> &samples, Mat<real> &results) override;

 private:
  std::string type_;  // type_ = "l1" or "l2"
  real C_;
  real p_;
  real eps_;
  Mat<real> w0_;
  int m_;
  int l_;
  int iter_;  // current iter_num
  int num_iterations_;  // max iter_num
  int active_size_;
  ivec index_;
  Col<real> w_;
  real d_;
  real G_;
  real H_;
  real gmax_old_;
  real gmax_new_;
  real gnorm1_new_;
  real gnorm1_init_;
  Col<real> beta_;
  Col<real> qd_;
  real lambda_;
  real upper_bound_;
  Random gen;

  int RngNext(int s) {
    return gen.randi(s);
  }

  void BuildOne(const Mat<real> &train, const Mat<real> &targets);
};
}
#endif
