#ifndef DTL_ANALYTICS_GBT_H
#define DTL_ANALYTICS_GBT_H
#include <assert.h>
#include <vector>
#include <unordered_map>
#include <random>
#include <set>
#include <utility>
#include <algorithm>
#include <functional>
#include "analytics/ml.h"
#include "analytics/random.h"

namespace analytics {

template <typename real>
class GradientBoostingTreeRegressor : public Regressor<real> {
  struct SNode {
    int pid;
    real vle;
    int left_id;
    SNode()
      : pid(-1), vle(0), left_id(-1) {}
    SNode(int pid, real vle, int left_id)
      : pid(pid), vle(vle), left_id(left_id) {}
  };

 public :
  GradientBoostingTreeRegressor() :
      shrinkage_(0.01),
      max_level_(5),
      gamma_(0.01),
      lambda_(0.01),
      min_ratio_(0.1),
      null_zero_(0),
      num_trees_(10),
      fp_(0.1),
      fp_level_(1),
      y_mean_(0.0),
      verbose_(false) { }

  void set_shrinkage(real shrinkage) {
    shrinkage_ = shrinkage;
  }

  real shrinkage() const {
    return shrinkage_;
  }

  void set_max_level(int max_level) {
    if (max_level > 12) {
      ANALYTICS_THROW("max_level should not be large than 12");
    }
    max_level_ = max_level;
  }

  int max_level() const {
    return max_level_;
  }

  void set_gamma(real gamma) {
    gamma_ = gamma;
  }

  real gamma() const {
    return gamma_;
  }

  void set_lambda(real lambda) {
    lambda_ = lambda;
  }

  real lambda() const {
    return lambda_;
  }

  void set_null_zero(int null_zero) {
    null_zero_ = null_zero;
  }

  int null_zero() const {
    return null_zero_;
  }

  void set_num_trees(int num_trees) {
    num_trees_ = num_trees;
  }

  int num_trees() const {
    return num_trees_;
  }

  void set_fp(real fp) {
    assert(fp > 0 && fp <= 1.0);
    fp_ = fp;
  }

  real fp() const {
    return fp_;
  }

  void set_fp_level(int fp_level) {
    assert(fp_level >= 0);
    fp_level_ = fp_level;
  }

  int fp_level() const {
    return fp_level_;
  }

  void set_verbose(bool verbose) {
    verbose_ = verbose;
  }

  bool verbose() const {
    return verbose_;
  }

  void set_seed(int seed) {
    gen = Random(seed);
  }

 protected:
  void FitImpl(const Mat<real> &trains, const Mat<real> &targets) override;

  void PredictImpl(const Mat<real> &samples, Mat<real> &results) {
    results = Mat<real>(samples.n_rows, 1);
    for (uword i = 0; i < samples.n_rows; ++i) {
      results(i, 0) = y_mean_;
      const Row<real> sample_row = samples.row(i);
      for (uword j = 0; j < forest_.size(); ++j) {
        results(i, 0) += PredictOne(forest_[j], sample_row) * shrinkage_;
      }
    }
  }

  real PredictOne(const std::vector<SNode> &tree, const Row<real> &features) {
    if (tree.size() == 0) return 0.0;
    int now_node = 0;
    while (tree[now_node].pid >= 0) {
      real cv = tree[now_node].vle;
      int dim = tree[now_node].pid;
      if (features[dim] <= cv) {
        now_node = tree[now_node].left_id;
      } else {
        now_node = tree[now_node].left_id + 1;
      }
    }
    return tree[now_node].vle;
  }

  void BuildOne(const Mat<real> &trains);

 private :
  int RngNext(int s) {
    return gen.randi(s);
  }

  std::vector<real> x_;  // train x_
  std::vector<real> y_;  // train y_
  std::vector<int> pos_;  // pos_ for each dime
  std::vector<real> pos_value_;
  std::vector<int> pos_value_offset_;
  std::vector<int> dim_sz_;
  std::vector<real> global_best_gain_;  // size:tree * (2^(max_level_ + 1) - 1)
  std::vector<real> global_best_v_;  // size:tree * (2^(max_level_ + 1) - 1)
  std::vector<real> global_best_lw_;  // size:tree * (2^(max_level_ + 1) - 1)
  std::vector<real> global_best_rw_;  // size:tree * (2^(max_level_ + 1) - 1)
  std::vector<int> global_best_dim_;  // size:tree * (2^(max_level_ + 1) - 1)
  std::vector<real> curpred_;  // size:sz_, current prediction
  std::vector<real> g_;  // size:sz_
  std::vector<real> h_;  // size:sz_
  std::vector<int> dim_nid_;  // size:sz_ * dim
  std::vector<int> is_last_;  // size:sz_ * dim_
  std::vector<int> tpos_cache_;  // size:sz_ * dim
  std::vector<real> best_gain_;  // size:dim_ * (2^(max_level_ + 1) - 1)
  std::vector<real> best_v_;  // size:dim_ each dim_ has one value
  std::vector<real> best_lw_;  // size:dim_ each dim_ has one value
  std::vector<real> best_rw_;  // size:dim_ each dim_ has one value
  std::vector<int> best_d_;  // size:dim_ * (2^(max_level_ + 1) - 1)
  std::vector<int> best_sz_;  // size:dim_ each dim_ has one value
  std::vector<real> global_all_g_;  // size:dim_ * (2^(max_level_ + 1) - 1)
  std::vector<real> global_all_h_;  // size:dim_ * (2^(max_level_ + 1) - 1)
  std::vector<real> global_tmp_g_;  // size:dim_ * (2^(max_level_ + 1) - 1)
  std::vector<real> global_tmp_h_;  // size:dim_ * (2^(max_level_ + 1) - 1)
  int sz_;  // sample sz_
  int dim_;  // feature num
  real shrinkage_;
  int max_level_;
  real gamma_;
  real lambda_;
  real min_ratio_;
  int null_zero_;
  int num_trees_;
  real fp_;  // around fp_ percent of feature will be considered in depth [0, fp_level_]
  int fp_level_;  // in depth [0, fp_level_], will choose random feature
  real y_mean_;
  bool verbose_;
  std::vector<std::vector<SNode>> forest_;
  Random gen;
};

}

#endif
