/*********************************************************
 * File       : random.h
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 02 Mar 2017 11:42:17 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_RANDOM_H
#define DTL_ANALYTICS_RANDOM_H

#include <algorithm>
#include <limits>
#include <random>
#include <utility>
#include <vector>

#include "analytics/def.h"

namespace analytics {

class Random {
 public:
  typedef std::mt19937_64::result_type seed_type;

  Random() : Random(0) {}
  explicit Random(int seed) : engine_(seed), i_distr_(0, std::numeric_limits<int>::max() - 1) {}

  seed_type seed() const {
    return seed_;
  }

  void set_seed(seed_type v) {
    seed_ = v;
    engine_.seed(v);

    i_distr_.reset();
    u_distr_.reset();
    n_distr_.reset();
  }

  // Gets a random int from a uniform distribution on [0, INT_MAX)
  int randi() {
    return i_distr_(engine_);
  }

  // Gets a random int from a uniform distribution on [0, max)
  int randi(int max) {
    return randi() % max;
  }

  // Gets a random int from a uniform distribution on [min, max)
  int randi(int min, int max) {
    return min + randi(max - min);
  }

  // Gets a random double from a uniform distribution on [0, 1)
  double randu() {
    return u_distr_(engine_);
  }

  // Gets a random double from a uniform distribution on [0, max)
  double randu(double max) {
    return randu() * max;
  }

  // Gets a random double from a uniform distribution on [0, max)
  double randu(double min, double max) {
    return min + randu() * (max - min);
  }

  // Gets a random double from the standard normal distribution N(0, 1)
  double randn() {
    return n_distr_(engine_);
  }

  // Gets a random double from N(mean, stddev ^ 2)
  double randn(double mean, double stddev) {
    return n_distr_(engine_) * stddev + mean;
  }

  // Fills a matrix with random integer values
  template <typename T>
  void randi(Mat<T> &m, T min = 0, T max = std::numeric_limits<T>::max()) {
    std::uniform_int_distribution<T> distr(min, max - 1);
    RandFill(m.memptr(), m.n_rows * m.n_cols, distr);
  }

  // Fills a matrix with random floating point values
  template <typename T>
  void randu(Mat<T> &m, T min = 0, T max = 1) {
    std::uniform_real_distribution<T> distr(min, max);
    RandFill(m.memptr(), m.n_rows * m.n_cols, distr);
  }

  // Fills a matrix with random values from the standard normal distribution
  template <typename T>
  void randn(Mat<T> &m, T mean = 0.0, T stddev = 1.0) {
    std::normal_distribution<T> distr(mean, stddev);
    RandFill(m.memptr(), m.n_rows * m.n_cols, distr);
  }

  // Shuffles a matrix
  //
  // dim = 0: shuffle elements in each column
  // dim = 1: shuffle elements in each row
  template <typename T>
  void shuffle(const Mat<T> &X, Mat<T> &out, uword dim = 0);

  // Shuffles a row vector
  template <typename T>
  void shuffle(const Row<T> &X, Row<T> &out) {
    shuffle(X, out, 1);
  }

  // Shuffles a matrix in place
  //
  // dim = 0: shuffle elements in each column
  // dim = 1: shuffle elements in each row
  template <typename T>
  void shuffle(Mat<T> &m, uword dim = 0) {
    shuffle(m, m, dim);
  }

  // Shuffles a row vector in place
  template <typename T>
  void shuffle(Row<T> &m) {
    shuffle(m, m);
  }

  // Generates a shuffled sequence from 0 to (n - 1)
  Col<uword> shuffled_seq(int n) {
    Col<uword> seq = arma::linspace<Col<uword>>(0, n - 1, n);
    shuffle(seq);
    return seq;
  }

 private:
  seed_type seed_;

  std::mt19937_64 engine_;

  // [0, std::numeric_limits<int>::max())
  std::uniform_int_distribution<int> i_distr_;

  // [0, 1)
  std::uniform_real_distribution<double> u_distr_;

  // mean = 0, stddev = 1.0
  std::normal_distribution<double> n_distr_;

  template <typename T, typename Distr>
  void RandFill(T *mem, size_t N, Distr &distr) {
    for (size_t i = 0; i < N; i++) {
      mem[i] = T(distr(engine_));
    }
  }
};

// Shuffle function copied from Armadillo
template <typename T>
void Random::shuffle(const Mat<T> &X, Mat<T> &out, uword dim) {
  if (X.is_empty()) {
    out.copy_size(X);
    return;
  }

  const uword N = (dim == 0) ? X.n_rows : X.n_cols;
  const bool is_alias = (&out == &X);
  std::vector<arma::arma_sort_index_packet<int>> packet_vec(N);

  for (uword i = 0; i < N; ++i) {
    packet_vec[i].val = randi();
    packet_vec[i].index = i;
  }

  arma::arma_sort_index_helper_ascend<int> comparator;

  std::sort(packet_vec.begin(), packet_vec.end(), comparator);
  if (!X.is_vec()) {
    if (!is_alias) {
      out.copy_size(X);

      if (dim == 0) {
        for (uword i = 0; i < N; ++i) { out.row(i) = X.row(packet_vec[i].index); }
      } else {
        for (uword i = 0; i < N; ++i) { out.col(i) = X.col(packet_vec[i].index); }
      }
    } else {  // in-place shuffle
      // reuse the val member variable of packet_vec
      // to indicate whether a particular row or column
      // has already been shuffled

      for (uword i = 0; i < N; ++i) {
        packet_vec[i].val = 0;
      }

      if (dim == 0) {
        for (uword i = 0; i < N; ++i) {
          if (packet_vec[i].val == 0) {
            const uword j = packet_vec[i].index;
            out.swap_rows(i, j);
            packet_vec[j].val = 1;
          }
        }
      } else {
        for (uword i = 0; i < N; ++i) {
          if (packet_vec[i].val == 0) {
            const uword j = packet_vec[i].index;
            out.swap_cols(i, j);
            packet_vec[j].val = 1;
          }
        }
      }
    }
  } else {  // we're dealing with a vector
    if (!is_alias) {
      out.copy_size(X);

      if (dim == 0) {
        if (X.n_rows > 1) {  // i.e. column vector
          for (uword i=0; i < N; ++i) { out[i] = X[packet_vec[i].index]; }
        } else {
          out = X;
        }
      } else {
        if (X.n_cols > 1) {  // i.e. row vector
          for (uword i = 0; i < N; ++i) { out[i] = X[packet_vec[i].index]; }
        } else {
          out = X;
        }
      }
    } else {  // in-place shuffle
      // reuse the val member variable of packet_vec
      // to indicate whether a particular row or column
      // has already been shuffled

      for (uword i = 0; i < N; ++i) {
        packet_vec[i].val = 0;
      }

      if (dim == 0) {
        if (X.n_rows > 1) {  // i.e. column vector
          for (uword i = 0; i < N; ++i) {
            if (packet_vec[i].val == 0) {
              const uword j = packet_vec[i].index;
              std::swap(out[i], out[j]);
              packet_vec[j].val = 1;
            }
          }
        }
      } else {
        if (X.n_cols > 1) {  // i.e. row vector
          for (uword i = 0; i < N; ++i) {
            if (packet_vec[i].val == 0) {
              const uword j = packet_vec[i].index;
              std::swap(out[i], out[j]);
              packet_vec[j].val = 1;
            }
          }
        }
      }
    }
  }
}

}
#endif
