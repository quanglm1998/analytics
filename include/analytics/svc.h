#ifndef DTL_ANALYTICS_SVC_H
#define DTL_ANALYTICS_SVC_H

/*********************************************************
 * File       : svc.h
 * Author     : yliu
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 07 Jul 2017 05:50:03 PM SGT
 * Revision   : none
 *
 *********************************************************/


#include <assert.h>
#include <memory>
#include <string>
#include <random>
#include <utility>
#include <algorithm>
#include "analytics/ml.h"
#include "analytics/random.h"

namespace analytics {
template <typename real = float>
class SVMClassifier : public Classifier<real, uword> {
 public:
  SVMClassifier()
      : type_("l2"),
        eps_(0.001),
        Cp_(0.1),
        Cn_(0.1),
        num_iterations_(100),
        gen(1) {}
  void set_type(const std::string &type) {
    type_ = type;
  }
  void set_eps(real eps) {
    eps_ = eps;
  }
  void set_Cp(real Cp) {
    Cp_ = Cp;
  }
  void set_Cn(real Cn) {
    Cn_ = Cn;
  }
  void set_num_iterations(int num_iterations) {
    num_iterations_ = num_iterations;
  }
  const Mat<real> &weight() {
    return w_;
  }

 protected:
  int RngNext(int s) {
    return gen.randi(s);
  }
  void FitImpl(const Mat<real> &train, const Mat<uword> &targets) override;
  void PredictImpl(const Mat<real> &samples, Mat<uword> &results) override;

 private:
  std::string type_;  // type_ = "l2" or "l1". defaut solver type l2: L2R_L2LOSS_SVC_DUAL. l1: L2R_L1LOSS_SVC_DUAL
  real eps_;  // stopping tolerance
  real Cp_;
  real Cn_;
  int l_;  // number of samples
  int w_size_;  // number of features
  int num_iterations_;  // number of interations
  Random gen;
  Col<real> w_;  // weights
};
}

#endif
