#ifndef DTL_ANALYTICS_MLP_H
#define DTL_ANALYTICS_MLP_H

#include <assert.h>
#include <string>
#include <vector>
#include <algorithm>
#include "analytics/ml.h"
#include "analytics/random.h"

namespace analytics {

template <typename real>
class MLPRegressor : public Regressor<real> {
 public:
  MLPRegressor():
      output_function_("sigm"),
      activation_function_("relu"),
      learning_rate_(0.00001),
      min_learning_rate_(0.000001),
      batch_normalization_(1),
      epoch_(5),
      batch_sz_(100),
      weight_penalty_l2_(0),
      epsilon_(1e-10),
      momentum_(0.8),
      scaling_learning_rate_(0.9),
      layer_sz_(-1),
      cat_sz_(-1) { }

  const std::string &output_function() const {
    return output_function_;
  }

  void set_output_function(const std::string &output_function) {
    output_function_ = output_function;
  }

  const std::string &activation_function() const {
    return activation_function_;
  }

  void set_activation_function(const std::string &activation_function) {
    activation_function_ = activation_function;
  }

  real min_learning_rate() const {
    return min_learning_rate_;
  }

  void set_min_learning_rate(real min_learning_rate) {
    min_learning_rate_ = min_learning_rate;
  }

  real learning_rate() const {
    return learning_rate_;
  }

  void set_learning_rate(real learning_rate) {
    learning_rate_ = learning_rate;
  }

  bool batch_normalization() const {
    return batch_normalization_;
  }

  void set_batch_normalization(bool batch_normalization) {
    batch_normalization_ = batch_normalization;
  }

  int epoch() const {
    return epoch_;
  }

  void set_epoch(int epoch) {
    epoch_ = epoch;
  }

  int batch_sz() const {
    return batch_sz_;
  }

  void set_batch_sz(int batch_sz) {
    batch_sz_ = batch_sz;
  }

  real weight_penalty_l2() const {
    return weight_penalty_l2_;
  }

  void set_weight_penalty_l2(real weight_penalty_l2) {
    weight_penalty_l2_ = weight_penalty_l2;
  }

  int momentum() const {
    return momentum_;
  }

  void set_momentum(int momentum) {
    momentum_ = momentum;
  }

  const std::vector<uword>& layer() const {
    return layer_;
  }

  void set_layer(const std::vector<uword> &layer) {
    layer_ = layer;
  }

  void FitImpl(const Mat<real> &x, const Mat<real> &y) override;

  void PredictImpl(const Mat<real> &x, Mat<real> &y);

  void Clear() {
    relu_da_.clear();
    relu_va_.clear();
    for (auto &it : cache_a_) it.clear();
    for (auto &it : cache_dw_) it.clear();
    for (auto &it : cache_vw_) it.clear();
    for (auto &it : cache_rw_) it.clear();
    for (auto &it : cache_apre_) it.clear();
    for (auto &it : cache_ahat_) it.clear();
    for (auto &it : vbn_) it.clear();
    for (auto &it : rbn_) it.clear();
    for (auto &it : dbn_) it.clear();
    for (auto &it : d_sigma_) it.clear();
  }

 private:
  void Init();

  void BuildOne(Mat<real> &batch_x, Mat<real> &batch_y);

  Mat<real> Append(const Mat<real> &x) {
    Mat<real> res = arma::ones<Mat<real>>(x.n_rows, x.n_cols + 1);
    res.submat(0, 1, x.n_rows - 1, x.n_cols) = x;
    return res;
  }
  Mat<real> Bsxfun(const Mat<real> &a, const Row<real> &b, char c) {
    if (c == '-') {
      return a.each_row() - b;
    } else if (c == '*') {
      return a.each_row() % b;  // a * b actually
    } else if (c == '+') {
      return a.each_row() + b;
    }
    return a;
  }

  Row<real> SumMul(Mat<real> &a, Mat<real> &b) {
    Row<real> res = arma::zeros<Row<real>>(a.n_cols);
    for (uword i = 0; i < a.n_cols; ++i) {
      res[i] = arma::sum(a.col(i) % b.col(i));
    }
    return res;
  }

  Row<real> Mean2(const Mat<real> &a) {
    Row<real> res = arma::zeros<Row<real>>(a.n_cols);
    for (uword i = 0; i < a.n_cols; ++i) {
      res[i] = arma::sum(a.col(i) % a.col(i)) / a.n_rows;
    }
    return res;
  }

  Mat<real> SigmDot(const Mat<real> &a) {
    Mat<real> res = a;
    res.transform([](real val) { return val * (1.0 - val); });
    return res;
  }

  Mat<real> Sigm(const Mat<real> &a) {
    Mat<real> res = a;
    res.transform( [](real val) { return 1.0 / (1.0 + exp(-val)); } );
    return res;
  }

  Mat<real> Max(const Mat<real> &x, real v) {
    Mat<real> res = x;
    res.transform( [v](real val) { return (val < v) ? v : val; } );
    return res;
  }

  void ApplyGrads();

  void Backward();

  void Forward(const Mat<real> &x, Mat<real> &batch_y, bool is_testing);

  std::vector<uword> layer_;
  std::string output_function_;
  std::string activation_function_;
  real learning_rate_;
  real min_learning_rate_;
  bool batch_normalization_;
  int epoch_;
  int batch_sz_;
  real weight_penalty_l2_;
  real epsilon_;
  real momentum_;
  real scaling_learning_rate_;
  int layer_sz_;
  int cat_sz_;
  Row<real> relu_ra_;
  Row<real> relu_da_;
  Row<real> relu_va_;
  std::vector<Mat<real>> cache_a_;
  std::vector<Mat<real>> cache_w_;
  std::vector<Mat<real>> cache_dw_;
  std::vector<Mat<real>> cache_vw_;
  std::vector<Mat<real>> cache_rw_;
  std::vector<Mat<real>> cache_apre_;
  std::vector<Mat<real>> cache_ahat_;
  std::vector<Row<real>>  sigma2_;
  std::vector<Row<real>>  gamma_;
  std::vector<Row<real>>  mu_;
  Mat<real>  x_mu_;
  std::vector<Row<real>>  beta_;
  std::vector<Row<real>>  vbn_;
  std::vector<Row<real>>  rbn_;
  std::vector<Row<real>>  mean_sigma2_;
  std::vector<Row<real>>  mean_mu_;
  Mat<real>  e_;
  std::vector<Row<real>>  dbn_;
  std::vector<Row<real>>  d_sigma_;
};
}
#endif
