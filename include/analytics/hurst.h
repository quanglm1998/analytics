/*********************************************************
 * File       : hurst.h
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 21 Mar 2019 09:42:31 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_HURST_H
#define DTL_ANALYTICS_HURST_H

#include "analytics/common.h"

namespace analytics {

template <typename real = float>
class HurstExponent {
 public:
  static HurstExponent &Instance() {
    static HurstExponent instance;
    return instance;
  }

  static real compute(const Col<real> &series,
                      size_t min_interval_num = 2,
                      size_t min_interval_len = 5);

 private:
  static real regress(const Col<real> &x, const Col<real> &y);
  HurstExponent() {}
};

}

#endif
