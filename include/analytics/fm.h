/*********************************************************
 * File       : fm.h
 * Author     : yliu
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 27 Feb 2017 04:04:08 PM SGT
 * Revision   : none
 *
 *********************************************************/
#ifndef DTL_ANALYTICS_FM_H
#define DTL_ANALYTICS_FM_H

#include <random>
#include <algorithm>
#include "analytics/ml.h"
#include "analytics/random.h"

// paper link: http://www.csie.ntu.edu.tw/~b97053/paper/Rendle2010FM.pdf
namespace analytics {
template <typename real = float>
class FM : public Regressor<real> {
 public:
  real max() const {
    return max_value_;
  }
  void set_max(real max) {
    max_value_ = max;
  }

  real min() const {
    return min_value_;
  }
  void set_min(real min) {
    min_value_ = min;
  }

  int p() const {
    return p_;
  }
  void set_p(int p) {
    p_ = p;
  }

  int k() const {
    return k_;
  }
  void set_k(int k) {
    k_ = k;
  }

  real stddev() const {
    return stddev_;
  }
  void set_stddev(real stddev) {
    stddev_ = stddev;
  }

  real learn_rate() const {
    return learn_rate_;
  }
  void set_learn_rate(real learn_rate) {
    learn_rate_ = learn_rate;
  }

  real reg0() const {
    return reg0_;
  }
  void set_reg0(real reg) {
    reg0_ = reg;
  }
  real reg1() const {
    return regw_;
  }
  void set_reg1(real reg) {
    regw_ = reg;
  }
  real reg2() const {
    return regv_;
  }
  void set_reg2(real reg) {
    regv_ = reg;
  }
  void set_reg(real reg) {
    reg0_ = reg;
    regw_ = reg;
    regv_ = reg;
  }
  void set_reg(real r0, real r1, real r2) {
    reg0_ = r0;
    regw_ = r1;
    regv_ = r2;
  }

  int num_iterations() const {
    return num_iter_;
  }
  void set_num_iterations(int num_iter) {
    num_iter_ = num_iter;
  }

  int rng_seed() const {
    return seed_;
  }
  void set_rng_seed(int seed) {
    seed_ = seed;
    rng_.set_seed(seed_);
  }

  real w0() const {
    return w0_;
  }
  const Col<real> &w1() const {
    return w_;
  }
  Mat<real> w2() const {
    return u_ * v_.t();
  }

  // Initiation of the Factorizaton Machine model, setting parameters
  void Initialize(int p, int k, real stddev, real learn_rate, real reg, int num_iter, int seed = 0) {
    seed_ = seed;
    rng_ = Random(seed_);

    p_ = p;
    k_ = k;
    stddev_ = stddev;
    learn_rate_ = learn_rate;
    reg0_ = reg;
    regw_ = reg;
    regv_ = reg;
    num_iter_ = num_iter;

    max_value_ = 1e20;
    min_value_ = -1e20;
    w0_ = 0.0;
    w_.zeros(p_);

    u_.resize(p_, k_);
    v_.resize(p_, k_);
    rng_.randn<real>(u_, 0.0, stddev_);
    rng_.randn<real>(v_, 0.0, stddev_);

    sumuk_.zeros(k_);
    sumvk_.zeros(k_);
  }

  // default parameters
  FM() {
    Initialize(3, 2, 1, 0.00004, 0.0, 15000, 0);
  }
  // Initiation of the Factorizaton Machine model, setting parameters
  FM(int p, int kk, real stddev, real learn_rate, real reg, int num_iter, int seed = 0) {
    Initialize(p, kk, stddev, learn_rate, reg, num_iter, seed);
  }

  // Predict for the first sample/row
  real PredictOne(const Row<real> &x, Col<real> &s1, Col<real> &s2);

  // Predict for the first sample/row
  real PredictOne(const Row<real> &x);

  // Predict for all samples/rows, write the targets in y
  void PredictAll(const Mat<real> &x, Mat<real> &y);

  // Fit the first sample/row
  void LearnOne(const Row<real> &x, real y);

  // Fit the all samples/rows
  void LearnAll(const Mat<real>& x, const Mat<real>& y);

 protected:
  void FitImpl(const Mat<real> &samples, const Mat<real> &results) override {
    LearnAll(samples, results);
  }

  void PredictImpl(const Mat<real> &samples, Mat<real> &results) override {
    PredictAll(samples, results);
  }

 private:
  int seed_;  // seed for random number generator
  Random rng_;  // random generator

  int p_;  // number of features
  int k_;  // the second order inner dimension, rank k_ << p_

  real max_value_;  // maximum value
  real min_value_;  // minimum value

  real stddev_;  // Initialize second order weights v_ ~ N(i, stddev_^2), normal distribution of weights
  int num_iter_;  // number of iterations
  real reg0_, regw_, regv_;  // regularization parameters
  real learn_rate_;  // learning rate

  real w0_;  // const term weight
  Col<real> w_;  // 1-order weights vector
  Mat<real> u_;  // 2-order weights matrix
  Mat<real> v_;  // 2-order weights matrix
  Col<real> sumvk_;  // sum of vij*xi, for inner calculation
  Col<real> sumuk_;  // sum of uij*xi, for inner calculation
};
}

#endif
