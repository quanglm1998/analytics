/*********************************************************
 * File       : emd.h
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Sun 05 Aug 2018 11:52:39 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_EMD_H
#define DTL_ANALYTICS_EMD_H

#include "analytics/common.h"

namespace analytics {

template <typename real = float>
class EmpiricalModeDecomposition {
 public:
  struct EmdData {
    int order, size, min_size, max_size;
    Row<int> min_point, max_point;
    Row<real> min_curve, max_curve, residue;
    Mat<real> imfs;

    EmdData() : order(0), size(0), min_size(0), max_size(0) {}
    explicit EmdData(int in_order, int in_size = 0)
      : order(in_order), size(in_size), min_size(0), max_size(0)
    {}

    void Init(int in_order, int in_size = 0) {
      order = in_order;
      size = in_size;
    }

    void Resize(int in_size) {
      size = in_size;
      min_point.set_size(size);
      max_point.set_size(size);
      residue.set_size(size);
      min_curve.set_size(size);
      max_curve.set_size(size);
      imfs.set_size(order, size);
    }

    void Clear() {
      min_point.clear();
      max_point.clear();
      residue.clear();
      min_curve.clear();
      max_curve.clear();
      imfs.clear();
    }
  };

 public:
  EmpiricalModeDecomposition()
    : data_(), order_(0), max_iter_(0), locality_(0)
  {}

  EmpiricalModeDecomposition(int max_iter, int order, int locality)
    : data_(order), order_(order), max_iter_(max_iter), locality_(locality)
  {}

  void Init(int order, int max_iter, int locality = 0);
  void Decompose(const Row<real> &signal);

  const Mat<real>& Imf() const;
  Row<real> Imf(int i) const;

 protected:
  void MakeExtrema(const Row<real>&);
  void Interpolate(const Row<real>&, Row<real>&, const Row<int>&, int);
  void UpdateImf(Row<real>&);
  void MakeResidue(const Row<real>&);

  int MirrorIndex(int i, int size) {
    if (i < size) {
      return (i < 0) ? - i - 1 : i;
    } else {
      return (size - 1) + (size - i);
    }
  }

 private:
  EmdData data_;
  int order_;
  int max_iter_;
  int locality_;
};

}

#endif
