/*********************************************************
 * File       : analytics_lsr.h
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 23 Feb 2017 03:55:04 PM
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_LSR_H
#define DTL_ANALYTICS_LSR_H

#include "analytics/ml.h"

namespace analytics {

template <typename real = float>
class LeastSquareRegressor : public Regressor<real> {
 public:
  LeastSquareRegressor() : has_inter_(true), success_(false) {}

  void set_intercept(bool has_inter) { has_inter_ = has_inter; }

  const Mat<real> &coefficients() const { return coeff_; }
  const Row<real> &intercepts() const { return inter_; }

 protected:
  void FitImpl(const Mat<real> &trains, const Mat<real> &target) override;
  void PredictImpl(const Mat<real> &samples, Mat<real> &results) override;

 private:
  Mat<real> coeff_;  // coefficients or exposures
  Row<real> inter_;  // intercept
  bool has_inter_, success_;
};

}

#endif
