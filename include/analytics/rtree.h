#ifndef DTL_ANALYTICS_RTREE_H
#define DTL_ANALYTICS_RTREE_H

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <queue>
#include <iostream>
#include <memory>
#include <vector>

#include <algorithm>


//
// RTree.h
//

#define RTREE_TEMPLATE template<class DATATYPE, class ELEMTYPE, class ELEMTYPEREAL>
#define RTREE_QUAL RTree<DATATYPE, ELEMTYPE, ELEMTYPEREAL>




template<class DATATYPE, class ELEMTYPE,
         class ELEMTYPEREAL = ELEMTYPE>
class RTree {
 protected:
  struct Node;  // Fwd decl.  Used by other internal structs and iterator

 public:
  RTree(int nd, int maxnodes, int minnodes);
  virtual ~RTree();

  /// Insert entry
  /// \param a_min Min of bounding rect
  /// \param a_max Max of bounding rect
  /// \param a_dataId Positive Id of data.  Maybe zero, but negative numbers not allowed.
  void Insert(const std::vector<ELEMTYPE>& a_min, const std::vector<ELEMTYPE>& a_max, const DATATYPE& a_dataId);

  /// Remove entry
  /// \param a_min Min of bounding rect
  /// \param a_max Max of bounding rect
  /// \param a_dataId Positive Id of data.  Maybe zero, but negative numbers not allowed.
  void Remove(const std::vector<ELEMTYPE>& a_min, const std::vector<ELEMTYPE>& a_max, const DATATYPE& a_dataId);
  std::vector<DATATYPE> FindTopK(const std::vector<ELEMTYPE>& a_pos, int k);


  /// Remove all entries from tree
  void RemoveAll();

 protected:
  int numdis;
  int maxnodes;
  int minnodes;
  /// Minimal bounding rectangle (n-dimensional)
  struct Rect {
    int rnumdis;
    std::vector<ELEMTYPE> m_min;                      ///< Min dimensions of bounding box
    std::vector<ELEMTYPE> m_max;                      ///< Max dimensions of bounding box
    explicit Rect(int rd): rnumdis(rd), m_min(rd), m_max(rd) {
    }
  };

  /// May be data or may be another subtree
  /// The parents level determines this.
  /// If the parents level is 0, then this is data
  struct Branch {
    int bnumdis;
    Rect m_rect;                                  ///< Bounds
    std::shared_ptr<Node> m_child;                                ///< Child node
    DATATYPE m_data;                              ///< Data Id
    explicit Branch(int bd):bnumdis(bd), m_rect(bd) {
    }
  };

  struct pqData {
    float dis;
    std::shared_ptr<Node> m_node;
    DATATYPE m_data;
  };
  class pqData_comp {
   public:
    bool operator() (const pqData& b1, const pqData& b2) const {
      return b1.dis > b2.dis;
    }
  };

  /// Node for each branch level
  struct Node {
    bool IsInternalNode()                         { return (m_level > 0); }   // Not a leaf, but a internal node
    bool IsLeaf()                                 { return (m_level == 0); }   // A leaf, contains data

    int m_count;                                  ///< Count
    int m_level;                                  ///< Leaf is zero, others positive
    std::vector<Branch> m_branch;
    Node(int bd, int mn):m_branch(mn, Branch(bd)) {
    }
  };

  /// A link list of nodes for reinsertion after a delete operation
  struct ListNode {
    std::shared_ptr<ListNode> m_next;                             ///< Next in list
    std::shared_ptr<Node> m_node;                                 ///< Node
  };

  /// Variables for finding a split partition
  struct PartitionVars {
    enum { NOT_TAKEN = -1 };  // indicates that position

    std::vector<Branch> m_branchBuf;
    std::vector<Rect> m_cover;
    std::vector<int> m_partition;
    int m_total;
    int m_minFill;
    int m_count[2];
    /* Rect m_cover[2]; */
    ELEMTYPEREAL m_area[2];

    int m_branchCount;
    Rect m_coverSplit;
    ELEMTYPEREAL m_coverSplitArea;
    PartitionVars(int nd, int mn) : m_branchBuf(mn+1, Branch(nd)), m_cover(2, Rect(nd)),
                   m_partition(mn+1), m_coverSplit(nd)  {  // numdis, maxnodes
    }
  };

  std::shared_ptr<Node> AllocNode();
  void FreeNode(std::shared_ptr<Node> a_node);
  void InitNode(std::shared_ptr<Node> a_node);
  void InitRect(Rect* a_rect);
  bool InsertRectRec(const Branch& a_branch, std::shared_ptr<Node> a_node,
                     std::shared_ptr<Node>* a_newNode, int a_level);
  bool InsertRect(const Branch& a_branch, std::shared_ptr<Node>* a_root, int a_level);
  Rect NodeCover(std::shared_ptr<Node> a_node);
  bool AddBranch(const Branch* a_branch, std::shared_ptr<Node> a_node, std::shared_ptr<Node>* a_newNode);
  void DisconnectBranch(std::shared_ptr<Node> a_node, int a_index);
  int PickBranch(const Rect* a_rect, std::shared_ptr<Node> a_node);
  Rect CombineRect(const Rect* a_rectA, const Rect* a_rectB);
  void SplitNode(std::shared_ptr<Node> a_node, const Branch* a_branch, std::shared_ptr<Node>* a_newNode);
  ELEMTYPEREAL RectVolume(Rect* a_rect);
  ELEMTYPEREAL CalcRectVolume(Rect* a_rect);
  ELEMTYPE minDis(const std::vector<ELEMTYPE>& a_pos, const Rect* a_rect);
  void GetBranches(std::shared_ptr<Node> a_node, const Branch* a_branch, PartitionVars* a_parVars);
  void ChoosePartition(PartitionVars* a_parVars, int a_minFill);
  void LoadNodes(std::shared_ptr<Node> a_nodeA, std::shared_ptr<Node> a_nodeB, PartitionVars* a_parVars);
  void InitParVars(PartitionVars* a_parVars, int a_maxRects, int a_minFill);
  void PickSeeds(PartitionVars* a_parVars);
  void Classify(int a_index, int a_group, PartitionVars* a_parVars);
  bool RemoveRect(Rect* a_rect, const DATATYPE& a_id, std::shared_ptr<Node>* a_root);
  bool RemoveRectRec(Rect* a_rect, const DATATYPE& a_id, std::shared_ptr<Node> a_node,
                     std::shared_ptr<ListNode>* a_listNode);
  std::shared_ptr<ListNode> AllocListNode();
  void FreeListNode(std::shared_ptr<ListNode> a_listNode);
  bool Overlap(Rect* a_rectA, Rect* a_rectB);
  void ReInsert(std::shared_ptr<Node> a_node, std::shared_ptr<ListNode>* a_listNode);
  /* bool Search(Node* a_node, Rect* a_rect, int& a_foundCount, t_resultCallback a_resultCallback, void* a_context); */
  void RemoveAllRec(std::shared_ptr<Node> a_node);
  void Reset();

  std::shared_ptr<Node> m_root;                                    ///< Root of tree
};



RTREE_TEMPLATE
RTREE_QUAL::RTree(int nd, int maxn, int minn) {
  assert(maxn > minn);
  assert(minn > 0);

  maxnodes = maxn;
  minnodes = minn;
  numdis = nd;


  m_root = AllocNode();
  m_root->m_level = 0;
}


RTREE_TEMPLATE
RTREE_QUAL::~RTree() {
  /* std::cout << "free here for dis " << numdis << "\n"; */
  Reset();  // Free, or reset node memory
}


RTREE_TEMPLATE
void RTREE_QUAL::Insert(const std::vector<ELEMTYPE>& a_min, const std::vector<ELEMTYPE>& a_max,
                        const DATATYPE& a_dataId) {
#ifdef _DEBUG
  for (int index = 0; index < numdis; ++index) {
    assert(a_min[index] <= a_max[index]);
  }
#endif  // _DEBUG

  Branch branch(numdis);
  branch.m_data = a_dataId;
  branch.m_child = NULL;

  for (int axis = 0; axis < numdis; ++axis) {
    branch.m_rect.m_min[axis] = a_min[axis];
    branch.m_rect.m_max[axis] = a_max[axis];
  }

  InsertRect(branch, &m_root, 0);
}


RTREE_TEMPLATE
void RTREE_QUAL::Remove(const std::vector<ELEMTYPE>& a_min, const std::vector<ELEMTYPE>& a_max,
                        const DATATYPE& a_dataId) {
#ifdef _DEBUG
  for (int index = 0; index < numdis; ++index) {
    assert(a_min[index] <= a_max[index]);
  }
#endif  // _DEBUG

  Rect rect(numdis);

  for (int axis = 0; axis < numdis; ++axis) {
    rect.m_min[axis] = a_min[axis];
    rect.m_max[axis] = a_max[axis];
  }

  RemoveRect(&rect, a_dataId, &m_root);
}

// Calculate the minimum distance with the rect
RTREE_TEMPLATE
ELEMTYPE RTREE_QUAL::minDis(const std::vector<ELEMTYPE>& a_pos, const Rect* a_rect) {
  assert(a_rect);

  ELEMTYPE finaldis = (ELEMTYPE)0;

  for (int index = 0; index < numdis; ++index) {
    ELEMTYPE disi = 0;
    if (a_pos[index] < a_rect->m_min[index]) {
      disi = a_rect->m_min[index]-a_pos[index];
    } else if (a_pos[index] > a_rect->m_max[index]) {
      disi = a_pos[index]-a_rect->m_max[index];
    }
    finaldis += disi*disi;
  }
  finaldis = sqrt(finaldis);

  assert(finaldis >= (ELEMTYPE)0);

  return finaldis;
}

// return the nearest k
RTREE_TEMPLATE
std::vector<DATATYPE> RTREE_QUAL::FindTopK(const std::vector<ELEMTYPE>& a_pos, int k)  {
  pqData tmppqdata;
  tmppqdata.dis = 0;
  tmppqdata.m_node = m_root;
  std::priority_queue<pqData, std::vector<pqData>, pqData_comp> pq;
  pq.push(tmppqdata);

  std::vector<DATATYPE> answer;
  std::vector<ELEMTYPE> ansdis;
  int foundanswer = 0;
  while (!pq.empty()) {
    if (foundanswer >= k) break;
    pqData mpqd = pq.top();
    pq.pop();
    if (mpqd.m_node == NULL) {
      answer.push_back(mpqd.m_data);
      ansdis.push_back(mpqd.dis);
      foundanswer += 1;
      /* std::cout << mpqd.m_data << "\t" << mpqd.dis << std::endl; */
    } else {
      if (mpqd.m_node->IsLeaf()) {
        for (int index = 0; index < mpqd.m_node->m_count; index++) {
          Branch& tmpbra = mpqd.m_node->m_branch[index];
          tmppqdata.m_node = NULL;
          tmppqdata.m_data = tmpbra.m_data;
          tmppqdata.dis = minDis(a_pos, &(tmpbra.m_rect));
          pq.push(tmppqdata);
        }
      } else {
        for (int index = 0; index < mpqd.m_node->m_count; index++) {
          Branch& tmpbra = mpqd.m_node->m_branch[index];
          tmppqdata.m_node = tmpbra.m_child;;
          tmppqdata.dis = minDis(a_pos, &(tmpbra.m_rect));
          pq.push(tmppqdata);
        }
      }
    }
  }
  return answer;
}


RTREE_TEMPLATE
void RTREE_QUAL::RemoveAll() {
  // Delete all existing nodes
  Reset();

  m_root = AllocNode();
  m_root->m_level = 0;
}


RTREE_TEMPLATE
void RTREE_QUAL::Reset() {
#ifdef RTREE_DONT_USE_MEMPOOLS
  // Delete all existing nodes
  RemoveAllRec(m_root);
#else  // RTREE_DONT_USE_MEMPOOLS
  // Just reset memory pools.  We are not using complex types
  // EXAMPLE
#endif  // RTREE_DONT_USE_MEMPOOLS
}


RTREE_TEMPLATE
void RTREE_QUAL::RemoveAllRec(std::shared_ptr<Node> a_node) {
  assert(a_node);
  assert(a_node->m_level >= 0);

  if (a_node->IsInternalNode()) {
    // This is an internal node in the tree
    for (int index = 0; index < a_node->m_count; ++index) {
      RemoveAllRec(a_node->m_branch[index].m_child);
    }
  }
  FreeNode(a_node);
}


RTREE_TEMPLATE
std::shared_ptr<typename RTREE_QUAL::Node> RTREE_QUAL::AllocNode() {
  std::shared_ptr<Node> newNode(new Node(numdis, maxnodes));
#ifdef RTREE_DONT_USE_MEMPOOLS
  /* newNode = new Node; */
#else  // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif  // RTREE_DONT_USE_MEMPOOLS
  InitNode(newNode);
  return newNode;
}


RTREE_TEMPLATE
void RTREE_QUAL::FreeNode(std::shared_ptr<Node> a_node) {
  assert(a_node);

#ifdef RTREE_DONT_USE_MEMPOOLS
  a_node.reset();
  /* delete a_node; */
#else  // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif  // RTREE_DONT_USE_MEMPOOLS
}


// Allocate space for a node in the list used in DeletRect to
// store Nodes that are too empty.
RTREE_TEMPLATE
std::shared_ptr<typename RTREE_QUAL::ListNode> RTREE_QUAL::AllocListNode() {
#ifdef RTREE_DONT_USE_MEMPOOLS
  return std::shared_ptr<ListNode>(new ListNode);
#else  // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif  // RTREE_DONT_USE_MEMPOOLS
}


RTREE_TEMPLATE
void RTREE_QUAL::FreeListNode(std::shared_ptr<ListNode> a_listNode) {
#ifdef RTREE_DONT_USE_MEMPOOLS
  /* delete a_listNode; */
  a_listNode.reset();
#else  // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif  // RTREE_DONT_USE_MEMPOOLS
}


RTREE_TEMPLATE
void RTREE_QUAL::InitNode(std::shared_ptr<Node> a_node) {
  a_node->m_count = 0;
  a_node->m_level = -1;
}


RTREE_TEMPLATE
void RTREE_QUAL::InitRect(Rect* a_rect) {
  for (int index = 0; index < numdis; ++index) {
    a_rect->m_min[index] = (ELEMTYPE)0;
    a_rect->m_max[index] = (ELEMTYPE)0;
  }
}


// Inserts a new data rectangle into the index structure.
// Recursively descends tree, propagates splits back up.
// Returns 0 if node was not split.  Old node updated.
// If node was split, returns 1 and sets the pointer pointed to by
// new_node to point to the new node.  Old node updated to become one of two.
// The level argument specifies the number of steps up from the leaf
// level to insert; e.g. a data rectangle goes in at level = 0.
RTREE_TEMPLATE
bool RTREE_QUAL::InsertRectRec(const Branch& a_branch, std::shared_ptr<Node> a_node,
                               std::shared_ptr<Node>* a_newNode, int a_level) {
  assert(a_node && a_newNode);
  assert(a_level >= 0 && a_level <= a_node->m_level);

  // recurse until we reach the correct level for the new record. data records
  // will always be called with a_level == 0 (leaf)
  if (a_node->m_level > a_level) {
    // Still above level for insertion, go down tree recursively
    std::shared_ptr<Node> otherNode;

    // find the optimal branch for this record
    int index = PickBranch(&a_branch.m_rect, a_node);

    // recursively insert this record into the picked branch
    bool childWasSplit = InsertRectRec(a_branch, a_node->m_branch[index].m_child, &otherNode, a_level);

    if (!childWasSplit) {
      // Child was not split. Merge the bounding box of the new record with the
      // existing bounding box
      a_node->m_branch[index].m_rect = CombineRect(&a_branch.m_rect, &(a_node->m_branch[index].m_rect));
      return false;
    } else {
      // Child was split. The old branches are now re-partitioned to two nodes
      // so we have to re-calculate the bounding boxes of each node
      a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
      Branch branch(numdis);
      branch.m_child = otherNode;
      branch.m_rect = NodeCover(otherNode);

      // The old node is already a child of a_node. Now add the newly-created
      // node to a_node as well. a_node might be split because of that.
      return AddBranch(&branch, a_node, a_newNode);
    }
  } else if (a_node->m_level == a_level) {
    // We have reached level for insertion. Add rect, split if necessary
    return AddBranch(&a_branch, a_node, a_newNode);
  } else {
    // Should never occur
    assert(0);
    return false;
  }
}


// Insert a data rectangle into an index structure.
// InsertRect provides for splitting the root;
// returns 1 if root was split, 0 if it was not.
// The level argument specifies the number of steps up from the leaf
// level to insert; e.g. a data rectangle goes in at level = 0.
// InsertRect2 does the recursion.
//
RTREE_TEMPLATE
bool RTREE_QUAL::InsertRect(const Branch& a_branch, std::shared_ptr<Node>* a_root, int a_level) {
  assert(a_root);
  assert(a_level >= 0 && a_level <= (*a_root)->m_level);
#ifdef _DEBUG
  for (int index = 0; index < numdis; ++index) {
    assert(a_branch.m_rect.m_min[index] <= a_branch.m_rect.m_max[index]);
  }
#endif  // _DEBUG

  std::shared_ptr<Node> newNode;
  if (InsertRectRec(a_branch, *a_root, &newNode, a_level))  {
    // Root split
    // Grow tree taller and new root
    std::shared_ptr<Node> newRoot = AllocNode();
    newRoot->m_level = (*a_root)->m_level + 1;

    Branch branch(numdis);

    // add old root node as a child of the new root
    branch.m_rect = NodeCover(*a_root);
    branch.m_child = *a_root;
    AddBranch(&branch, newRoot, NULL);

    // add the split node as a child of the new root
    branch.m_rect = NodeCover(newNode);
    branch.m_child = newNode;
    AddBranch(&branch, newRoot, NULL);

    // set the new root as the root node
    *a_root = newRoot;

    return true;
  }

  return false;
}


// Find the smallest rectangle that includes all rectangles in branches of a node.
RTREE_TEMPLATE
typename RTREE_QUAL::Rect RTREE_QUAL::NodeCover(std::shared_ptr<Node> a_node) {
  assert(a_node);

  Rect rect = a_node->m_branch[0].m_rect;
  for (int index = 1; index < a_node->m_count; ++index) {
     rect = CombineRect(&rect, &(a_node->m_branch[index].m_rect));
  }

  return rect;
}


// Add a branch to a node.  Split the node if necessary.
// Returns 0 if node not split.  Old node updated.
// Returns 1 if node split, sets *new_node to address of new node.
// Old node updated, becomes one of two.
RTREE_TEMPLATE
bool RTREE_QUAL::AddBranch(const Branch* a_branch, std::shared_ptr<Node> a_node, std::shared_ptr<Node>* a_newNode) {
  assert(a_branch);
  assert(a_node);

  if (a_node->m_count < maxnodes) {
    // split won't be necessary
    a_node->m_branch[a_node->m_count] = *a_branch;
    ++a_node->m_count;

    return false;
  } else {
    assert(a_newNode);

    SplitNode(a_node, a_branch, a_newNode);
    return true;
  }
}


// Disconnect a dependent node.
// Caller must return (or stop using iteration index) after this as count has changed
RTREE_TEMPLATE
void RTREE_QUAL::DisconnectBranch(std::shared_ptr<Node> a_node, int a_index) {
  assert(a_node && (a_index >= 0) && (a_index < maxnodes));
  assert(a_node->m_count > 0);

  // Remove element by swapping with the last element to prevent gaps in array
  a_node->m_branch[a_index] = a_node->m_branch[a_node->m_count - 1];

  --a_node->m_count;
}


// Pick a branch.  Pick the one that will need the smallest increase
// in area to accomodate the new rectangle.  This will result in the
// least total area for the covering rectangles in the current node.
// In case of a tie, pick the one which was smaller before, to get
// the best resolution when searching.
RTREE_TEMPLATE
int RTREE_QUAL::PickBranch(const Rect* a_rect, std::shared_ptr<Node> a_node) {
  assert(a_rect && a_node);

  bool firstTime = true;
  ELEMTYPEREAL increase;
  ELEMTYPEREAL bestIncr = (ELEMTYPEREAL)-1;
  ELEMTYPEREAL area;
  ELEMTYPEREAL bestArea;
  int best = 0;
  Rect tempRect(numdis);

  for (int index = 0; index < a_node->m_count; ++index) {
    Rect* curRect = &a_node->m_branch[index].m_rect;
    area = CalcRectVolume(curRect);
    tempRect = CombineRect(a_rect, curRect);
    increase = CalcRectVolume(&tempRect) - area;
    if ((increase < bestIncr) || firstTime) {
      best = index;
      bestArea = area;
      bestIncr = increase;
      firstTime = false;
    } else if ((increase == bestIncr) && (area < bestArea)) {
      best = index;
      bestArea = area;
      bestIncr = increase;
    }
  }
  return best;
}


// Combine two rectangles into larger one containing both
RTREE_TEMPLATE
typename RTREE_QUAL::Rect RTREE_QUAL::CombineRect(const Rect* a_rectA, const Rect* a_rectB) {
  assert(a_rectA && a_rectB);

  Rect newRect(numdis);

  for (int index = 0; index < numdis; ++index) {
    newRect.m_min[index] = std::min(a_rectA->m_min[index], a_rectB->m_min[index]);
    newRect.m_max[index] = std::max(a_rectA->m_max[index], a_rectB->m_max[index]);
  }

  return newRect;
}



// Split a node.
// Divides the nodes branches and the extra one between two nodes.
// Old node is one of the new ones, and one really new one is created.
// Tries more than one method for choosing a partition, uses best result.
RTREE_TEMPLATE
void RTREE_QUAL::SplitNode(std::shared_ptr<Node> a_node, const Branch* a_branch, std::shared_ptr<Node>* a_newNode) {
  assert(a_node);
  assert(a_branch);

  // Could just use local here, but member or external is faster since it is reused
  PartitionVars localVars(numdis, maxnodes);
  PartitionVars* parVars = &localVars;

  // Load all the branches into a buffer, initialize old node
  GetBranches(a_node, a_branch, parVars);

  // Find partition
  ChoosePartition(parVars, minnodes);

  // Create a new node to hold (about) half of the branches
  *a_newNode = AllocNode();
  (*a_newNode)->m_level = a_node->m_level;

  // Put branches from buffer into 2 nodes according to the chosen partition
  a_node->m_count = 0;
  LoadNodes(a_node, *a_newNode, parVars);

  assert((a_node->m_count + (*a_newNode)->m_count) == parVars->m_total);
}


// Calculate the n-dimensional volume of a rectangle
RTREE_TEMPLATE
ELEMTYPEREAL RTREE_QUAL::RectVolume(Rect* a_rect) {
  assert(a_rect);

  ELEMTYPEREAL volume = (ELEMTYPEREAL)1;

  for (int index = 0; index < numdis; ++index) {
    volume += log(a_rect->m_max[index] - a_rect->m_min[index]+1);
  }

  assert(volume >= (ELEMTYPEREAL)0);

  return volume;
}

// Use one of the methods to calculate retangle volume
RTREE_TEMPLATE
ELEMTYPEREAL RTREE_QUAL::CalcRectVolume(Rect* a_rect) {
  return RectVolume(a_rect);  // Faster but can cause poor merges
}


// Load branch buffer with branches from full node plus the extra branch.
RTREE_TEMPLATE
void RTREE_QUAL::GetBranches(std::shared_ptr<Node> a_node, const Branch* a_branch, PartitionVars* a_parVars) {
  assert(a_node);
  assert(a_branch);

  assert(a_node->m_count == maxnodes);

  // Load the branch buffer
  for (int index = 0; index < maxnodes; ++index) {
    a_parVars->m_branchBuf[index] = a_node->m_branch[index];
  }
  a_parVars->m_branchBuf[maxnodes] = *a_branch;
  a_parVars->m_branchCount = maxnodes + 1;

  // Calculate rect containing all in the set
  a_parVars->m_coverSplit = a_parVars->m_branchBuf[0].m_rect;
  for (int index=1; index < maxnodes+1; ++index) {
    a_parVars->m_coverSplit = CombineRect(&a_parVars->m_coverSplit, &a_parVars->m_branchBuf[index].m_rect);
  }
  a_parVars->m_coverSplitArea = CalcRectVolume(&a_parVars->m_coverSplit);
}


// Method #0 for choosing a partition:
// As the seeds for the two groups, pick the two rects that would waste the
// most area if covered by a single rectangle, i.e. evidently the worst pair
// to have in the same group.
// Of the remaining, one at a time is chosen to be put in one of the two groups.
// The one chosen is the one with the greatest difference in area expansion
// depending on which group - the rect most strongly attracted to one group
// and repelled from the other.
// If one group gets too full (more would force other group to violate min
// fill requirement) then other group gets the rest.
// These last are the ones that can go in either group most easily.
RTREE_TEMPLATE
void RTREE_QUAL::ChoosePartition(PartitionVars* a_parVars, int a_minFill) {
  assert(a_parVars);

  ELEMTYPEREAL biggestDiff;
  int group = 0, chosen = 0, betterGroup = 0;

  InitParVars(a_parVars, a_parVars->m_branchCount, a_minFill);
  PickSeeds(a_parVars);

  while (((a_parVars->m_count[0] + a_parVars->m_count[1]) < a_parVars->m_total)
       && (a_parVars->m_count[0] < (a_parVars->m_total - a_parVars->m_minFill))
       && (a_parVars->m_count[1] < (a_parVars->m_total - a_parVars->m_minFill))) {
    biggestDiff = (ELEMTYPEREAL) -1;
    for (int index = 0; index < a_parVars->m_total; ++index) {
      if (PartitionVars::NOT_TAKEN == a_parVars->m_partition[index]) {
        Rect* curRect = &a_parVars->m_branchBuf[index].m_rect;
        Rect rect0 = CombineRect(curRect, &a_parVars->m_cover[0]);
        Rect rect1 = CombineRect(curRect, &a_parVars->m_cover[1]);
        ELEMTYPEREAL growth0 = CalcRectVolume(&rect0) - a_parVars->m_area[0];
        ELEMTYPEREAL growth1 = CalcRectVolume(&rect1) - a_parVars->m_area[1];
        ELEMTYPEREAL diff = growth1 - growth0;
        if (diff >= 0) {
          group = 0;
        } else {
          group = 1;
          diff = -diff;
        }

        if (diff > biggestDiff) {
          biggestDiff = diff;
          chosen = index;
          betterGroup = group;
        } else if ((diff == biggestDiff) && (a_parVars->m_count[group] < a_parVars->m_count[betterGroup])) {
          chosen = index;
          betterGroup = group;
        }
      }
    }
    Classify(chosen, betterGroup, a_parVars);
  }

  // If one group too full, put remaining rects in the other
  if ((a_parVars->m_count[0] + a_parVars->m_count[1]) < a_parVars->m_total) {
    if (a_parVars->m_count[0] >= a_parVars->m_total - a_parVars->m_minFill) {
      group = 1;
    } else {
      group = 0;
    }
    for (int index = 0; index < a_parVars->m_total; ++index) {
      if (PartitionVars::NOT_TAKEN == a_parVars->m_partition[index]) {
        Classify(index, group, a_parVars);
      }
    }
  }

  assert((a_parVars->m_count[0] + a_parVars->m_count[1]) == a_parVars->m_total);
  assert((a_parVars->m_count[0] >= a_parVars->m_minFill) &&
        (a_parVars->m_count[1] >= a_parVars->m_minFill));
}


// Copy branches from the buffer into two nodes according to the partition.
RTREE_TEMPLATE
void RTREE_QUAL::LoadNodes(std::shared_ptr<Node> a_nodeA, std::shared_ptr<Node> a_nodeB, PartitionVars* a_parVars) {
  assert(a_nodeA);
  assert(a_nodeB);
  assert(a_parVars);

  for (int index = 0; index < a_parVars->m_total; ++index) {
    assert(a_parVars->m_partition[index] == 0 || a_parVars->m_partition[index] == 1);

    int targetNodeIndex = a_parVars->m_partition[index];
    std::shared_ptr<Node> targetNodes[] = {a_nodeA, a_nodeB};

    // It is assured that AddBranch here will not cause a node split.
    bool nodeWasSplit = AddBranch(&a_parVars->m_branchBuf[index], targetNodes[targetNodeIndex], NULL);
    if (nodeWasSplit) {
      assert(!nodeWasSplit);
    }
  }
}


// Initialize a PartitionVars structure.
RTREE_TEMPLATE
void RTREE_QUAL::InitParVars(PartitionVars* a_parVars, int a_maxRects, int a_minFill) {
  assert(a_parVars);

  a_parVars->m_count[0] = a_parVars->m_count[1] = 0;
  a_parVars->m_area[0] = a_parVars->m_area[1] = (ELEMTYPEREAL)0;
  a_parVars->m_total = a_maxRects;
  a_parVars->m_minFill = a_minFill;
  for (int index = 0; index < a_maxRects; ++index) {
    a_parVars->m_partition[index] = PartitionVars::NOT_TAKEN;
  }
}


RTREE_TEMPLATE
void RTREE_QUAL::PickSeeds(PartitionVars* a_parVars) {
  int seed0 = 0, seed1 = 1;
  ELEMTYPEREAL worst, waste;
  /* ELEMTYPEREAL area[maxnodes+1]; */
  std::vector<ELEMTYPEREAL> area(maxnodes+1);

  for (int index = 0; index < a_parVars->m_total; ++index) {
    area[index] = CalcRectVolume(&a_parVars->m_branchBuf[index].m_rect);
  }

  worst = -a_parVars->m_coverSplitArea - 1;
  for (int indexA = 0; indexA < a_parVars->m_total-1; ++indexA) {
    for (int indexB = indexA+1; indexB < a_parVars->m_total; ++indexB) {
      Rect oneRect = CombineRect(&a_parVars->m_branchBuf[indexA].m_rect, &a_parVars->m_branchBuf[indexB].m_rect);
      waste = CalcRectVolume(&oneRect) - area[indexA] - area[indexB];
      if (waste > worst) {
        worst = waste;
        seed0 = indexA;
        seed1 = indexB;
      }
    }
  }

  Classify(seed0, 0, a_parVars);
  Classify(seed1, 1, a_parVars);
}


// Put a branch in one of the groups.
RTREE_TEMPLATE
void RTREE_QUAL::Classify(int a_index, int a_group, PartitionVars* a_parVars) {
  assert(a_parVars);
  assert(PartitionVars::NOT_TAKEN == a_parVars->m_partition[a_index]);

  a_parVars->m_partition[a_index] = a_group;

  // Calculate combined rect
  if (a_parVars->m_count[a_group] == 0) {
    a_parVars->m_cover[a_group] = a_parVars->m_branchBuf[a_index].m_rect;
  } else {
    a_parVars->m_cover[a_group] = CombineRect(&a_parVars->m_branchBuf[a_index].m_rect, &a_parVars->m_cover[a_group]);
  }

  // Calculate volume of combined rect
  a_parVars->m_area[a_group] = CalcRectVolume(&a_parVars->m_cover[a_group]);

  ++a_parVars->m_count[a_group];
}


// Delete a data rectangle from an index structure.
// Pass in a pointer to a Rect, the tid of the record, ptr to ptr to root node.
// Returns 1 if record not found, 0 if success.
// RemoveRect provides for eliminating the root.
RTREE_TEMPLATE
bool RTREE_QUAL::RemoveRect(Rect* a_rect, const DATATYPE& a_id, std::shared_ptr<Node>* a_root) {
  assert(a_rect && a_root);
  assert(*a_root);

  std::shared_ptr<ListNode> reInsertList(nullptr);

  if (!RemoveRectRec(a_rect, a_id, *a_root, &reInsertList)) {
    // Found and deleted a data item
    // Reinsert any branches from eliminated nodes
    while (reInsertList) {
      std::shared_ptr<Node> tempNode = reInsertList->m_node;

      for (int index = 0; index < tempNode->m_count; ++index) {
        InsertRect(tempNode->m_branch[index],
                   a_root,
                   tempNode->m_level);
      }

      std::shared_ptr<ListNode> remLNode = reInsertList;
      reInsertList = reInsertList->m_next;

      FreeNode(remLNode->m_node);
      FreeListNode(remLNode);
    }

    // Check for redundant root (not leaf, 1 child) and eliminate TODO replace
    // if with while? In case there is a whole branch of redundant roots...
    if ((*a_root)->m_count == 1 && (*a_root)->IsInternalNode()) {
      std::shared_ptr<Node> tempNode = (*a_root)->m_branch[0].m_child;

      assert(tempNode);
      FreeNode(*a_root);
      *a_root = tempNode;
    }
    return false;
  } else {
    return true;
  }
}


// Delete a rectangle from non-root part of an index structure.
// Called by RemoveRect.  Descends tree recursively,
// merges branches on the way back up.
// Returns 1 if record not found, 0 if success.
RTREE_TEMPLATE
bool RTREE_QUAL::RemoveRectRec(Rect* a_rect, const DATATYPE& a_id, std::shared_ptr<Node> a_node,
                               std::shared_ptr<ListNode>* a_listNode) {
  assert(a_rect && a_node && a_listNode);
  assert(a_node->m_level >= 0);

  if (a_node->IsInternalNode())  {
    for (int index = 0; index < a_node->m_count; ++index) {
      if (Overlap(a_rect, &(a_node->m_branch[index].m_rect))) {
        if (!RemoveRectRec(a_rect, a_id, a_node->m_branch[index].m_child, a_listNode)) {
          if (a_node->m_branch[index].m_child->m_count >= minnodes) {
            // child removed, just resize parent rect
            a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
          } else {
            // child removed, not enough entries in node, eliminate node
            ReInsert(a_node->m_branch[index].m_child, a_listNode);
            DisconnectBranch(a_node, index);  // Must return after this call as count has changed
          }
          return false;
        }
      }
    }
    return true;
  } else {
    for (int index = 0; index < a_node->m_count; ++index) {
      if (a_node->m_branch[index].m_data == a_id) {
        DisconnectBranch(a_node, index);  // Must return after this call as count has changed
        return false;
      }
    }
    return true;
  }
}


// Decide whether two rectangles overlap.
RTREE_TEMPLATE
bool RTREE_QUAL::Overlap(Rect* a_rectA, Rect* a_rectB) {
  assert(a_rectA && a_rectB);

  for (int index = 0; index < numdis; ++index) {
    if (a_rectA->m_min[index] > a_rectB->m_max[index] ||
        a_rectB->m_min[index] > a_rectA->m_max[index]) {
      return false;
    }
  }
  return true;
}


// Add a node to the reinsertion list.  All its branches will later
// be reinserted into the index structure.
RTREE_TEMPLATE
void RTREE_QUAL::ReInsert(std::shared_ptr<Node> a_node, std::shared_ptr<ListNode>* a_listNode) {
  std::shared_ptr<ListNode> newListNode;

  newListNode = AllocListNode();
  newListNode->m_node = a_node;
  newListNode->m_next = *a_listNode;
  *a_listNode = newListNode;
}



#undef RTREE_TEMPLATE
#undef RTREE_QUAL

#endif  // RTREE_H

