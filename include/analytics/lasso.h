/*********************************************************
 * File       : lasso.h
 * Author     : jhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 27 Feb 2017 04:13:11 PM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_LASSO_H
#define DTL_ANALYTICS_LASSO_H


#include <iostream>
#include <algorithm>
#include "analytics/logging.h"
#include "analytics/ml.h"

namespace analytics {
template <typename real = float>
class Lasso : public Regressor<real> {
 public:
  Lasso() : max_iter_(20000), has_inter_(true), tol_(0.0001), lambda_(0.1), success_(false) {
  }

  const Mat<real>& coefficients() const { return coeff_; }
  const Row<real>& intercepts() const { return inter_; }

  bool has_intercept() const { return has_inter_; }
  real lambda() const { return lambda_; }
  int max_iter() const {return max_iter_; }
  real tol() const { return tol_; }

  void set_intercept(bool has_inter) { has_inter_ = has_inter; }
  void set_lambda(real lambda) { lambda_ = lambda; }
  void set_max_iter(int max_iter) { max_iter_ = max_iter; }
  void set_tol(real tol) { tol_ = tol; }

 protected:
  void FitImpl(const Mat<real> &x, const Mat<real> &y) override;

  void PredictImpl(const Mat<real> &samples, Mat<real> &results) override;

 private:
  int max_iter_;
  bool has_inter_;
  real tol_;
  real lambda_;
  bool success_;
  Mat<real> coeff_;
  Row<real> inter_;
};

}
#endif
