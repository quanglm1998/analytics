/*********************************************************
 * File       : analytics_logging.h
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 24 Feb 2017 05:12:30 PM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_LOGGING_H
#define DTL_ANALYTICS_LOGGING_H

#include <log4cxx/logger.h>

#include <string>

namespace analytics {

extern log4cxx::LoggerPtr logger;

#define ANALYTICS_LOG_TRACE_INLINE(msg) LOG4CXX_TRACE(analytics::logger, msg)
#define ANALYTICS_LOG_DEBUG_INLINE(msg) LOG4CXX_DEBUG(analytics::logger, msg)
#define ANALYTICS_LOG_INFO_INLINE(msg) LOG4CXX_INFO(analytics::logger, msg)
#define ANALYTICS_LOG_WARN_INLINE(msg) LOG4CXX_WARN(analytics::logger, msg)
#define ANALYTICS_LOG_ERROR_INLINE(msg) LOG4CXX_ERROR(analytics::logger, msg)
#define ANALYTICS_LOG_FATAL_INLINE(msg) LOG4CXX_FATAL(analytics::logger, msg)

#define ANALYTICS_LOG_TRACE(msg) ANALYTICS_LOG_TRACE_INLINE(msg << '\n')
#define ANALYTICS_LOG_DEBUG(msg) ANALYTICS_LOG_DEBUG_INLINE(msg << '\n')
#define ANALYTICS_LOG_INFO(msg) ANALYTICS_LOG_INFO_INLINE(msg << '\n')
#define ANALYTICS_LOG_WARN(msg) ANALYTICS_LOG_WARN_INLINE(msg << '\n')
#define ANALYTICS_LOG_ERROR(msg) ANALYTICS_LOG_ERROR_INLINE(msg << '\n')
#define ANALYTICS_LOG_FATAL(msg) ANALYTICS_LOG_FATAL_INLINE(msg << '\n')

void ConfigLogger(
    const std::string &name, const std::string &config_file, const std::string &level);


}
#endif
