/*********************************************************
 * File       : knn.h
 * Author     : jhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Wed 29 Mar 2017 09:33:54 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_KNN_H
#define DTL_ANALYTICS_KNN_H

#include <memory>
#include "analytics/logging.h"
#include "analytics/ml.h"
#include "analytics/rtree.h"


namespace analytics {
template <typename real = float>
class KNN : public Classifier<real> {
 public:
  /**
   * if numdis_=-1 means model haven't been trained
   * numdis_ can only be set through fit methos
   * **/
    int num_dis() {
      return numdis_;
    }

    int max_nodes() {
      return maxnodes_;
    }

    void set_max_nodes(int mn) {
      maxnodes_ = mn;
    }

    int min_nodes() {
      return minnodes_;
    }

    void set_min_nodes(int mn) {
      minnodes_ = mn;
    }

    void setTopk(int nm) {
      topk_ = nm;
    }

    int top_k() {
      return topk_;
    }

    KNN():maxnodes_(10), minnodes_(5), topk_(10), numdis_(-1) {
    }

 protected:
  void FitImpl(const Mat<real> &, const Mat<uword> &) override;

  void PredictImpl(const Mat<real> &samples, Mat<uword> &results) override;

 private:
    int maxnodes_;
    int minnodes_;
    int topk_;
    int numdis_;
    std::unique_ptr<RTree<int, float, float>> ntree_;
    Mat<uword> resp_;
};
}


#endif
