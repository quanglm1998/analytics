/*********************************************************
 * File       : analytics_hmm.h
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Tue 18 Apr 2017 11:43:00 AM
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_HMM_H
#define DTL_ANALYTICS_HMM_H

#include "analytics/common.h"

namespace analytics {

template <typename real, typename ObsType, typename HidType>
class HiddenMarkovModelBase {
 public:
  HiddenMarkovModelBase() : max_iter_(10000), tol_(1e-5) {}

  void set_max_iter(int max_iter) { max_iter_ = max_iter; }
  void set_tol(real tol) { tol_ = tol; }

  virtual bool Fit(const Mat<ObsType> &observ) = 0;
  virtual bool Decode(const Mat<ObsType> &observ, Mat<HidType> &hidden, bool is_fit = false) = 0;
  virtual bool Predict(const Mat<ObsType> &observ, Mat<HidType> &hidden,
                       Cube<real> &out_observ, Cube<real> &out_hidden,
                       int step = 1, bool is_decode = false, bool is_fit = false) = 0;

 protected:
  bool CheckMatrix(const Mat<real> &matrix, int row = 1, int col = 1);
  void ResetMatrix(Mat<real> &matrix, int row = 1, int col = 1);

 protected:
  int max_iter_;
  real tol_;
};

// Discretized version of HMM
template <typename real = float>
class HiddenMarkovModel : public HiddenMarkovModelBase<real, int, int> {
 public:
  HiddenMarkovModel()
    : HiddenMarkovModelBase<real, int, int>(), n_hid_state_(0), n_obs_state_(0) {}

  void set_transit_matrix(const Mat<real> &A) { A_ = A; }
  void set_emission_matrix(const Mat<real> &B) { B_ = B; }
  void set_init_distribution(const Row<real> &p) { p_ = p; }

  const Mat<real> &transit_matrix() const {return A_;}
  const Mat<real> &emission_matrix() const {return B_;}
  const Row<real> &init_distribution() const {return p_;}

  bool Fit(const Mat<int> &observ) override;
  bool Decode(const Mat<int> &observ, Mat<int> &hidden, bool is_fit = false) override;
  bool Predict(const Mat<int> &observ, Mat<int> &hidden,
               Cube<real> &out_observ, Cube<real> &out_hidden,
               int step = 1, bool is_decode = false, bool is_fit = false) override;

 protected:
  void ForwardBackwardProb(const Mat<int> &observ);
  void ConditionalProb();

  void InitParameters();
  void CheckEmissionMatrix();

  bool EstimateParameters(const Mat<int> &observ);
  bool EstimateHiddenSequence(const Mat<int> &observ, Mat<int> &hidden);

 private:
  int n_hid_state_, n_obs_state_;

  Mat<real> A_;
  Mat<real> B_;
  Row<real> p_;

  Cube<real> alpha_;
  Cube<real> beta_;
  Mat<real> c_;
  Row<real> log_prob_;
};

template <typename real = float>
class HiddenMarkovModelCont : public HiddenMarkovModelBase<real, real, int> {
 public:
  HiddenMarkovModelCont()
    : HiddenMarkovModelBase<real, real, int>(), n_hid_state_(0), n_obs_state_(0) {}

  void set_transit_matrix(const Mat<real> &A) { A_ = A; }
  void set_emission_mu(const Mat<real> &mu) { mu_ = mu; }  // (n_obs_state_ , n_hid_state_)
  void set_emission_sigma(const Mat<real> &sigma_2) { sigma_2_ = sigma_2; }
  void set_init_distribution(const Row<real> &p) { p_ = p; }

  const Mat<real> &transit_matrix() const {return A_;}
  const Mat<real> &emission_mu() const {return mu_;}
  const Mat<real> &emission_sigma() const {return sigma_2_;}
  const Row<real> &init_distribution() const {return p_;}
  const Mat<real> &log_likelihood() const {return log_likelihood_;}

  bool Fit(const Mat<real> &observ) override;
  bool Decode(const Mat<real> &observ, Mat<int> &hidden, bool is_fit = false) override;
  bool Predict(const Mat<real> &observ, Mat<int> &hidden,
               Cube<real> &out_observ, Cube<real> &out_hidden,
               int step = 1, bool is_decode = false, bool is_fit = false) override;

 protected:
  void GaussianProb(const Mat<real> &observ, const Mat<real> &mu, const Mat<real> &sigma_2);
  void ForwardBackwardProb(const Mat<real> &observ);
  void ConditionalProb();

  void InitParameters();
  void CheckEmissionMu();
  bool CheckEmissionSigma(const Mat<real> &matrix, int row = 1, int col = 1);
  void ResetMu(Mat<real> &matrix, int row = 1, int col = 1);
  void ResetSigma(Mat<real> &matrix, int row = 1, int col = 1);

  bool EstimateParameters(const Mat<real> &observ);
  bool EstimateHiddenSequence(const Mat<real> &observ, Mat<int> &hidden);

 private:
  int n_hid_state_, n_obs_state_;

  Mat<real> A_;
  Row<real> p_;
  Mat<real> mu_;
  Mat<real> sigma_2_;
  Mat<real> log_likelihood_;

  Cube<real> B_;
  Cube<real> alpha_;
  Cube<real> beta_;
  Mat<real> c_;
  Row<real> log_prob_;
};

}

#endif
