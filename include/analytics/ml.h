#ifndef DTL_ANALYTICS_ML_H
#define DTL_ANALYTICS_ML_H

#include <type_traits>

#include "analytics/common.h"

namespace analytics {

// TODO(myang): Add more validations
class Validator {
 public:
  template <template <class> class M, typename real, typename Out>
  static void ValidatesFitInput(const M<real> &train, const Mat<Out> &targets) {
    if (train.n_rows != targets.n_rows) {
      ANALYTICS_THROW("train.n_rows (" << train.n_rows << ") does not match with targets.n_rows (" <<
                      targets.n_rows << ")");
    }
    if (!train.is_finite()) {
      ANALYTICS_THROW("train should not contains NAN");
    }
    if (!targets.is_finite()) {
      ANALYTICS_THROW("targets should not contains NAN");
    }
  }

  template <template <class> class M, typename real>
  static void ValidatesPredictInput(const M<real> &samples) {
    if (!samples.is_finite()) {
      ANALYTICS_THROW("samples should not contains NAN");
    }
  }

  template <template <class> class M, typename real, typename Out>
  static void ValidatesPredictOutput(const M<real> &samples, const Mat<Out> &results) {
    if (samples.n_rows != results.n_rows) {
      ANALYTICS_THROW("samples.n_rows (" << samples.n_rows << ") does not match with results.n_rows (" <<
                      results.n_rows << ")");
    }
  }
};

template <typename real, typename Out>
class PredictionModel {
 public:
  virtual ~PredictionModel() {}

  // Trains the regressor with the given training data and targets.
  //
  // Input feature and target vectors are stored as rows.
  void Fit(const Mat<real> &train, const Mat<Out> &targets) {
    FitGeneric(train, targets);
  }

  // Predicts the results for the given samples.
  //
  // Input feature vectors and output result vectors are stored as rows.
  void Predict(const Mat<real> &samples, Mat<Out> &results) {
    PredictGeneric(samples, results);
  }
  Mat<Out> Predict(const Mat<real> &samples) {
    return PredictGeneric(samples);
  }

  // Sparse versions
  void Fit(const SpMat<real> &train, const Mat<Out> &targets) {
    FitGeneric(train, targets);
  }
  void Predict(const SpMat<real> &samples, Mat<Out> &results) {
    PredictGeneric(samples, results);
  }
  Mat<Out> Predict(const SpMat<real> &samples) {
    return PredictGeneric(samples);
  }

 protected:
  virtual void FitImpl(const Mat<real> &train, const Mat<Out> &targets) = 0;
  virtual void PredictImpl(const Mat<real> &samples, Mat<Out> &results) = 0;

  // By default the sparse version Fit and Predict are not implemented to signal that
  // the model cannot handle sparse matrices efficently. Subclasses that intends to provide
  // efficent operations on sparse matrices should override these functions.
  virtual void FitImpl(const SpMat<real> &/* train */, const Mat<Out> &/* targets */) {
    ANALYTICS_THROW("Not implemented");
  }
  virtual void PredictImpl(const SpMat<real> &/* samples */, Mat<Out> &/* results */) {
    ANALYTICS_THROW("Not implemented");
  }

 private:
  template <template <class> class M>
  void FitGeneric(const M<real> &train, const Mat<Out> &targets) {
    Validator::ValidatesFitInput(train, targets);
    if (train.n_rows > 0) {
      FitImpl(train, targets);
    }
    num_features_trained_ = train.n_cols;
  }

  template <template <class> class M>
  void PredictGeneric(const M<real> &samples, Mat<Out> &results) {
    if (samples.n_rows == 0) {
      results.clear();
      return;
    }
    if (num_features_trained_ != samples.n_cols) {
      ANALYTICS_THROW("samples.n_cols (" << samples.n_cols << ") does not match with train.n_cols (" <<
                      num_features_trained_ << ")");
    }
    Validator::ValidatesPredictInput(samples);

    PredictImpl(samples, results);
    Validator::ValidatesPredictOutput(samples, results);
  }

  template <template <class> class M>
  Mat<Out> PredictGeneric(const M<real> &samples) {
    Mat<Out> out;
    Predict(samples, out);
    return out;
  }

  uword num_features_trained_ = 0;
};

template <typename real = float>
using Regressor = PredictionModel<real, real>;

template <typename real = float, typename label = uword>
using Classifier = PredictionModel<
  real,
  typename std::enable_if<std::is_integral<label>::value, label>::type
>;

}

#endif
