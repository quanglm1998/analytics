#ifndef DTL_ANALYTICS_DT_H
#define DTL_ANALYTICS_DT_H
#include <assert.h>
#include <queue>
#include <vector>
#include <unordered_map>
#include <random>
#include <utility>
#include <algorithm>
#include "analytics/ml.h"
#include "analytics/random.h"

namespace analytics {
struct DesisionTreeConfig {
  static const int MSE = 0;
  static const int MCE = 1;
  static const int MAE = 2;
  static const int CUSTOM = 3;
  /*
   * if random_features = {4, 5, 10}, it means in depth = 0 select 4 random features for checking
   * in depth = 1, select 5 random features for checking,
   * for depth >= 2, select 10 random features for checking.
   * random_features can be of different size
   * It is for feature ID
   */
  ivec random_features = {10};

  /*
  * for kth features, we define (the set of all the values in Gthe kth column of the input matrix) = Sk
   * if random_positions = {2, 7, 10}, it means for each selected feature
   * in depth = 0 select 2 random features value in Sk for checking
   * in depth = 1, select 7 random features value in Sk for checking,
   * for depth >= 2, select 10 random features value in Sk for checking.
   * It is for split value
   */
  ivec random_positions = {5};
  int features_ignored = 0;
  int group_feature = -1;
  ivec groups = {};
  int max_level = 100;  // if depth >= max_level stop spliting
  int max_node_size = 1;  // if sample size <= max_node_size, stop spliting
  int max_nodes = 0;  // if tree size >= max_nodes, stop spliting
  int num_trees = 1;  // how many trees
  double bag_size = 1.0;
  int loss_function = MSE;  // loss function for determine best split (MSE,MAE,MCE)
  bool use_bootstrapping = true;
  bool compute_importances = false;
  bool use_line_search = false;
  double shrinkage = 0.1;
};

template <typename real>
class DecisionTree {
  struct TreeNode {
    int level;
    int feature;
    real value;
    real result;
    int left;
    int right;
    TreeNode() {
      level = -1;
      feature = -1;
      value = 0;
      result = 0;
      left = -1;
      right = -1;
    }
  };

 public:
  DecisionTree(const Mat<real> &features,
               const Mat<real> &results,
               const DesisionTreeConfig &config,
               const int seed,
               const int use_weights,
               const int scoring_function);

  real Predict(const Row<real> &features) const;

  const Col<real>& importance() const {
    return this->importances_;
  }

 private:
  std::vector<TreeNode> nodes_;
  Col<real> importances_;

  real CustomLoss(real x) {
    return abs(x) * sqrt(abs(x));
  }
};
template <typename real>
class TreeEnsemble {
 public:
  explicit TreeEnsemble(int seed = 1) : gen_(seed) { }

  void Clear() {
    trees_.clear();
    trees_.shrink_to_fit();
  }

  int RngNext(int s = 1 << 30) {
    return gen_.randi(s);  // [0, s) uniform distribution
  }

  DecisionTree<real> CreateTree(const Mat<real> &features,
                                const Mat<real> &results,
                                DesisionTreeConfig &config,
                                int seed) {
    return DecisionTree<real>(features, results, config, seed, config.use_bootstrapping, config.loss_function);
  }

 protected:
  Random gen_;
  std::vector<DecisionTree<real>> trees_;
  Col<real> importances_;
  DesisionTreeConfig config_;
};

template <typename real>
class RandomForestRegressor : public TreeEnsemble<real>, public Regressor<real> {
 public:
  // treenum:how many trees to build
  void set_num_trees(int num_trees) {
    this->config_.num_trees = num_trees;
  }

  int num_trees() {
    return this->config_.num_trees;
  }

  // lossfunction:can be MSE MCE MAE
  void set_loss_function(int loss_function) {
    this->config_.loss_function = loss_function;
  }

  int loss_function() {
    return this->config_.loss_function;
  }

  // maxlevel:max tree depth
  void set_max_level(int max_level) {
    this->config_.max_level = max_level;
  }

  int max_level() {
    return this->config_.max_level;
  }

  // random_features:random features selected for checking
  void set_random_features(const ivec &random_features) {
    this->config_.random_features = random_features;
  }

  const ivec &random_features() {
    return this->config_.random_features;
  }

  // random_positions:random positions selected for checking
  void set_random_positions(const ivec &random_positions) {
    this->config_.random_positions = random_positions;
  }

  const ivec &random_positions() {
    return this->config_.random_positions;
  }

  // compute_importances:whether to compute importance or not
  void set_compute_importances(bool compute_importances) {
    this->config_.compute_importances = compute_importances;
  }

  // return feature importance if needed
  const Col<real> &importance() {
    assert(this->config_.compute_importances == 1);
    return this->importances_;
  }

 protected:
  void FitImpl(const Mat<real> &trains, const Mat<real> &targets) override;

  void PredictImpl(const Mat<real> &samples, Mat<real> &results) override;
};
}
#endif
