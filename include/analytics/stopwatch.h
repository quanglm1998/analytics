/*********************************************************
 * File       : stopwatch.h
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 27 Feb 2017 09:44:29 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_STOPWATCH_H
#define DTL_ANALYTICS_STOPWATCH_H

#include <chrono>
#include <cstdint>

namespace analytics {

class Stopwatch {
 public:
  using clock = std::chrono::system_clock;
  using time_point = std::chrono::time_point<clock>;

  Stopwatch() {
    Start();
  }

  void Start() {
    if (running_) return;

    running_ = true;
    start_time_ = clock::now();
  }

  void Stop() {
    if (!running_) return;

    elapsed_ns_ = elapsed_ms();
    running_ = false;
  }

  void Reset() {
    elapsed_ns_ = 0;
    start_time_ = clock::now();
  }

  size_t elapsed_ns() const {
    if (!running_) return elapsed_ns_;

    time_point end_time = clock::now();
    auto diff = end_time - start_time_;
    return elapsed_ns_ + std::chrono::duration_cast<std::chrono::nanoseconds>(diff).count();
  }

  double elapsed_us() const {
    return elapsed_ns() * 10e-3;
  }

  double elapsed_ms() const {
    return elapsed_ns() * 1e-6;
  }

  double elapsed_secs() const {
    return elapsed_ns() * 1e-9;
  }

 private:
  bool running_ = false;
  size_t elapsed_ns_ = 0;
  time_point start_time_;
};

}

#endif
