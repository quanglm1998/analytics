/*********************************************************
 * File       : analytics_rr.h
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 24 Feb 2017 09:59:49 AM
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_RR_H
#define DTL_ANALYTICS_RR_H

#include "analytics/ml.h"

namespace analytics {

template <typename real = float>
class RidgeRegressor : public Regressor<real> {
 public:
  RidgeRegressor() : has_inter_(true), success_(false), lambda_(0) {}

  void set_intercept(bool has_inter) { has_inter_ = has_inter; }
  void set_lambda(real lambda) { lambda_ = lambda; }

  const Mat<real> &coefficients() const { return coeff_; }
  const Row<real> &intercepts() const { return inter_; }

 protected:
  void FitImpl(const Mat<real> &trains, const Mat<real> &target) override;
  void PredictImpl(const Mat<real> &samples, Mat<real> &results) override;

 private:
  Mat<real> coeff_;  // coefficients or exposures
  Row<real> inter_;  // intercept
  bool has_inter_, success_;
  real lambda_;  // penalty factor
};

}

#endif
