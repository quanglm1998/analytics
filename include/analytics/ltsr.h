/*********************************************************
 * File       : lts.h
 * Author     : xzhuo
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Sun 19 Aug 2018 03:58:51 PM +08
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_LTSR_H
#define DTL_ANALYTICS_LTSR_H

#include "analytics/ml.h"

namespace analytics {

template <typename real = float>
class LeastTrimmedSquareRegressor : public Regressor<real> {
 public:
  LeastTrimmedSquareRegressor()
    : path_num_(100), select_num_(10), eps_(1e-4), has_inter_(true), success_(false)
  {}

  LeastTrimmedSquareRegressor(int path_num,
                              int select_num,
                              real eps = 1e-4,
                              bool has_inter = true)
    : path_num_(path_num), select_num_(select_num), eps_(eps),
      has_inter_(has_inter), success_(false)
  {}

  void set_intercept(bool has_inter) { has_inter_ = has_inter; }
  void set_path_num(int path_num) { path_num_ = path_num; }
  void set_select_num(int select_num) { select_num_ = select_num; }
  void set_tolerance(real eps) { eps_ = eps; }

  const Mat<real> &coefficients() const { return coeff_; }
  const Row<real> &intercepts() const { return inter_; }

 protected:
  void FitImpl(const Mat<real> &trains, const Mat<real> &target) override;
  void PredictImpl(const Mat<real> &samples, Mat<real> &results) override;

  Mat<real> Regress(const Mat<real>& X, const Mat<real>& Y);
  real Cstep(const Mat<real>& X, const Mat<real>& Y, size_t h, Mat<real>& params);
  void AddRand(Mat<real> &X, Mat<real> &Y);
  Mat<real> Initialize(const Mat<real>& X, const Mat<real>& Y, size_t p, int seed);


 private:
  int path_num_, select_num_;
  real eps_;
  bool has_inter_, success_;
  Mat<real> coeff_;  // coefficients or exposures
  Mat<real> resid_;  // residue
  Row<real> inter_;
};

}

#endif
