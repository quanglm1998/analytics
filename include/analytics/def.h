/*********************************************************
 * File       : def.h
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 02 Mar 2017 11:41:57 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_DEF_H
#define DTL_ANALYTICS_DEF_H

#define ARMA_DONT_PRINT_CXX11_WARNING
#include <armadillo>

#include <cstdint>

namespace analytics {

typedef std::int8_t int8_t;
typedef std::int16_t int16_t;
typedef std::int32_t int32_t;
typedef std::int64_t int64_t;

typedef std::uint8_t uint8_t;
typedef std::uint16_t uint16_t;
typedef std::uint32_t uint32_t;
typedef std::uint64_t uint64_t;

// import some types from armadillo
template <typename T>
using Mat = arma::Mat<T>;

template <typename T>
using SpMat = arma::SpMat<T>;

template <typename T>
using Col = arma::Col<T>;

template <typename T>
using Row = arma::Row<T>;

template <typename T>
using Cube = arma::Cube<T>;

typedef arma::uword uword;
typedef arma::sword sword;

typedef arma::mat mat;
typedef arma::fmat fmat;
typedef arma::imat imat;
typedef arma::umat umat;

typedef arma::vec vec;
typedef arma::fvec fvec;
typedef arma::ivec ivec;
typedef arma::uvec uvec;

}

#endif
