/*********************************************************
 * File       : entropy_model.h
 * Author     : hyu
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: entropy model interface
 * Created    : Mon 26 Nov 2018 02:32:44 PM +08
 * Revision   : ftang
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_ENTROPY_H
#define DTL_ANALYTICS_ENTROPY_H

#include <vector>
#include "analytics/def.h"

namespace analytics {

template <typename data, typename real>
class EntropyModelBase {
 public:
  EntropyModelBase() {}

  virtual real ComputeEntropy(const Col<data> &seq, int cond_len) = 0;
  virtual data Predict(const Col<data> &seq, int cond_len) = 0;
};

template <typename data, typename real = float>
class EntropyModel: public EntropyModelBase<data, real> {
 public:
  EntropyModel() : EntropyModelBase<data, real>() {}
  explicit EntropyModel(const Col<data> &state_list)
    : EntropyModelBase<data, real>(), state_list_(state_list) {}

  void set_state_list(const Col<data> &state_list) { state_list_ = state_list; }
  Col<data> state_list() const { return state_list_; }

  real ComputeEntropy(const Col<data> &seq, int cond_len) override;
  data Predict(const Col<data> &seq, int cond_len) override;

  real ComputeRenyiEntropy(const Col<data> &seq, int cond_len, real q);

 protected:
  arma::uvec MatchSubSeries(const Col<data> &series, const Col<data> &sub);
  real ComputeProbility(const Col<data> &series, const Col<data> &condition, data state);

 private:
  Col<data> state_list_;
};

template <typename data = float, typename real = data>
class EntropyModelCont: public EntropyModelBase<data, real> {
 public:
  EntropyModelCont() : EntropyModelBase<data, real>() {}

  real ComputeEntropy(const Col<data> &seq, int cond_len) override;
  data Predict(const Col<data> &seq, int cond_len) override;

 protected:
  Mat<data> Series2Mat(int m, const Col<data> &series);
  Col<data> Preprocess(const Col<data> &seq);
  real Entanglement(int m, data r, const Col<data> &series);
};

}

#endif
