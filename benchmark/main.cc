/*********************************************************
 * File       : main.cc
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 24 Feb 2017 05:47:46 PM SGT
 * Revision   : none
 *
 *********************************************************/
#include <string>
#include <memory>
#include <utility>
#include <vector>

#include "analytics.h"
#include "analytics/benchmark.h"

using namespace analytics;

typedef double real;
using std::pair;
using std::shared_ptr;
using std::string;
using std::vector;
using std::make_shared;

vector<pair<string, shared_ptr<Regressor<real>>>> MakeRegressors() {
  vector<pair<string, shared_ptr<Regressor<real>>>> ret;

  ret.emplace_back("Least square regression", make_shared<LeastSquareRegressor<real>>());

  auto rr = make_shared<RidgeRegressor<real>>();
  rr->set_lambda(0.1);
  ret.emplace_back("Ridge regression", rr);

  auto lasso = make_shared<Lasso<real>>();
  lasso->set_lambda(0.1);
  ret.emplace_back("Lasso", lasso);

  auto fm = make_shared<FM<real>>();
  fm->set_learn_rate(0.00004);
  fm->set_num_iterations(5000);
  ret.emplace_back("Factorization Machine", fm);

  auto dt = make_shared<RandomForestRegressor<real>>();
  dt->set_num_trees(20);
  dt->set_compute_importances(1);
  dt->set_max_level(3);
  ret.emplace_back("Random Forest", dt);

  auto svr = make_shared<SVR<real>>();
  svr->set_num_iterations(10);
  svr->set_C(0.0002);
  ret.emplace_back("SVR", svr);

  return ret;
}

vector<pair<string, shared_ptr<Classifier<real>>>> MakeClassifiers() {
  vector<pair<string, shared_ptr<Classifier<real>>>> ret;

  return ret;
}

template <typename Out, typename ModelFactory, typename BenchmarkFunc>
void RunBenchmark(const vector<string> datasets, ModelFactory factory, BenchmarkFunc benchmark) {
  for (const auto &dataset : datasets) {
    Mat<real> train;
    Col<Out> train_targets;
    Mat<real> test;
    Col<Out> test_targets;
    LoadDataset(dataset, train, train_targets, test, test_targets);

    auto models = factory();
    for (auto &model : models) {
      benchmark(model.first, *(model.second),
                train, train_targets, test, test_targets);
    }
  }
}

int main(int argc, char **argv) {
  using namespace analytics;

  vector<string> regression_datasets = {
    "data/yacht_hydrodynamics.data"
  };

  vector<string> classification_datasets = {
    "data/fertility.data"
  };

  for (int i = 1; i < argc; i++) {
    string arg(argv[i]);
    if (arg == "--large" || arg == "-l") {
      regression_datasets.emplace_back("data/buzz/TomsHardware/TomsHardware.data");
      regression_datasets.emplace_back("data/buzz/Twitter/Twitter.data");

      classification_datasets.emplace_back("data/drive_diagnosis.data");
    }
  }

  ConfigLogger("analytics", "", "info");
  ANALYTICS_LOG_INFO("Running benchmark");

  ANALYTICS_LOG_INFO("-----------");
  ANALYTICS_LOG_INFO("Regression Benchmark");
  RunBenchmark<real>(regression_datasets, MakeRegressors, BenchmarkRegressor<real>);

  ANALYTICS_LOG_INFO("-----------");
  ANALYTICS_LOG_INFO("Classification Benchmark");
  RunBenchmark<uword>(classification_datasets, MakeClassifiers, BenchmarkClassifier<real>);

  return 0;
}
