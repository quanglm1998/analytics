/*********************************************************
 * File       : benchmark.h
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 27 Feb 2017 10:32:48 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_BENCHMARK_H
#define DTL_ANALYTICS_BENCHMARK_H

#include <string>

#include "analytics/ml.h"
#include "analytics/stopwatch.h"

namespace analytics {

const double TRAIN_RATIO = 0.9;

template <typename real, typename Out>
void LoadDataset(const std::string &path, Mat<real> &features, Col<Out> &targets) {
  ANALYTICS_LOG_INFO("Loading " << path);
  Mat<real> m;
  m.load(path, arma::csv_ascii);
  features = m.cols(0, m.n_cols - 2);
  targets = arma::conv_to<Col<Out>>::from(m.col(m.n_cols - 1));
  ANALYTICS_LOG_INFO("Loaded " << m.n_rows << "x" << m.n_cols << " from " << path);
}

template <typename real, typename Out>
void LoadDataset(const std::string &path,
                 Mat<real> &train, Col<Out> &train_targets,
                 Mat<real> &test, Col<Out> &test_targets) {
  Mat<real> features;
  Col<Out> targets;
  LoadDataset(path, features, targets);

  int num_train = static_cast<int>(features.n_rows * TRAIN_RATIO);
  train = features.rows(0, num_train - 1);
  train_targets = targets.rows(0, num_train - 1);
  test = features.rows(num_train, features.n_rows - 1);
  test_targets = targets.rows(num_train, features.n_rows - 1);
}

template <typename real>
real R2Score(const Col<real> &truth, const Col<real> &pred) {
  real avg = arma::mean(truth);
  Col<real> diff_pred = truth - pred;
  Col<real> diff_avg = truth - avg;
  return 1 - arma::accu(diff_pred % diff_pred) / arma::accu(diff_avg % diff_avg);
}

template <typename real>
real AvgError(const Col<real> &truth, const Col<real> &pred) {
  return arma::accu(arma::abs(pred - truth)) / arma::mean(truth) / truth.size();
}

template <typename T>
double Accuracy(const Col<T> &truth, const Col<T> &pred) {
  size_t matched = 0;
  for (unsigned i = 0; i < truth.n_rows; i++) {
    if (truth(i) == pred(i)) {
      matched++;
    }
  }
  return static_cast<double>(matched) / truth.n_rows;
}

template <typename real>
void BenchmarkRegressor(
    const std::string &name, Regressor<real> &m,
    const Mat<real> &train, const Col<real> &train_targets,
    const Mat<real> &test, const Col<real> &test_targets) {
  ANALYTICS_LOG_INFO("----- " << name << " -----");
  Stopwatch sw;
  m.Fit(train, train_targets);
  ANALYTICS_LOG_INFO("Training: " << sw.elapsed_secs() << " seconds");
  sw.Reset();
  Col<real> pred = m.Predict(test).col(0);
  ANALYTICS_LOG_INFO("Prediction: " << sw.elapsed_secs() << " seconds");
  ANALYTICS_LOG_INFO("R2 score: " << R2Score(test_targets, pred));
  ANALYTICS_LOG_INFO("Avg error %: " << (AvgError(test_targets, pred)) * 100);
}

template <typename real>
void BenchmarkClassifier(
    const std::string &name, Classifier<real> &m,
    const Mat<real> &train, const Col<uword> &train_targets,
    const Mat<real> &test, const Col<uword> &test_targets) {
  ANALYTICS_LOG_INFO("----- " << name << " -----");
  Stopwatch sw;
  m.Fit(train, train_targets);
  ANALYTICS_LOG_INFO("Training: " << sw.elapsed_secs() << " seconds");
  sw.Reset();
  Col<uword> pred = m.Predict(test).col(0);
  ANALYTICS_LOG_INFO("Prediction: " << sw.elapsed_secs() << " seconds");
  ANALYTICS_LOG_INFO("Accuracy: " << Accuracy(test_targets, pred));
}

}

#endif
