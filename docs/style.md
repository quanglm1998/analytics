Style Guide
-----------
[Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)

### Lint

- Run `bin/lint.sh` and fix all errors. __This shall be strictly enforced.__
  Exceptions are granted in very rare cases.

### Files
- Header files should end in `.h`

- C++ files should end in `.cc`

### Format
- Each line of text in your code should be at most __120__ characters long.
  Use `set cc=120` in Vim to help your visualize this.

- Use 2 spaces indent.

### Naming
- The names of types and functions should start with a capital letter and have a capital letter
  for each new word: `AppleCider`, `RandomDistribution`.
  Accessors and mutators (get and set functions) may be named like variables.
  For example, `int count()` and void `set_count(int count)`.

- The names of variables, parameters and data members are usaually all lowercase, with underscores
  between words. Private data members of classes additionally have trailing underscores.
  For instance: `billy_bat`, `wonder_land`, `butterfly_hunter_`.
  Single uppercase letter is also acceptable as this is very common in math: `A`, `X`.

- The names of constant variables are all uppercase, with underscores between words:
  `NUM_APPLES`, `DISTANCE_TO_MOON`.


### Functions
- Return types on the same line as function names:

```cpp
int Fib() {
}

// NOT THIS
int
Fib() {
}
```

- Explicit annotate overrides of virtual functions.  A function or destructor marked `override` or
  final that is not an override of a base class virtual function will not compile.
  This helps avoid some common errors.

```cpp
class FooBar : public Foo {
  void Foo() override {
  }
};
```

### Classes
- A class definition should usually start with a `public:` section, followed by `protected:`, then `private:`.
  Omit sections that would be empty.
  Within each section, generally prefer grouping similar kinds of declarations together

- Make data members `private`, unless they are `static const`.

### Comments

- Function and class comments should appear before the corresponding entity.

```cpp
// Comments here
class Foo {
  // Comments here
  void Bar() {
  }
};
```

- Variable comments can be inline or before the variable.

```cpp
int inc;  // inline comment

// another style
float delta;
```

### Misc
- Open curly braces on the same line

```cpp
namespace abc {

class Foo {
  void Bar() {
    if (1 == 0) {
    }
  }
};

}
```

- Use `uword` loop variables to avoid casting.

```cpp
for (uword i = 0; i < v.size(); i++) {
}
```
