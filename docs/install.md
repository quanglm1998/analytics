Installation and Usage in SIMA
---------
Say you want to install the library at `/tmp/analytics` for testing purpose:
1. Run the following bash script in the project directory

    ``` bash
    mkdir -p build
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=/tmp/analytics
    make -j8
    make install
    ```

2. Update your sima Makefile

    ```makefile
    CFLAGS += -I/tmp/analytics/include
    LIBDIRS += /tmp/analytics/lib
    ```

3. Update your alpha/dmgr Makefile to link with `libanalytics.so`

    ```makefile
    $(eval $(call LOCALSO_PRO, alpha_json_5dr, alpha_json_5dr, alphas/alpha_json_5dr, ,config universe niodata oputil analytics))
    ```

4. Include `analytics.h` in your alpha/dmgr and start using the library!

    ```cpp
    #include <analytics.h>
    ```
