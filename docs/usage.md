Usage
=====

Contents
---------

- [Armadillo](#armadillo)
- [Least Square Regression](#least-square-regression)
- [Ridge Regression](#ridge-regression)
- [Lasso](#lasso)
- [SVR](#svr)
- [Random Forest](#random-forest)
- [Factorization Machine](#fm-factorization-machine)
- [KNN](#knn)
- [Gradient Boosting Tree](#gradient-boosting-tree)
- [Multi-Layer Perceptron](#mlp)
- [Hidden Markov Model](#hmm-hidden-markov-model)
- [SVM Classifier](#svm-classifier)
- [EMD](#emd-empirical-mode-decomposition)
- [How to Use in Pulse](#how-to-use-in-pulse)


Armadillo
---------
This library is built on top of [Armadillo](http://arma.sourceforge.net/docs.html), a C++ linear algebra library.

### Basic Example
```cpp
#include <iostream>
#include <armadillo>
#include <analytics.h>

using namespace analytics;

int main() {
  Mat<double> A(3, 4, arma::fill::ones);
  Mat<double> B(3, 4, arma::fill::zeros);
  A += 1.5;
  B -= 2.0;
  Mat<double> C = A + B;
  Mat<double> D = A * 2;
  Mat<double> E = C * D.t();
  Mat<double> F = arma::sqrt(D);

  Row<double> G = F.row(0); // take the first row
  Col<double> H = F.col(1); // take the second column

  std::cout << E << std::endl;
  return 0;
}
```

### Matrices and Vectors
The root matrix class is [Mat<T>](http://arma.sourceforge.net/docs.html#Mat), where `T` is one of `float`, `double`,
`int`, `unsigned`, etc.

There are two vector classes, [Col<T>](http://arma.sourceforge.net/docs.html#Col) (column vector), and
[Row<T>](http://arma.sourceforge.net/docs.html#Row) (row vector). These are subclasses of `Mat<T>`, so they support
all matrix operations.

A quick way to learn what are the available operations is to check the tables
[here](http://arma.sourceforge.net/docs.html#Row).

### References
- [Armadillo website](http://arma.sourceforge.net/docs.html)
- [Nice tables summarizing useful operations and math functions](http://arma.sourceforge.net/armadillo_joss_2016.pdf)


Least Square Regression
------
### Example
```cpp
Mat<float> X = {{0.0, 0.0}, {0.0, 0.1}};
Mat<float> Y = {{0.0}, {1.0}};
Mat<float> A = {{0.0, 0.0}};
LeastSquareRegressor<float> lsr;
lsr.set_intercept(true);  // Optional, to tell the regressor whether to use intercept or not.
                          // Default value is true
lsr.Fit(X, Y);
Mat<float> pred = lsr.Predict(A);

Mat<float> coeff = rr.coefficients();  // regression coefficients or exposures
Mat<float> inter = rr.intercpts();  // regression intercepts, generally speaking it will return a Rowvec
```

### References
- [Wikipedia](https://en.wikipedia.org/wiki/Linear_regression)


Ridge Regression
------
### Parameters
- `lambda`: penalty coefficient

### Example
```cpp
Mat<float> X = {{0.0, 0.0}, {0.0, 0.1}};
Mat<float> Y = {{0.0}, {1.0}};
Mat<float> A = {{0.0, 0.0}};
RidgeRegressor<float> rr;
rr.set_intercept(true);  // Optional, to tell the regressor whether to use intercept or not.
                         // Default value is true
rr.set_lambda(0.1);   // Set penalty coefficient. Default value is 0.

rr.Fit(X, Y);
Mat<float> pred = rr.Predict(A);

Mat<float> coeff = rr.coefficients();  // regression coefficients or exposures
Mat<float> inter = rr.intercpts();  // regression intercepts, generally speaking it will return a Rowvec
```

### References
- [Wikipedia](https://en.wikipedia.org/wiki/Tikhonov_regularization)


Lasso
-----
### Parameters
- `lambda`:  constant that multiplies the L1 term.
- `has_inter`:  boolean. whether to calculate the intercept for this model.
- `tol`:  the tolerance for the optimization.
- `max_iter`: the maximum number of iteration times

### Example

```cpp
Mat<float> X = {{0.0, 0.0}, {0.0, 0.1}};
Mat<float> Y = {{0.0}, {1.0}};
Mat<float> A = {{0.0, 0.0}};
Lasso<float> lasso;
lasso.set_lambda(0.1);
lasso.Fit(X, Y);
Mat<float> pred = lasso.Predict(A);
```

### Reference
- [Wikipedia](https://en.wikipedia.org/wiki/Lasso_(statistics))


SVR
------
### Parameters
- `C`:  upper bound for coefficient, `-C <= weight[i] <= C`.

- `eps`: stopping tolerance.

- `p`: regularization for weight.

- `num_iterations`: regularization for weight.

### Example
```cpp
Mat<double> x1 = {{0.0, 0.0}, {0.0, 0.1}};
Mat<double> y1 = {{0.0}, {1.0}};
Mat<double> x2 = {{0.0, 0.0}, {0.0, 0.1}};

SVR<double> svr;
svr.set_num_iterations(5);
svr.set_C(0.001);
svr.Fit(x1, y1);
Mat<double> pred = svr.Predict(x2);
```

### References
- [Wikipedia](https://en.wikipedia.org/wiki/Support_vector_machine#Regression)


Random Forest
------
### Parameters
- `num_trees`:  number of trees to be built.

- `max_level`: max depth for each tree.

- `random_features`: regularization features for checking, if `random_features` = {4, 5, 10}, it means in depth = 0 select 4 random features for checking, in depth = 1, select 5 random features for checking, for depth >= 2, select 10 random features for checking. `random_features` can be of different size. (Default value is {10}).

- `random_positions`: random positions for checking. For kth features, we define (the set of all the values in the kth column of the input matrix) = Sk, if `random_positions` = {2, 7, 10}, it means for each selected feature in depth = 0 select 2 random features value in Sk for checking, in depth = 1, select 7 random features value in Sk for checking, for depth >= 2, select 10 random features value in Sk for checking. (Default value is {5}).


### Example
```cpp
Mat<double> x1 = {{0.0, 0.0}, {0.0, 0.1}};
Mat<double> y1 = {{0.0}, {1.0}};
Mat<double> x2 = {{0.0, 0.0}, {0.0, 0.1}};

RandomForestRegressor<double> dt;
dt.set_num_trees(100);  // set tree number
dt.set_max_level(it);  // set max depth
dt.Fit(x1, y1);
Mat<double> pred = dt.Predict(x2);
```

### References
- [Wikipedia](https://en.wikipedia.org/wiki/Random_forest)

FM (Factorization Machine)
------
### Parameters
- `p`: number of features

- `k`: the second order inner dimension (`k << p`)

- `max`: maximum target value

- `min`: minimum target value

- `stddev`: initialize second order weights with normal distribution `N(0, stddev_^2)`

- `learn_rate`: learning rate

- `reg0`: regulization parameter for the constant weight term

- `reg1`: regulization parameter for the first order feature weights

- `reg2`: regulization parameter for the second order feature weights

- `num_iterations`: number of iterations when learning

- `rng_seed`: seed for random number generator engine


### Example
```cpp
arma::Mat<float> x1 = {{0.0, 0.0}, {0.0, 0.1}};
arma::Mat<float> y1 = {{0.0}, {1.0}};
arma::Mat<float> x2 = {{0.0, 0.0}, {0.0, 0.1}};

FM<float> fm;
fm.Initialize(2, 2, 1, 0.00004, 0.0, 15000, 0); // set parameters for FM model
fm.Fit(x1, y1);
Mat<float> pred = fm.Predict(x2);
```

# References
- [Paper](http://www.algo.uni-konstanz.de/members/rendle/pdf/Rendle2010FM.pdf)


KNN
------
### Parameters
- `topk`:  number of nearest neighbors.

- `maxnodes`: the maximum number of children in the tree. default 10

- `minnodes`:  the minimum number of children in the tree.  default 5

### Example
```cpp
Mat<float> X(7, 3);
Mat<uword> y(7, 1);
X(0, 0) = 0.5; X(0, 1) = 0.5; X(0, 2) = 0.73;
X(1, 0) = 0.2; X(1, 1) = 0.4; X(1, 2) = 0.18;
X(2, 0) = 0.45; X(2, 1) = 0.23; X(2, 2) = 0.18;
X(3, 0) = 0.47; X(3, 1) = 0.83; X(3, 2) = 0.91;
X(4, 0) = 0.43; X(4, 1) = 0.27; X(4, 2) = 0.71;
X(5, 0) = 0.33; X(5, 1) = 0.74; X(5, 2) = 0.13;
X(6, 0) = 0.78; X(6, 1) = 0.33; X(6, 2) = 0.55;
y(0, 0) = 0; y(1, 0) = 1; y(2, 0) = 1; y(3, 0) = 0; y(4, 0) = 2; y(5, 0) = 1; y(6, 0) = 2;
KNN<float> knn;
knn.setMaxnodes(3);
knn.setMinnodes(1);
knn.setTopk(3);
knn.Fit(X, y);

Mat<float> test(2, 3);
test(0, 0) = 0.3; test(0, 1) = 0.2; test(0, 2) = 0.8;
test(1, 0) = 0.3; test(1, 1) = 0.2; test(1, 2) = 0.1;
Mat<uword> result(2, 1);
knn.Predict(test, result);
cout << result << endl;
```
### References
- [Wikipedia](https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm)

Gradient Boosting Tree
------

### Parameters
- `max_level`:  max depth of each tree(should be <= 12).

- `lambda`: penalty for weight. Should be >= 0. Bigger this value means more penalty.

- `gamma`:  penalty for tree node. Should be >= 0. Bigger this value means more penalty.

- `min_ratio`: each node will split data to two parts, if any part's sample number is smaller than `min_ratio` * all sample, will stop splitting.

- `shrinkage`: learning rate.

- `verbose`: verbose = 1 will print out fitting error.

### Example
```cpp
int sz = 5000;
int N = 10;
mat x1(sz, N);
x1.randu();
mat y1(sz, 1);
y1.randu();
mat x2(sz, N);
x2.randu();
mat y2(sz, 1);
y2.randu();
for (int i = 0; i < sz; ++i) {
  y1(i, 0) += x1(i, 0) * x1(i, 1) + x1(i, 3);
  y2(i, 0) += x2(i, 0) * x2(i, 1) + x2(i, 3);
}

GradientBoostingTreeRegressor<double> dt;
dt.set_num_trees(100);  // set tree number
dt.set_max_level(7);  // set max depth
dt.set_shrinkage(0.001);
dt.Fit(x1, y1);
arma::Mat<double> pyfm(x2.size(), 1);
dt.Predict(x2, pyfm);
double aucf = GBTR2(pyfm, y2);  // out sample
arma::Mat<double> pyim(x1.size(), 1);
dt.Predict(x1, pyim);
```

### Notes
Better to make training set's target's mean = 0.0.


MLP
------------

### Parameters
- `layer`:  a vector to specify the structure of MLP.

- `epoch`: number of iteration.

- `batch_sz`:  will train `batch_sz` number of data each time, can make it like 100.

- `learning_rate`: learning rate

- `activation_function`: function used inside the network. Can be LINEAR,RELU,SIGM

- `output_function`: function used for output layer. Can be LINEAR,RELU,SIGM

### Example
```cpp
int sz = 10000;
uword N = 10; //how many category to predict
uword catnum = 10; //10 categories
assert(N >= catnum);  // necessary in this example
// 0. prepare data
mat x1(sz, N);
x1.randu();
mat y1(sz, catnum);
y1.randu();
for (int i = 0; i < sz; ++i) {
  int maxi = 0;
  double maxv = x1(i, 0);
  for (uword j = 0; j < catnum; ++j) {
    if (x1(i, j) > maxv) {
      maxv = x1(i, j);
      maxi = j;
    }
    y1(i, j) = 0.0;
  }
  y1(i, maxi) = 1.0;//  each row of y have one 1, others zeros.
}

// 1. prepare mlp
class MLPRegressor<double> mlp;
vector<uword> layer = {N, 100, 100, 100, catnum}; //layer[0] = feature number, layer.back() == categories number
int epoch = 10; //depends on how many ber
int batch_sz = 10;
double learning_rate = 0.0001;
mlp.set_layer(layer);
mlp.set_epoch(epoch);
mlp.set_batch_sz(batch_sz);
mlp.set_learning_rate(learning_rate);
mlp.set_activation_function(MLPFunction::RELU);
mlp.set_output_function(MLPFunction::SIGM);

// 2. fit
mlp.Fit(x1, y1);

// 3. make prediction
mat py1;
mlp.Predict(x1, py1);
```

HMM (Hidden Markov Model)
------
### Parameters
- `max_iter`: the maximum number of iteration
- `tol`: the tolerance

### Example
```cpp

int n_hid(5);    // number of hidden states
int n_obs(6);   // number of observed states
int num(100);  // number of observations;
int len(100);    // length of the observed sequence

Random rand(2);

Mat<float> A(n_hid, n_hid);
rand.randu(A);
Mat<float> B(n_hid, n_obs);
rand.randu(B);
Row<float> p(h_hid);
rand.randu(p);

// Declare an object of hmm and initialize the parameters

HiddenMarkovModel<float> hmm;

hmm.set_transit_matrix(A);      // Set the transition matrix. Optional
hmm.set_emission_matrix(B);  // Set the emission matrix. Important. Users must give a meaningful emission matrix.
hmm.set_init_distribution(p);    // Set the initial states distribution. Optional

hmm.set_max_iter(1000);        // Set the maximum number of iteration. Optional. Default value is 10000
hmm.set_tol(1e-4);                  // Set the tolerance. Optional. Default value is 1e-5

Mat<int> obs(len, num);          // Generate the observations. The input observations can be a matrix
rand.randi(obs, 0, n_obs);        // The number of the row represents the number of observations
                                             // The number of the column represents the length of each observation

hmm.Fit(obs);                         // Fit parameters

Mat<int> hid;
hmm.Decode(obs, hid);           // Decode optimized hidden sequence

int step(4);
Cube<float> predict_obs, predict_hid;
hmm.Predict(obs, hid, predict_obs, predict_hid, step);  // Predict forward distribution for several steps

```

### Notes
- The users are only required to set a meaningful emission matrix if they let the model train the parameters. Other parameters are all optional.
- The users can also direct use the function of Decode() or Predict(). But in that case they need to set meaningful transition matrix and initial distribution first.

### References
- [Wikipedia](https://en.wikipedia.org/wiki/Hidden_Markov_model)

SVM Classifier
------
### Parameters
- `type`: solver type: "l2" or "l1", default value is l2

- `eps`: stopping error tolerance

- `Cp`: weight bound parameter

- `Cn`: weight bound parameter

- `l`: number of samples

- `w_size`: number of features

- `num_iterations`: number of iterations when learning


### Example
```cpp
arma::Mat<float> x1 = {{0.0, 0.0}, {0.0, 0.1}};
arma::Mat<uword> y1 = {{0}, {1}};
arma::Mat<float> x2 = {{0.0, 0.0}, {0.0, 0.1}};

SVMClassifier<float> svc;
svc.Fit(x1, y1);
arma::Mat<uword> pred;
svc.Predict(x2, pred);
```

### References
- [Paper](https://www.csie.ntu.edu.tw/~cjlin/papers/liblinear.pdf)
- [Paper](http://blog.csdn.net/v_july_v/article/details/7624837)
- [Library](https://www.csie.ntu.edu.tw/~cjlin/liblinear/)


EMD (Empirical Mode Decomposition)
------
### Parameters
- `order`: the number of implicit mode function (imf) to be sifted
- `max_iter`: the maximum number of iteration in sifting each imf
- `locality`: minimal distance between two extrema, default value is 0

### Example
```cpp

// Generate the input signal, which is composed with four different frequencies
int m(100);
float step(10.0 / m), pi(4.0 * atan(1.0));

Row<float> signal(m + 1);
for (int i = 0; i <= m; ++i) {
  float x = i * step;
  signal(i) = 0.5 * x + sin(pi * x) + sin(2 * pi * x) + sin(6 * pi * x);
}

// set parameters
int order(4), max_iter(5), locality(0);

// define the EMD object
EmpiricalModeDecomposition<float> emd;
emd.Init(order, max_iter, locality);

// signal decomposition
emd.Decompose(signal);

// print out the imfs
Mat<float> imf = emd.Imf();
for (int i = 0; i < imf.n_rows; ++i) {
  cout << "Implicit mode function - " << i << " : " << endl;
  cout << imf.row(i) << endl;
}

```

### References
- [Paper](Huang, N.E., Shen, Z., Long, S.R., Wu, M.L. Shih, H.H., Zheng, Q., Yen, N.C., Tung, C.C. and Liu, H.H. (1998)
The empirical mode decomposition and Hilbert spectrum for nonlinear and nonstationary time series analysis.
Proc. Roy. Soc. London A., 454, 903–995.)

How to Use in Pulse
-------
1. Update your alpha/dmgr Makefile to link with `libanalytics.so`.

    ```python
      pulse_bin(
          name = "libalpha_foo.so",
          srcs = glob([
              "*.cpp",
              "*.h",
          ]),
          linkopts = [
              "-lanalytics",
          ],
      )
    ```

2. Include `analytics.h` in your code.

    ```cpp
    #include <analytics.h>
    ```
