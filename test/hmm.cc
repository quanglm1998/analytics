/*********************************************************
 * File       : hmm.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 20 Apr 2017 09:46:15 AM
 * Revision   : Add test example for continuous HMM model,
 *              which is original written by jfu
 *
 *********************************************************/


#include "catch.hpp"

#include "analytics/test.h"
#include "analytics/hmm.h"
using namespace std;
using namespace analytics;

TEST_CASE("Hidden Markov model test", "[hidden Markov model]") {
  Random rand(2);

  int n_hid(5);  // number of hidden states
  int n_obs(6);  // number of observed states
  int num(100);  // number of observations;
  int len(100);  // length of the observed sequence

  Mat<float> B(n_hid, n_obs);
  rand.randu(B);

  ANALYTICS_LOG_INFO("Input emission matrix:");
  std::cout << B << std::endl;

  // Declare an object of hmm and initialize the parameters

  HiddenMarkovModel<float> hmm;
  hmm.set_emission_matrix(B);

  Mat<int> obs(len, num);
  rand.randi(obs, 0, n_obs);
  /* for (int i = 0; i < (int)obs.n_rows; i++) obs(i) = i % n_obs; */

  ANALYTICS_LOG_INFO("Number of input observation: " << num);
  ANALYTICS_LOG_INFO("The first input observation: " << num);
  std::cout << obs.col(0).t() << std::endl;

  // Fit parameters

  hmm.Fit(obs);

  ANALYTICS_LOG_INFO("Output transition matrix:");
  std::cout << hmm.transit_matrix() << std::endl;

  ANALYTICS_LOG_INFO("Output emission matrix:");
  std::cout << hmm.emission_matrix() << std::endl;

  ANALYTICS_LOG_INFO("Output initial distribution:");
  std::cout << hmm.init_distribution() << std::endl;

  // Decode optimized hidden sequence

  Mat<int> hid;
  hmm.Decode(obs, hid);

  ANALYTICS_LOG_INFO("The first optimized hidden sequence: " << num);
  std::cout << hid.col(0).t() << std::endl;

  int step(4);
  Cube<float> predict_obs, predict_hid;
  hmm.Predict(obs, hid, predict_obs, predict_hid, step);

  ANALYTICS_LOG_INFO("Predicted hidden states distributeion of the first sequence:");
  std::cout << predict_hid.slice(0) << std::endl;

  ANALYTICS_LOG_INFO("Predicted observation states distributeion of the first sequence:");
  std::cout << predict_obs.slice(0) << std::endl;
}

TEST_CASE("Continuous Hidden Markov model test 1", "[continuous hidden Markov model]") {
  ANALYTICS_LOG_INFO("Continuous HMM case 1: univariate input signal.");

  // Step 1: Initialize Model Parameters
  int n_hid(3);  // number of hidden states (positive integer)
  int n_obs(1);  // row number of distribution parameter mu and sigma_2
                 // (dimension: n_obs * n_hid)
  int len(15);  // sample length of observations;
  int num(n_obs);  // Dimension of the observed sequence should match
                   // the row number of mu and sigma_2.

  // (1) Generate the initial probability transition matrix A
  // Each row of A has sum 1. If this condition is not satisfied,
  // uniform distribution will be assigned to each row of A.
  // Typically, each hidden state has an intention to stay in the current state.
  // Thus, the diagonal elements of A are bigger than off-diagonal elements.
  Mat<float> A(n_hid, n_hid);

  A.ones();
  for (int j = 0; j < n_hid; ++j) {
    A(j, j) += 5;
  }
  for (int j = 0; j < n_hid; ++j) {
    float temp = sum(A.row(j));
    for (int i = 0; i < n_hid; ++i) {
      A(j, i) /= temp;  // normalization
    }
  }

  ANALYTICS_LOG_INFO("Input probability transition matrix:");
  std::cout << A << std::endl;

  // (2) Generate the initial probability vector p
  Row<float> p(n_hid);
  for (int j = 0; j < n_hid; ++j) {
    p[j] = 1.0 / n_hid;
  }
  ANALYTICS_LOG_INFO("Input initial probability vector:");
  std::cout << p << std::endl;

  // (3) Generate the initial mu vector (n_obs * n_hid)
  Mat<float> mu(n_obs, n_hid);
  mu(0, 0) = 0.8560;
  mu(0, 1) = 0.4662;
  mu(0, 2) = 0.0361;

  ANALYTICS_LOG_INFO("Input mu vector:");
  std::cout << mu << std::endl;

  // (4) Generate the initial sigma_2 vector (n_obs * n_hid),
  // which represents the variance of the normal distribution in each hidden state.
  Mat<float> sigma_2(n_obs, n_hid);
  sigma_2(0, 0) = 0.4540;
  sigma_2(0, 1) = 0.8720;
  sigma_2(0, 2) = 0.6714;

  ANALYTICS_LOG_INFO("Input sigma_2 vector:");
  std::cout << sigma_2 << std::endl;

  // Step 2: Declare an object of hmm and initialize the parameters

  HiddenMarkovModelCont<float> hmmc;
  hmmc.set_transit_matrix(A);
  hmmc.set_init_distribution(p);
  hmmc.set_emission_mu(mu);
  hmmc.set_emission_sigma(sigma_2);

  hmmc.set_max_iter(2000);
  hmmc.set_tol(1e-3);

  Mat<float> obs = {-0.68,  0.78, 0.12, 0.09, -0.05,
                    -0.29, -2.13, 0.23, 0.45, -1.68,
                     0.14, -1.12, 1.15, 0.07, -2.23};
  obs = obs.t();

  ANALYTICS_LOG_INFO("Number of input observation: " << len);
  ANALYTICS_LOG_INFO("The dimension of input observation: " << num);
  ANALYTICS_LOG_INFO("The first input observation: ");
  std::cout << obs.row(0) << std::endl;

  // Step 3: Fit parameters

  hmmc.Fit(obs);

  ANALYTICS_LOG_INFO("Output transition matrix:");
  std::cout << hmmc.transit_matrix() << std::endl;

  ANALYTICS_LOG_INFO("Output initial distribution:");
  std::cout << hmmc.init_distribution() << std::endl;

  ANALYTICS_LOG_INFO("Estimated mu vector:");
  std::cout << hmmc.emission_mu() << std::endl;

  ANALYTICS_LOG_INFO("Estimated variance vector:");
  std::cout << hmmc.emission_sigma() << std::endl;

  ANALYTICS_LOG_INFO("log_likelihood:");
  std::cout << hmmc.log_likelihood() << std::endl;

  // Step 4: Decode optimized hidden sequence

  Mat<int> hid;
  hmmc.Decode(obs, hid);

  ANALYTICS_LOG_INFO("The first optimized hidden sequence: " << num);
  std::cout << hid.row(0) << std::endl;

  int step(1);
  Cube<float> predict_obs, predict_hid;
  hmmc.Predict(obs, hid, predict_obs, predict_hid, step);

  ANALYTICS_LOG_INFO("Predicted hidden states distributeion of the first sequence:");
  std::cout << predict_hid.slice(0) << std::endl;

  ANALYTICS_LOG_INFO("Predicted obs value of the first sequence:");
  std::cout << predict_obs.slice(0) << std::endl;
}

TEST_CASE("Continuous Hidden Markov model test 2", "[continuous hidden Markov model]") {
  ANALYTICS_LOG_INFO("Case 2: multi-variate input signal.");

  // Step 1: Initialize Model Parameters
  int n_hid(3);  // number of hidden states (positive integer)
  int n_obs(2);  // row number of distribution parameter mu and sigma_2
                 // (dimension: n_obs * n_hid)
  int len(15);  // sample length of observations;
  int num(n_obs);  // Dimension of the observed sequence should match
                   // the row number of mu and sigma_2.

  // (1) Generate the initial probability transition matrix A
  // Each row of A has sum 1. If this condition is not satisfied,
  // uniform distribution will be assigned to each row of A.
  // Typically, each hidden state has an intention to stay in the current state.
  // Thus, the diagonal elements of A are bigger than off-diagonal elements.
  Mat<float> A(n_hid, n_hid);

  A.ones();
  for (int j = 0; j < n_hid; ++j) {
    A(j, j) += 5;
  }
  for (int j = 0; j < n_hid; ++j) {
    float temp = sum(A.row(j));
    for (int i = 0; i < n_hid; ++i) {
      A(j, i) /= temp;  // normalization
    }
  }

  ANALYTICS_LOG_INFO("Input probability transition matrix:");
  std::cout << A << std::endl;

  // (2) Generate the initial probability vector p
  Row<float> p(n_hid);
  for (int j = 0; j < n_hid; ++j) {
    p[j] = 1.0 / n_hid;
  }
  ANALYTICS_LOG_INFO("Input initial probability vector:");
  std::cout << p << std::endl;

  // (3) Generate the initial mu vector (n_obs * n_hid)
  Mat<float> mu(n_obs, n_hid);
  mu(0, 0) = 0.8560;
  mu(0, 1) = 0.0361;
  mu(0, 2) = 0.8720;
  mu(1, 0) = 0.4662;
  mu(1, 1) = 0.4540;
  mu(1, 2) = 0.6714;

  ANALYTICS_LOG_INFO("Input mu:");
  std::cout << mu << std::endl;

  // (3) Generate the initial sigma_2 vector (n_obs * n_hid),
  //  which represents the variance of the normal distribution in each hidden state.
  Mat<float> sigma_2(n_obs, n_hid);
  sigma_2(0, 0) = 0.6516;
  sigma_2(0, 1) = 0.7697;
  sigma_2(0, 2) = 0.0892;
  sigma_2(1, 0) = 0.3610;
  sigma_2(1, 1) = 0.0710;
  sigma_2(1, 2) = 0.0691;

  ANALYTICS_LOG_INFO("Input sigma_2:");
  std::cout << sigma_2 << std::endl;

  // Step 2: Declare an object of hmm and initialize the parameters

  HiddenMarkovModelCont<float> hmmc;
  hmmc.set_transit_matrix(A);
  hmmc.set_init_distribution(p);
  hmmc.set_emission_mu(mu);
  hmmc.set_emission_sigma(sigma_2);

  hmmc.set_max_iter(2000);
  hmmc.set_tol(1e-3);

  Mat<float> obs ={{-0.68,  0.78,  0.12, 0.09, -0.05,
                    -0.29, -2.13,  0.23, 0.45, -1.68,
                     0.14, -1.12,  1.15, 0.07, -2.23},
                   {-0.85,  0.34,  0.41, 0.57, -2.21,
                    -0.79, -0.84, -0.09, 0.31, -0.89,
                     0.93, -1.42,  0.71, 0.11, -0.28}};
  obs = obs.t();

  ANALYTICS_LOG_INFO("Number of input observation: " << len);
  ANALYTICS_LOG_INFO("The dimension of input observation: " << num);
  ANALYTICS_LOG_INFO("The first input observation: ");
  std::cout << obs.row(0) << std::endl;

  // Step 3: Fit parameters

  hmmc.Fit(obs);

  ANALYTICS_LOG_INFO("Output transition matrix:");
  std::cout << hmmc.transit_matrix() << std::endl;

  ANALYTICS_LOG_INFO("Output initial distribution:");
  std::cout << hmmc.init_distribution() << std::endl;

  ANALYTICS_LOG_INFO("Estimated mu:");
  std::cout << hmmc.emission_mu() << std::endl;

  ANALYTICS_LOG_INFO("Estimated variance:");
  std::cout << hmmc.emission_sigma() << std::endl;

  ANALYTICS_LOG_INFO("log_likelihood:");
  std::cout << hmmc.log_likelihood() << std::endl;

  // Step 4: Decode optimized hidden sequence

  Mat<int> hid;
  hmmc.Decode(obs, hid);

  ANALYTICS_LOG_INFO("The first optimized hidden sequence: " << num);
  std::cout << hid.row(0) << std::endl;

  int step(1);
  Cube<float> predict_obs, predict_hid;
  hmmc.Predict(obs, hid, predict_obs, predict_hid, step);

  ANALYTICS_LOG_INFO("Predicted hidden states distributeion of the first sequence:");
  std::cout << predict_hid.slice(0) << std::endl;

  ANALYTICS_LOG_INFO("Predicted obs value of the first sequence:");
  std::cout << predict_obs.slice(0) << std::endl;

  ANALYTICS_LOG_INFO("Predicted hidden states distributeion of the last sequence:");
  std::cout << predict_hid.slice(num - 1) << std::endl;

  ANALYTICS_LOG_INFO("Predicted obs value of the last sequence:");
  std::cout << predict_obs.slice(num - 1) << std::endl;
}
