#include <vector>
#include "catch.hpp"
#include "analytics/test.h"
#include "analytics/svr.h"
using namespace std;

using namespace analytics;

double SVRR2(arma::Mat<double> &x1, arma::Mat<double> &x2) {
  double up = 0;
  double down = 0;
  for (size_t i = 0; i < x1.size(); ++i) {
    up += (x1(i, 0) - x2(i, 0)) * (x1(i, 0) - x2(i, 0));
    down += x2(i, 0) * x2(i, 0);
  }
  return up / down;
}

TEST_CASE("SVR test", "[svr]") {
  int sz = 5000;
  int N = 1000;
  mat x1(sz, N);
  x1.randu();
  mat y1(sz, 1);
  y1.randu();
  mat x2(sz, N);
  x2.randu();
  mat y2(sz, 1);
  y2.randu();
  for (int i = 0; i < sz; ++i) {
    y1(i, 0) += x1(i, 0) * x1(i, 1) + x1(i, 3);
    y2(i, 0) += x2(i, 0) * x2(i, 1) + x2(i, 3);
  }

  vector<double> Cs = {0.0001, 0.0002, 0.0003, 0.0005, 0.0007, 0.0009, 0.0011, 0.0020, 0.0030, 0.0040, 0.005};
  for (auto &it : Cs) {
    SVR<double> svr;
    svr.set_num_iterations(5);
    svr.set_C(it);
    svr.Fit(x1, y1);
    arma::Mat<double> pyfm(x2.size(), 1);
    svr.Predict(x2, pyfm);
    double aucf = SVRR2(pyfm, y2);  // out sample

    arma::Mat<double> pyim(x1.size(), 1);
    svr.Predict(x1, pyim);
    double auci = SVRR2(pyim, y1);  // in sample
    ANALYTICS_LOG_INFO("C=" << it << " outsample score:" << aucf << " insample score:" << auci);
  }

  SVR<float> r;
  TEST_REGRESSOR(r);
}
