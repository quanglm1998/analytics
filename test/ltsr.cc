/*********************************************************
 * File       : ltsr.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 27 Aug 2018 04:43:19 PM SGT
 * Revision   : none
 *
 *********************************************************/


#include "catch.hpp"

#include "analytics/test.h"
#include "analytics/ltsr.h"
using namespace std;
using namespace analytics;

TEST_CASE("Least trimmed square regression test", "[least trimmed square regression]") {
  Mat<float> A(5, 3);
  Mat<float> b(5, 1);

  A(0, 0) = 1; A(0, 1) = 2; A(0, 2) = 9;
  A(1, 0) = 0; A(1, 1) = 2; A(1, 2) = 0;
  A(2, 0) = 1; A(2, 1) = 2; A(2, 2) = 7;
  A(3, 0) = 3; A(3, 1) = 3; A(3, 2) = 0;
  A(4, 0) = 9; A(4, 1) = 8; A(4, 2) = 7;

  b(0, 0) = 1.1;
  b(1, 0) = 2.1;
  b(2, 0) = 4.3;
  b(3, 0) = 8.4;
  b(4, 0) = 2.3;

  int path_num(3), select_num(1);

  LeastTrimmedSquareRegressor<float> ls(path_num, select_num);
  ls.Fit(A, b);

  Mat<float> x = ls.coefficients();
  Mat<float> y = ls.intercepts();

  ANALYTICS_LOG_INFO("------------ Least trimmed square Regression: with intercept ----------");
  ANALYTICS_LOG_INFO("Coefficients: " << x.t());
  ANALYTICS_LOG_INFO("Intercepts: " << y);

  Mat<float> p;
  ls.Predict(A.head_rows(2), p);
  ANALYTICS_LOG_INFO("Predict  : " << p.t());
}

TEST_CASE("Least trimmed square regression common", "[least trimmed square regression]") {
  LeastTrimmedSquareRegressor<float> ls(20, 5);
  TEST_REGRESSOR(ls);
}
