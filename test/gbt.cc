#include <vector>
#include "catch.hpp"
#include "analytics/test.h"
#include "analytics/gbt.h"
using namespace std;
using namespace analytics;

double GBTR2(arma::Mat<double> &x1, arma::Mat<double> &x2) {
  double up = 0;
  double down = 0;
  for (size_t i = 0; i < x1.size(); ++i) {
    up += (x1(i, 0) - x2(i, 0)) * (x1(i, 0) - x2(i, 0));
    down += x2(i, 0) * x2(i, 0);
  }
  return up / down;
}

TEST_CASE("gradient boosting tree test", "[gradient boosting tree]") {
  int sz = 5000;
  int N = 10;
  mat x1(sz, N);
  x1.randu();
  mat y1(sz, 1);
  y1.randu();
  mat x2(sz, N);
  x2.randu();
  mat y2(sz, 1);
  y2.randu();
  for (int i = 0; i < sz; ++i) {
    y1(i, 0) += x1(i, 0) * x1(i, 1) + x1(i, 3);
    y2(i, 0) += x2(i, 0) * x2(i, 1) + x2(i, 3);
  }
  vector<int> depths = {1, 2, 3, 4, 5, 6, 7};
  for (auto &it : depths) {
    GradientBoostingTreeRegressor<double> dt;
    dt.set_num_trees(100);  // set tree number
    dt.set_max_level(it);  // set max depth
    dt.Fit(x1, y1);
    arma::Mat<double> pyfm(x2.size(), 1);
    dt.Predict(x2, pyfm);
    double aucf = GBTR2(pyfm, y2);  // out sample
    arma::Mat<double> pyim(x1.size(), 1);
    dt.Predict(x1, pyim);
    double auci = GBTR2(pyim, y1);  // in sample
    ANALYTICS_LOG_INFO("depth=" << it << " outsample score:" << aucf << " insample score:" << auci);
  }
  GradientBoostingTreeRegressor<float> r;
  TEST_REGRESSOR(r);
}
