/*********************************************************
 * File       : hurst.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 21 Mar 2019 10:24:32 AM SGT
 * Revision   : none
 *
 *********************************************************/

#include "catch.hpp"

#include "analytics/test.h"
#include "analytics/hurst.h"

using namespace analytics;
using namespace std;

TEST_CASE("HurstExponent", "[hurst exponent]") {
  Random rand(2);

  int len(100), min_interval_num(2), min_interval_len(5);

  // Randam array case
  vector<double> v1(len);
  for (int i = 0; i < len; ++i) {
    double v = rand.randu();
    v1[i] = v;
  }

  ANALYTICS_LOG_INFO("Random case:");
  for (int i = 0; i < len; ++i) {
    cout << v1[i] << " ";
  }
  cout << endl;

  ANALYTICS_LOG_INFO("Hurst exponent: " <<
                     HurstExponent<double>::compute(v1, min_interval_num, min_interval_len));

  // Trend array case
  vector<double> v2(len);
  for (int i = 0; i < len; ++i) {
    double v = 1.0 / len * i;
    v2[i] = v;
  }

  ANALYTICS_LOG_INFO("Trend case:");
  for (int i = 0; i < len; ++i) {
    cout << v2[i] << " ";
  }
  cout << endl;

  ANALYTICS_LOG_INFO("Hurst exponent: " <<
                     HurstExponent<double>::compute(v2, min_interval_num, min_interval_len));

  // Identical array case
  vector<double> v3(len);
  for (int i = 0; i < len; ++i) {
    double v = 1.0 / len;
    v3[i] = v;
  }

  ANALYTICS_LOG_INFO("Identical case:");
  for (int i = 0; i < len; ++i) {
    cout << v3[i] << " ";
  }
  cout << endl;

  ANALYTICS_LOG_INFO("Hurst exponent: " <<
                     HurstExponent<double>::compute(v3, min_interval_num, min_interval_len));


  // Reversal array case
  vector<double> v4(len);
  for (int i = 0; i < len; ++i) {
    double v = i % 2 + rand.randu() / 5;
    v4[i] = v;
  }

  ANALYTICS_LOG_INFO("Reversal case:");
  for (int i = 0; i < len; ++i) {
    cout << v4[i] << " ";
  }
  cout << endl;

  ANALYTICS_LOG_INFO("Hurst exponent: " <<
                     HurstExponent<double>::compute(v4, min_interval_num, min_interval_len));
}


