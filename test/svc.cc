#include <vector>
#include "catch.hpp"
#include "analytics/test.h"
#include "analytics/svc.h"
using namespace std;

using namespace analytics;

template<typename real>
int signum(real x) {
  if (x > 0) return 1;
  return -1;
}

template<typename real>
double SRCR2(arma::Mat<real> &x1, arma::Mat<real> &x2) {
  double up = 0;
  double down = 0;
  for (size_t i = 0; i < x1.size(); ++i) {
    up += (signum(x1(i, 0)) - signum(x2(i, 0))) * (signum(x1(i, 0)) - signum(x2(i, 0)));
    down += signum(x2(i, 0)) * signum(x2(i, 0));
  }
  return up / down;
}

TEST_CASE("SVC test", "[svc]") {
  int sz = 5000;
  int N = 1000;
  mat x1(sz, N);
  x1.randu();
  mat y1f(sz, 1);
  // y1f.randu();
  y1f.zeros();
  arma::Mat<uword> y1(sz, 1);
  mat x2(sz, N);
  x2.randu();
  mat y2f(sz, 1);
  // y2f.randu();
  y2f.zeros();
  arma::Mat<uword> y2(sz, 1);
  for (int i = 0; i < sz; ++i) {
    y1f(i, 0) += x1(i, 0) + 5 * x1(i, 1) - 10 * x1(i, 3);
    y1(i, 0) = (signum(y1f(i, 0)) + 1) / 2;
    y2f(i, 0) += x2(i, 0) + 5 * x2(i, 1) - 10 * x2(i, 3);
    y2(i, 0) = (signum(y2f(i, 0)) + 1) / 2;
  }

  vector<double> Cs = {0.0001, 0.0002, 0.0003, 0.0005, 0.0007, 0.0009, 0.0011, 0.0020,
    0.0030, 0.0040, 0.005, 0.006, 0.007, 0.008, 0.009};
  for (auto &it : Cs) {
    SVMClassifier<double> svc;
    svc.set_num_iterations(500);
    svc.set_Cp(it);
    svc.set_Cn(it);
    svc.Fit(x1, y1);
    arma::Mat<uword> pyfm(x2.size(), 1);
    svc.Predict(x2, pyfm);
    double aucf = SRCR2(pyfm, y2);  // out sample

    arma::Mat<uword> pyim(x1.size(), 1);
    svc.Predict(x1, pyim);
    double auci = SRCR2(pyim, y1);  // in sample

    ANALYTICS_LOG_INFO("C=" << it << " outsample score:" << aucf << " insample score:" << auci);
    // ANALYTICS_LOG_INFO("C=" << it << " weight: " << svc.weight().t());
  }

  SVMClassifier<float> r;
  TEST_CLASSIFIER(r);
}
