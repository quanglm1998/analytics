/*********************************************************
 * File       : example.cpp
 * Author     : hyu
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 17 Jan 2019 11:21:34 AM +08
 * Revision   : none
 *
 *********************************************************/

#include "catch.hpp"

#include "analytics/test.h"
#include "analytics/entropy.h"

using namespace analytics;
using namespace std;

TEST_CASE("EntropyModel", "[entropy model]") {
  Random rand(2);

  // instance for discrete case:
  ANALYTICS_LOG_INFO("Discrete case:");

  int n(100);
  Col<char> v_t1(n);
  for (int i = 0; i < n; ++i) {
    if (rand.randi() % 3 == 0) {
      v_t1(i) = 'u';
    } else if (rand.randi() % 3 == 1) {
      v_t1(i) = 'd';
    } else {
      v_t1(i) = 'f';
    }
  }

  ANALYTICS_LOG_INFO("array in test: ");
  for (int i = 0; i < n; ++i) {
    cout << v_t1(i) << " ";
  }
  cout << endl;

  std::vector<char> list = {'f', 'd', 'u'};
  ANALYTICS_LOG_INFO("state list: ");
  for (int i = 0; i < static_cast<int>(list.size()); ++i) {
    cout << list[i] << " ";
  }
  cout << endl;

  EntropyModel<char> em_t1;
  em_t1.set_state_list(list);
  ANALYTICS_LOG_INFO("entropy " << em_t1.ComputeEntropy(v_t1, 2) <<
                     ", forecast " << em_t1.Predict(v_t1, 2));
  ANALYTICS_LOG_INFO("Renyi entropy: ");
  ANALYTICS_LOG_INFO("order 0: " << em_t1.ComputeRenyiEntropy(v_t1, 2, 0));
  ANALYTICS_LOG_INFO("order 0.5: " << em_t1.ComputeRenyiEntropy(v_t1, 2, 0.5));
  ANALYTICS_LOG_INFO("order 1: " << em_t1.ComputeRenyiEntropy(v_t1, 2, 1));
  ANALYTICS_LOG_INFO("order 2: " << em_t1.ComputeRenyiEntropy(v_t1, 2, 2));
  ANALYTICS_LOG_INFO("order 100: " << em_t1.ComputeRenyiEntropy(v_t1, 2, 100));

  std::cout << std::endl;

  //  instance for continuous case:
  ANALYTICS_LOG_INFO("Continuous case:");

  int m(50);
  Col<float> v_t2(m);
  for (int i = 0; i < m; ++i) {
    v_t2(i) = rand.randu();
  }

  ANALYTICS_LOG_INFO("array in test: ");
  for (int i = 0; i < m; ++i) {
    cout << v_t2[i] << " ";
  }
  cout << endl;

  EntropyModelCont<float> em_t2;
  ANALYTICS_LOG_INFO("entropy " << em_t2.ComputeEntropy(v_t2, 2) <<
                     ", forecast " << em_t2.Predict(v_t2, 2));

  std::cout << std::endl;
}
