/*********************************************************
 * File       : emd.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 06 Aug 2018 06:49:18 PM SGT
 * Revision   : none
 *
 *********************************************************/


#include "catch.hpp"

#include "analytics/test.h"
#include "analytics/emd.h"
using namespace std;
using namespace analytics;

TEST_CASE("Empirical Mode Decomposition test", "[empirical mode decomposition]") {
  int m(100);
  float step(10.0 / m), pi(4.0 * atan(1.0));

  Row<float> signal(m + 1);
  for (int i = 0; i <= m; ++i) {
    float x = i * step;
    signal(i) = 0.5 * x + sin(pi * x) + sin(2 * pi * x) + sin(6 * pi * x);
  }
  ANALYTICS_LOG_INFO("Input signal: ");
  ANALYTICS_LOG_INFO(signal);

  int order(4), max_iter(5), locality(0);

  EmpiricalModeDecomposition<float> emd;
  emd.Init(order, max_iter, locality);
  emd.Decompose(signal);

  Mat<float> imf = emd.Imf();
  for (int i = 0; i < imf.n_rows; ++i) {
    ANALYTICS_LOG_INFO("Implicit mode function - " << i << " : ");
    ANALYTICS_LOG_INFO(imf.row(i));
  }
}
