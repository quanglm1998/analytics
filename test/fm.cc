/*********************************************************
 * File       : fm.cc
 * Author     : yliu
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Tue 28 Feb 2017 06:26:02 PM SGT
 * Revision   : none
 *
 *********************************************************/


#include "catch.hpp"

#include "analytics/test.h"
#include "analytics/fm.h"
using namespace std;
using namespace analytics;

TEST_CASE("Factorization machine test", "[factorization machine]") {
  Mat<float> A(5, 3);
  Mat<float> b(5, 1);

  A(0, 0) = 1; A(0, 1) = 2; A(0, 2) = 9;
  A(1, 0) = 0; A(1, 1) = 2; A(1, 2) = 0;
  A(2, 0) = 1; A(2, 1) = 2; A(2, 2) = 7;
  A(3, 0) = 3; A(3, 1) = 3; A(3, 2) = 0;
  A(4, 0) = 9; A(4, 1) = 8; A(4, 2) = 7;

  b(0, 0) = 1.1;
  b(1, 0) = 2.1;
  b(2, 0) = 4.3;
  b(3, 0) = 8.4;
  b(4, 0) = 2.3;

  FM<float> fm;
  // fm.Initiate(3, 2, 1, 0.00004, 0.0, 15000, 0);
  // fm.set_max(8.4);
  // fm.set_min(1.1);
  fm.Fit(A, b);

  ANALYTICS_LOG_INFO("--------------- Factorization -------------");

  Mat<float> p;
  fm.Predict(A, p);
  ANALYTICS_LOG_INFO("Target   : " << b.t());
  ANALYTICS_LOG_INFO("Predict  : " << p.t());
  ANALYTICS_LOG_INFO("Intercept  : " << "   " << fm.w0());
  ANALYTICS_LOG_INFO("Exposures  : " << fm.w1().t());
  ANALYTICS_LOG_INFO("Interacts  : " << "\n" << fm.w2());
}

TEST_CASE("Factorization machine common", "[factorization machine]") {
  FM<float> fm;
  // fm.Initiate(2, 2, 0.8, 0.00004, 0.0, 20000, 0);
  TEST_REGRESSOR(fm);
}
