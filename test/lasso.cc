/*********************************************************
 * File       : lasso.cc
 * Author     : jhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 02 Mar 2017 04:50:41 PM SGT
 * Revision   : none
 *
 *********************************************************/


#include "catch.hpp"
#include "analytics/test.h"
#include "analytics/lasso.h"

using namespace analytics;

TEST_CASE("lasso test", "[lasso]") {
  Mat<float> A(5, 3);
  Mat<float> b(5, 1);

  A(0, 0) = 1; A(0, 1) = 2; A(0, 2) = 9;
  A(1, 0) = 0; A(1, 1) = 2; A(1, 2) = 0;
  A(2, 0) = 1; A(2, 1) = 2; A(2, 2) = 7;
  A(3, 0) = 3; A(3, 1) = 3; A(3, 2) = 0;
  A(4, 0) = 9; A(4, 1) = 8; A(4, 2) = 7;

  b(0, 0) = 1.1;
  b(1, 0) = 2.1;
  b(2, 0) = 4.3;
  b(3, 0) = 8.4;
  b(4, 0) = 2.3;

  Lasso<float> r;
  r.set_intercept(true);
  r.set_tol(0.000001);
  r.set_lambda(0.1);

  r.Fit(A, b);
  Mat<float> x = r.coefficients();
  Mat<float> y = r.intercepts();
  ANALYTICS_LOG_INFO("--------------- Lasso Regression: with intercept -------------");
  ANALYTICS_LOG_INFO("Coefficients: " << x.t());
  ANALYTICS_LOG_INFO("Intercepts: " << y);

  Mat<float> p;
  r.Predict(A.head_rows(2), p);
  ANALYTICS_LOG_INFO("Predict  : " << p.t());

  r.set_intercept(false);
  r.Fit(A, b);

  x = r.coefficients();
  y = r.intercepts();
  ANALYTICS_LOG_INFO("--------------- Lasso Regression: without intercept -------------");
  ANALYTICS_LOG_INFO("Coefficients: " << x.t());
  ANALYTICS_LOG_INFO("Intercepts: " << y);

  r.Predict(A.head_rows(2), p);
  ANALYTICS_LOG_INFO("Predict  : " << p.t());
}
