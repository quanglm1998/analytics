#include <exception>
#include <iostream>

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include "analytics/logging.h"

void DumpException() {
  static bool thrown = false;

  try {
    // try once to re-throw currently active exception
    if (!thrown) {
      thrown = true;
      throw;
    }
  } catch (const std::exception &e) {
    std::cerr << "Caught unhandled exception. what(): " << e.what() << std::endl;
  } catch (...) {
    std::cerr << "Caught unknown/unhandled exception." << std::endl;
  }

  std::abort();
}


int main(int argc, char **argv) {
  analytics::ConfigLogger("analytics", "", "debug");
  std::set_terminate(DumpException);
  ANALYTICS_LOG_INFO("Running tests");
  return Catch::Session().run(argc, argv);
}
