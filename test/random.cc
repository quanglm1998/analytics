/*********************************************************
 * File       : random.cc
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 02 Mar 2017 03:03:45 PM SGT
 * Revision   : none
 *
 *********************************************************/

#include "catch.hpp"
#include "analytics/common.h"

using namespace analytics;

TEST_CASE("randi", "[random]") {
  Random r(0);
  double sum = 0;
  const int N = 1000000;
  const int m = 100;

  float f = r.randu();

  for (int i = 0; i < N; i++) {
    sum += r.randi(m);
  }
  REQUIRE(sum / N == Approx(m / 2).epsilon(0.05));
}

TEST_CASE("randu fill", "[random]") {
  Random r(0);
  Mat<double> m(10, 10);
  r.randu(m);
  for (uword i = 0; i < m.n_cols; i++) {
    for (uword j = 0; j < m.n_rows; j++) {
      REQUIRE(m(j, i) >= 0);
      REQUIRE(m(j, i) < 1);
    }
  }
}

TEST_CASE("randi fill", "[random]") {
  Random r(0);
  Mat<int> m(10, 10);
  r.randi(m, 0, 100);
  for (uword i = 0; i < m.n_cols; i++) {
    for (uword j = 0; j < m.n_rows; j++) {
      REQUIRE(m(j, i) >= 0);
      REQUIRE(m(j, i) < 100);
    }
  }
}

TEST_CASE("shuffle matrix", "[random]") {
  Random r(0);
  mat m(10, 10);
  r.shuffle(m);
}

template <typename Vec>
bool IsSorted(const Vec v) {
  for (uword i = 1; i < v.size(); i++) {
    if (v(i) > v(i - 1)) {
      return false;
    }
  }
  return true;
}

TEST_CASE("shuffle vector", "[random]") {
  Random r(0);
  Row<double> row(10);
  for (uword i = 0; i < row.size(); i++) row(i) = i;
  r.shuffle(row);
  REQUIRE_FALSE(IsSorted(row));

  Col<int> col(10);
  for (uword i = 0; i < col.size(); i++) col(i) = i;
  r.shuffle(col);
  REQUIRE_FALSE(IsSorted(col));
}

TEST_CASE("shuffled seq", "[random]") {
  Random r(0);
  const int N = 10;
  Col<uword> seq = r.shuffled_seq(N);
  uvec check(N);
  check.zeros();
  seq.for_each([&check](uword i) { check(i) = 1; });
  check.for_each([&](uword v) { REQUIRE(v == 1); });
}
