/*********************************************************
 * File       : analytics_test.h
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 24 Feb 2017 09:29:51 AM SGT
 * Revision   : none
 *
 *********************************************************/


#ifndef DTL_ANALYTICS_TEST_H
#define DTL_ANALYTICS_TEST_H

#include "analytics/ml.h"
#include "catch.hpp"

namespace analytics {

template <typename real>
void TestRegressorSimple(Regressor<real> &regressor) {
  using namespace arma;
  Mat<real> x = join_rows(
      linspace<Col<real>>(10, 15),
      linspace<Col<real>>(50, 100));
  Mat<real> y = x.col(0) + x.col(1) * 0.5;
  regressor.Fit(x, y);
  Mat<real> s = join_rows(
      linspace<Col<real>>(100, 110, 5),
      linspace<Col<real>>(1, 10, 5));
  Mat<real> r = regressor.Predict(s);

  REQUIRE((r.n_rows == 5));
  REQUIRE((r.n_cols == 1));
}

template <typename real>
void TestRegressorThrow(Regressor<real> &regressor) {
  using namespace arma;

  // mismatched rows between train and targets
  REQUIRE_THROWS(regressor.Fit(Mat<real>(5, 4), Mat<real>(3, 1)));

  // mismatched cols between fit and predict
  regressor.Fit(Mat<real>(5, 4, fill::ones), Mat<real>(5, 1, fill::ones));
  REQUIRE_THROWS(regressor.Predict(Mat<real>(5, 3)));

  // NAN
  REQUIRE_THROWS(regressor.Fit(Mat<real>({{NAN, 1.0}}), Row<real>({0.0})));
  REQUIRE_THROWS(regressor.Fit(Mat<real>({{2.0, 0.0}, {1.0, NAN}}), Row<real>({1.0, 0.0})));
  REQUIRE_THROWS(regressor.Fit(Mat<real>({{2.0, 0.0}, {1.0, 3.0}}), Row<real>({1.0, NAN})));
  REQUIRE_THROWS(regressor.Predict(Mat<real>({{NAN, 1.0}})));
  REQUIRE_THROWS(regressor.Predict(Mat<real>({{1.0, NAN}})));
  REQUIRE_THROWS(regressor.Predict(Mat<real>({{NAN, NAN}})));
}

template <typename real>
void TestClassiferSimple(Classifier<real> &classifer) {
  using namespace arma;
  Mat<real> x = join_rows(
      linspace<Col<real>>(10, 15, 10),
      linspace<Col<real>>(50, 100, 10));
  Col<uword> y = {1, 1, 1, 1, 2, 2, 2, 3, 3, 3};
  classifer.Fit(x, y);
  Mat<real> s = join_rows(
      linspace<Col<real>>(100, 110, 5),
      linspace<Col<real>>(1, 10, 5));
  Mat<uword> r = classifer.Predict(s);

  REQUIRE((r.n_rows == 5));
  REQUIRE((r.n_cols == 1));
}

}

#define TEST_REGRESSOR(X) SECTION("common regressor tests") { \
  analytics::TestRegressorSimple(X); \
  analytics::TestRegressorThrow(X); \
}

#define TEST_CLASSIFIER(X) SECTION("common classifier tests") { \
  analytics::TestClassiferSimple(X); \
}

#endif
