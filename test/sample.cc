/*********************************************************
 * File       : sample.cc
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 23 Feb 2017 04:15:27 PM SGT
 * Revision   : none
 *
 *********************************************************/

#include "catch.hpp"
#include "analytics/test.h"

using namespace analytics;

template <typename real = float>
class SampleRegressor : public Regressor<real> {
 protected:
  void FitImpl(const Mat<real> &, const Mat<real> &) override {}

  void PredictImpl(const Mat<real> &samples, Mat<real> &results) override {
    results = arma::sum(samples, 1);
  }
};

template <typename real = float>
class SampleClassifier : public Classifier<real> {
 protected:
  void FitImpl(const Mat<real> &, const Mat<uword> &) override {}

  void PredictImpl(const Mat<real> &samples, Mat<uword> &results) override {
    results = arma::linspace<Col<uword>>(1, samples.n_rows, samples.n_rows);
  }
};

TEST_CASE("Sampe test", "[sample]") {
  REQUIRE(1 + 1 == 2);
  REQUIRE(2 + 2 == 4);

  SampleRegressor<float> r;
  TEST_REGRESSOR(r);

  SampleClassifier<float> c;
  TEST_CLASSIFIER(c);
}

TEST_CASE("Sparse matrix", "[sample]") {
  SampleRegressor<float> r;
  SpMat<float> A(5, 5);
  Col<float> B(5);
  REQUIRE_THROWS_AS(r.Fit(A, B), AnalyticsError);
  REQUIRE_THROWS_AS(r.Predict(A, B), AnalyticsError);
}
