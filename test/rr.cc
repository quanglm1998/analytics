/*********************************************************
 * File       : ridge_regress.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 24 Feb 2017 10:41:08 AM
 * Revision   : none
 *
 *********************************************************/


#include "catch.hpp"

#include "analytics/test.h"
#include "analytics/rr.h"
using namespace std;
using namespace analytics;

TEST_CASE("Ridge regression test", "[ridge regression]") {
  Mat<float> A(5, 3);
  Mat<float> b(5, 1);

  A(0, 0) = 1; A(0, 1) = 2; A(0, 2) = 9;
  A(1, 0) = 0; A(1, 1) = 2; A(1, 2) = 0;
  A(2, 0) = 1; A(2, 1) = 2; A(2, 2) = 7;
  A(3, 0) = 3; A(3, 1) = 3; A(3, 2) = 0;
  A(4, 0) = 9; A(4, 1) = 8; A(4, 2) = 7;

  b(0, 0) = 1.1;
  b(1, 0) = 2.1;
  b(2, 0) = 4.3;
  b(3, 0) = 8.4;
  b(4, 0) = 2.3;

  RidgeRegressor<float> ls;
  ls.set_lambda(0.1f);
  ls.Fit(A, b);

  Mat<float> x = ls.coefficients();
  Mat<float> y = ls.intercepts();

  ANALYTICS_LOG_INFO("--------------- Ridge Regression: with intercept -------------");
  ANALYTICS_LOG_INFO("Coefficients: " << x.t());
  ANALYTICS_LOG_INFO("Intercepts: " << y);

  Mat<float> p;
  ls.Predict(A.head_rows(2), p);
  ANALYTICS_LOG_INFO("Predict  : " << p.t());

  ls.set_intercept(false);
  ls.Fit(A, b);

  x = ls.coefficients();
  y = ls.intercepts();

  ANALYTICS_LOG_INFO("--------------- Ridge Regression: without intercept -------------");
  ANALYTICS_LOG_INFO("Coefficients: " << x.t());
  ANALYTICS_LOG_INFO("Intercepts: " << y);

  ls.Predict(A.head_rows(2), p);
  ANALYTICS_LOG_INFO("Predict  : " << p.t());
}

TEST_CASE("Ridge regression common", "[ridge regression]") {
  RidgeRegressor<float> ls;
  ls.set_lambda(0.1f);
  TEST_REGRESSOR(ls);
}
