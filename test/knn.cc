/*********************************************************
 * File       : lasso.cc
 * Author     : jhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 02 Mar 2017 04:50:41 PM SGT
 * Revision   : none
 *
 *********************************************************/


#include "catch.hpp"
#include "analytics/test.h"
#include "analytics/knn.h"


TEST_CASE("knn test",  "[lasso]") {
  using namespace std;
  using namespace analytics;
  /* using namespace arma; */
  cout << "begin test" << endl;

  Mat<float> X(7, 3);
  Mat<uword> y(7, 1);
  X(0, 0) = 0.5; X(0, 1) = 0.5; X(0, 2) = 0.73;
  X(1, 0) = 0.2; X(1, 1) = 0.4; X(1, 2) = 0.18;
  X(2, 0) = 0.45; X(2, 1) = 0.23; X(2, 2) = 0.18;
  X(3, 0) = 0.47; X(3, 1) = 0.83; X(3, 2) = 0.91;
  X(4, 0) = 0.43; X(4, 1) = 0.27; X(4, 2) = 0.71;
  X(5, 0) = 0.33; X(5, 1) = 0.74; X(5, 2) = 0.13;
  X(6, 0) = 0.78; X(6, 1) = 0.33; X(6, 2) = 0.55;
  y(0, 0) = 0; y(1, 0) = 1; y(2, 0) = 1; y(3, 0) = 0; y(4, 0) = 2; y(5, 0) = 1; y(6, 0) = 2;
  KNN<float> knn;
  knn.set_max_nodes(3);
  knn.set_min_nodes(1);
  knn.setTopk(3);
  knn.Fit(X, y);

  Mat<float> test(2, 3);
  test(0, 0) = 0.3; test(0, 1) = 0.2; test(0, 2) = 0.8;
  test(1, 0) = 0.3; test(1, 1) = 0.2; test(1, 2) = 0.1;
  Mat<uword> result;
  knn.Predict(test, result);
  cout << result << endl;


  /* int col = 20; */
  /* int row = 10000; */
  /* Mat<float> x(row,  col); */
  /* Mat<uword> y(row,  2); */
  /* for (int i=0; i<row; i++) { */
  /*   for (int j=0; j<col; j++) { */
  /*     x(i, j) = (float)rand()/RAND_MAX*100; */
  /*   } */
  /*   y(i, 0) = i%100; */
  /*   y(i, 1) = (i*i)%100; */
  /* } */

  /* KNN<float> knn; */
  /* knn.setMaxnodes(10); */
  /* knn.setMinnodes(5); */
  /* knn.Fit(x, y); */
  /* Mat<float> test(10, col); */
  /* Mat<uword> result(10, 2); */

  /* for (int i=0; i<10; i++) { */
  /*   for (int j=0; j<col; j++) { */
  /*     test(i, j) = (float)rand()/RAND_MAX*100; */
  /*   } */
  /* } */

  /* knn.Predict(test,  result); */

  /* cout << test << endl; */
  /* cout << result <<endl; */
}
