#include <vector>
#include "catch.hpp"
#include "analytics/test.h"
#include "analytics/mlp.h"
using namespace std;
using namespace analytics;

double MLPR2(arma::Mat<double> &x1, arma::Mat<double> &x2) {
  double up = 0;
  double down = 0;
  for (size_t i = 0; i < x1.size(); ++i) {
    up += (x1(i, 0) - x2(i, 0)) * (x1(i, 0) - x2(i, 0));
    down += x2(i, 0) * x2(i, 0);
  }
  return up / down;
}

TEST_CASE("mlp", "[mlp]") {
  int sz = 10000;
  uword N = 10;
  uword catnum = 10;
  assert(N >= catnum);  // necessary in this example
  // 0. prepare data
  mat x1(sz, N);
  mat x2(sz, N);
  x1.randu();
  x2.randu();
  mat y1(sz, catnum);
  mat y2(sz, catnum);
  y1.randu();
  y2.randu();
  for (int i = 0; i < sz; ++i) {
    int maxi = 0;
    double maxv = x1(i, 0);
    for (uword j = 0; j < catnum; ++j) {
      if (x1(i, j) > maxv) {
        maxv = x1(i, j);
        maxi = j;
      }
      y1(i, j) = 0.0;
    }
    y1(i, maxi) = 1.0;
    maxi = 0;
    maxv = y2(i, 0);
    for (uword j = 0; j < catnum; ++j) {
      if (x2(i, j) > maxv) {
        maxv = x2(i, j);
        maxi = j;
      }
      y2(i, j) = 0.0;
    }
    y2(i, maxi) = 1.0;
  }

  // 1. prepare mlp
  class MLPRegressor<double> mlp;
  vector<uword> layer = {N, 100, 100, 100, catnum};
  int epoch = 10;
  int batchsize = 10;
  double learning_rate = 0.0001;
  string activation_function("relu");
  string output_function("sigm");
  mlp.set_layer(layer);
  mlp.set_epoch(epoch);
  mlp.set_batch_sz(batchsize);
  mlp.set_learning_rate(learning_rate);
  mlp.set_activation_function(activation_function);
  mlp.set_output_function(output_function);

  // 2. fit
  mlp.Fit(x1, y1);

  // 3. in sample
  mat py1;
  mlp.Predict(x1, py1);
  double up = 0;
  double down = 0;
  for (uword z = 0; z < py1.n_rows; ++z) {
    double maxv = py1(z, 0);
    int maxi = 0;
    for (uword p = 0; p < py1.n_cols; ++p) {
      if (py1(z, p) > maxv) {
        maxv = py1(z, p);
        maxi = p;
      }
    }
    if (y1(z, maxi) > 0) up += 1;
    down += 1;
  }
  ANALYTICS_LOG_INFO("in sample correct ratio:" << up / down);

  // 4. out sample
  mat py2;
  mlp.Predict(x2, py2);
  up = 0;
  down = 0;
  for (uword z = 0; z < py2.n_rows; ++z) {
    double maxv = py2(z, 0);
    int maxi = 0;
    for (uword p = 0; p < py2.n_cols; ++p) {
      if (py2(z, p) > maxv) {
        maxv = py2(z, p);
        maxi = p;
      }
    }
    if (y2(z, maxi) > 0) up += 1;
    down += 1;
  }
  ANALYTICS_LOG_INFO("out sample correct ratio:" << up / down);

  MLPRegressor<float> r;
  TEST_REGRESSOR(r);
}
