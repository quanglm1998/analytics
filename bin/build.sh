#!/bin/bash

set -e

build_type=Release
catch_abort_on_exception=false
while getopts "da" opt; do
  case $opt in
    d)
      build_type=Debug
      ;;
    a)
      catch_abort_on_exception=true
      ;;
  esac
done

dir=$(dirname $(dirname $(readlink -e $0)))
echo "Project directory: $dir"
echo "Build type: $build_type"

cd $dir
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=$build_type -DCATCH_ABORT_ON_EXCEPTION=$catch_abort_on_exception ..
make -j8
