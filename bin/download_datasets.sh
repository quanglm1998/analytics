#!/bin/bash
set -e

large_datasets=false
while getopts "l" opt; do
  case $opt in
    l)
      large_datasets=true
      ;;
  esac
done

cd $(dirname $(dirname $(readlink -e $0)))

mkdir -p build/data
cd build/data

# Fertility Data Set
if [[ ! -e fertility.data ]]; then
  echo "Downloading Fertility dataset"
  wget -O fertility.data \
    https://archive.ics.uci.edu/ml/machine-learning-databases/00244/fertility_Diagnosis.txt
  sed -i -e "s/N/1/" -e "s/O/2/" fertility.data
else
  echo "Fertility dataset already exists"
fi

# Yacht Hydrodynamics Diagnosis Data Set
if [[ ! -e yacht_hydrodynamics.data ]]; then
  echo "Downloading Yacht hydrodynamics dataset"
  wget -O yacht_hydrodynamics.data \
    https://archive.ics.uci.edu/ml/machine-learning-databases/00243/yacht_hydrodynamics.data
  sed -i -r 's/ +$//g' yacht_hydrodynamics.data
  sed -i -r 's/ +/,/g' yacht_hydrodynamics.data
else
  echo "Yacht hydrodynamics dataset already exists"
fi

if $large_datasets; then
  # Buzz in social media Data Set
  if [[ ! -e buzz ]]; then
    echo "Downloading Buzz dataset"
    wget https://archive.ics.uci.edu/ml/machine-learning-databases/00248/regression.tar.gz
    tar xf regression.tar.gz
    mv regression buzz
    rm regression.tar.gz
  else
    echo "Buzz dataset already exists"
  fi

  # Sensorless Drive Diagnosis Data Set
  if [[ ! -e drive_diagnosis.data ]]; then
    echo "Downloading Drive diagnosis dataset"
    wget -O drive_diagnosis.data \
      https://archive.ics.uci.edu/ml/machine-learning-databases/00325/Sensorless_drive_diagnosis.txt
    sed -i -r 's/ +$//g' drive_diagnosis.data
    sed -i -r 's/ +/,/g' drive_diagnosis.data
  else
    echo "Drive diagnosis dataset already exists"
  fi
fi
