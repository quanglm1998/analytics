#!/bin/bash

cd $(dirname $(dirname $(readlink -e $0)))
./bin/cpplint.py $(find . -name *.h -or -name *.cc)
