#!/bin/bash

set -e

./bin/download_datasets.sh "$@"
./bin/build.sh
cd build
./benchmark "$@"
