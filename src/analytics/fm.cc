/*********************************************************
 * File       : fm.cc
 * Author     : yliu
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 13 Mar 2017 03:22:02 PM SGT
 * Revision   : none
 *
 *********************************************************/


#include "analytics/fm.h"
namespace analytics {

template<typename real>
real FM<real>::PredictOne(const Row<real> &x, Col<real> &s1, Col<real> &s2) {
  if (static_cast<int>(x.n_cols) != p_) {
    ANALYTICS_LOG_ERROR("Wrong number of columns " << x.n_cols << ", assigned is " <<p_);
    throw AnalyticsError("Number of columns doesn't match assigned");
  }
  s1 = (x * u_).t();
  s2 = (x * v_).t();
  real ret = w0_ + arma::dot(w_, x.t()) + arma::dot(s1, s2);

  // truncate the value
  ret = std::min(ret, max_value_);
  ret = std::max(ret, min_value_);
  return ret;
}

template<typename real>
real FM<real>::PredictOne(const Row<real> &x) {
  return PredictOne(x, sumuk_, sumvk_);
}

template<typename real>
void FM<real>::PredictAll(const Mat<real> &x, Mat<real> &y) {
  int m = x.n_rows;
  if (m == 0) return;
  y.zeros(m, 1);
  for (int i = 0; i < m; i++) {
    y(i, 0) = PredictOne(x.row(i));
  }
}

template<typename real>
void FM<real>::LearnOne(const Row<real> &x, real y) {
  real y0 = y;
  real y1 = PredictOne(x, sumuk_, sumvk_);

  real diff = 2 * (y1 - y0);

  // update weights accordingly
  w0_ -= learn_rate_ * (diff + reg0_ * w0_);

  for (int i = 0; i < p_; i++) {
    real &wi = w_(i);
    wi -= learn_rate_ * (diff * x(0, i) + regw_ * wi);
  }

  for (int j = 0; j < k_; j++) {
    for (int i = 0; i < p_; i++) {
      real &uij = u_(i, j);
      real gradu = sumvk_(j) * x(0, i);
      uij -= learn_rate_ * (diff * gradu + regv_ * uij);

      real &vij = v_(i, j);
      real gradv = sumuk_(j) * x(0, i);
      vij -= learn_rate_ * (diff * gradv + regv_ * vij);
    }
  }
}

template<typename real>
void FM<real>::LearnAll(const Mat<real>& x, const Mat<real>& y) {
  if (x.n_cols != p_) {
    Initialize(x.n_cols, k_, stddev_, learn_rate_, reg0_, num_iter_, seed_);
  }
  for (int i = 0; i < num_iter_; i++) {
    Col<uword> order = rng_.shuffled_seq(x.n_rows);
    for (uword j = 0; j < x.n_rows; j++) {
      LearnOne(x.row(order(j)), y(order(j), 0));
    }
  }
}

template class FM<float>;
template class FM<double>;

}
