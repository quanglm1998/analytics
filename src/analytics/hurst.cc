/*********************************************************
 * File       : hurst.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Thu 21 Mar 2019 09:54:25 AM SGT
 * Revision   : none
 *
 *********************************************************/

#include <limits>
#include "analytics/hurst.h"
#include "analytics/lsr.h"

#define HURST_TEMPLATE template<typename real>
#define HURST HurstExponent<real>

namespace analytics {

HURST_TEMPLATE
real HURST::compute(const Col<real> &series, size_t min_interval_num, size_t min_interval_len) {
  size_t series_len = series.n_elem;

  // construct intervals
  size_t num_str(min_interval_num), num_end(series_len / min_interval_len);
  std::vector<size_t> interval_lens;
  for (size_t a = num_str; a <= num_end; ++a) {
    size_t n = series_len / a;
    if (find(interval_lens.begin(), interval_lens.end(), n) == interval_lens.end()) {
      interval_lens.push_back(n);
    }
  }
  size_t num = interval_lens.size();
  Col<real> log_interval_lens(num);
  for (size_t i = 0; i < num; ++i) {
    log_interval_lens(i) = log(interval_lens[i]);
  }

  Col<real> sub(series_len), cum(series_len);
  Col<real> log_RS(interval_lens.size());

  for (size_t i = 0; i < interval_lens.size(); i++) {
    size_t len = interval_lens[i];
    size_t n = series_len / interval_lens[i];

    // compute means first
    Col<real> mean(n);
    for (size_t j = 0; j < n; ++j) {
      mean(j) = 0;
      for (size_t k = 0; k < len; ++k) {
        mean(j) += series(j * len + k);
      }
      mean(j) /= len;
    }

    // demean
    sub.fill(0);
    for (size_t j = 0; j < n; ++j) {
      for (size_t k = 0; k < len; ++k) {
        size_t index = j * len + k;
        sub(index) = series(index) - mean(j);
        if (fabs(sub(index)) < std::numeric_limits<real>::epsilon()) {
          sub(index) = 0;
        }
      }
    }

    // compute std
    Col<real> stddev(n);
    for (size_t j = 0; j < n; ++j) {
      stddev(j) = 0;
      for (size_t k = 0; k < len; ++k) {
        size_t index = j * len + k;
        stddev(j) += sub(index) * sub(index);
      }
      stddev(j) = sqrt(stddev(j) / len);
    }

    // compute RS
    cum.fill(0);
    Col<real> max(n), min(n);
    max.fill(std::numeric_limits<real>::lowest());
    min.fill(std::numeric_limits<real>::max());

    for (size_t j = 0; j < n; ++j) {
      for (size_t k = 0; k < len; ++k) {
        size_t index = j * len + k;
        cum(index) = (k == 0) ? sub(index) : sub(index) + cum(index - 1);
        // record max and min
        max(j) = (k == 0) ? cum(index) : std::max(max(j), cum(index));
        min(j) = (k == 0) ? cum(index) : std::min(min(j), cum(index));
      }
    }

    real RS_sum(0);
    for (size_t j = 0; j < n; j++) {
      real val = max(j) - min(j);
      val = (stddev(j) > std::numeric_limits<real>::epsilon()) ? val / stddev(j) : val;
      RS_sum += val;
    }
    RS_sum /= n;
    log_RS(i) = (RS_sum > std::numeric_limits<real>::epsilon()) ?  log(RS_sum) : NAN;
  }

  // regression on log_RS and log_interval_lens
  // x: log_interval_lens
  // y: log_RS
  return regress(log_interval_lens, log_RS);
}

HURST_TEMPLATE
real HURST::regress(const Col<real> &x, const Col<real> &y) {
  real hurst_ratio(0.5);

  if (x.n_elem != y.n_elem) return hurst_ratio;

  std::vector<size_t> index;
  for (size_t i = 0; i < x.n_elem; ++i) {
    if (std::isnan(x(i)) || std::isnan(y(i))) continue;
    index.push_back(i);
  }

  size_t n = index.size();
  if (n <= 3) return hurst_ratio;

  Col<real> sub_x(n), sub_y(n);
  for (size_t i = 0; i < n; ++i) {
    sub_x(i) = x(index[i]);
    sub_y(i) = y(index[i]);
  }

  LeastSquareRegressor<real> ls;
  ls.Fit(sub_x, sub_y);
  return arma::as_scalar(ls.coefficients());
}

template class HurstExponent<float>;
template class HurstExponent<double>;

}

#undef HURST
#undef HURST_TEMPLATE
