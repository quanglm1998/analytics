/*********************************************************
 * File       : entropy_model.cpp
 * Author     : hyu
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: implemetation of the interface entropy_model.h
 * Created    : Mon 26 Nov 2018 02:33:21 PM +08
 * Revision   : ftang
 *
 *********************************************************/


#include "analytics/entropy.h"

#define ENTROPY_TEMPLATE template<typename data, typename real>
#define ENTROPY EntropyModel<data, real>

namespace analytics {

ENTROPY_TEMPLATE
arma::uvec ENTROPY::MatchSubSeries(const Col<data> &series, const Col<data> &sub) {
  size_t s = sub.n_elem;
  arma::uvec index(series.n_elem);
  for (size_t i = 0; i < series.n_elem; ++i) {
    if (i + s <= series.n_elem) {
      Col<data> sub_series = series.subvec(i, i + s - 1);
      index(i) = all(sub_series == sub);
    } else {
      index(i) = 0;
    }
  }
  return index;
}

ENTROPY_TEMPLATE
real ENTROPY::ComputeProbility(const Col<data> &series, const Col<data> &cond, data state) {
  real total(0), num(0);
  arma::uvec index = MatchSubSeries(series, cond);
  for (size_t i = 0; i < index.n_elem; ++i) {
    size_t loc = i + cond.n_elem + 1;
    if (index(i) && loc < series.n_elem) {
      total++;
      num += (series(loc) == state);
    }
  }
  return (total < 0.1) ? 0 : num / total;
}

ENTROPY_TEMPLATE
real ENTROPY::ComputeEntropy(const Col<data> &seq, int cond_len) {
  real ent(0);
  Col<data> cdt = seq.tail(cond_len);
  for (size_t i = 0; i < state_list_.size(); ++i) {
    real p = ComputeProbility(seq, cdt, state_list_(i));
    ent += (std::abs(p) < 1e-4) ? 0 : - p * log(p);
  }
  return ent;
}

ENTROPY_TEMPLATE
real ENTROPY::ComputeRenyiEntropy(const Col<data> &seq, int cond_len, real q) {
  if (std::abs(q - 1) < 1e-4) {
    return ComputeEntropy(seq, cond_len);
  } else {
    real ent(0);
    Col<data> cdt = seq.tail(cond_len);
    for (size_t i = 0; i < state_list_.size(); ++i) {
      real p = ComputeProbility(seq, cdt, state_list_(i));
      ent += (std::abs(p) < 1e-4) ? 0 : std::pow(p, q);
    }
    ent = log(ent) / (1 - q);
    return ent;
  }
}

ENTROPY_TEMPLATE
data ENTROPY::Predict(const Col<data> &seq, int cond_len) {
  data fore(state_list_(0));
  real pm(0);
  Col<data> cdt = seq.tail(cond_len);
  for (size_t i = 0; i < state_list_.size(); ++i) {
    float p = ComputeProbility(seq, cdt, state_list_(i));
    if (p > pm) {
      pm = p;
      fore = state_list_(i);
    }
  }
  return fore;
}

template class EntropyModel<char, float>;
template class EntropyModel<char, double>;

template class EntropyModel<int, float>;
template class EntropyModel<int, double>;

template class EntropyModel<unsigned, float>;
template class EntropyModel<unsigned, double>;

template class EntropyModel<float, float>;
template class EntropyModel<float, double>;

template class EntropyModel<double, float>;
template class EntropyModel<double, double>;

}

#undef ENTROPY
#undef ENTROPY_TEMPLATE

//////////////////////////////////////////////////////////////////////


#define ENTROPY_CONT_TEMPLATE template<typename data, typename real>
#define ENTROPY_CONT EntropyModelCont<data, real>

namespace analytics {

ENTROPY_CONT_TEMPLATE
Mat<data> ENTROPY_CONT::Series2Mat(int m, const Col<data> &series) {
  Mat<data> v_s(m, 1);
  for (size_t i = 0; i + (m - 1) < series.n_elem; ++i) {
    Col<data> v(m);
    int j(0);
    for (; j < m; ++j) {
      if (std::isnan(series(i + j))) {
        i = i + j;
        break;
      }
      v(j) = series(i + j);
    }
    if (j == m) {
      v_s = join_rows(v_s, v);
    }
  }
  v_s.shed_col(0);
  return v_s;
}

ENTROPY_CONT_TEMPLATE
Col<data> ENTROPY_CONT::Preprocess(const Col<data> &seq) {
  Col<data> series;

  arma::uvec index = find_finite(seq);
  auto m = index.n_elem;
  if (m * 5 < seq.n_elem * 4) {
    series.clear();
    return series;
  }

  series = seq.elem(index);
  data avg_ser = mean(series);
  data std_ser = stddev(series);
  for (size_t i = 0; i < series.n_elem; ++i) {
    if (std::isnan(series(i))) continue;
    series(i) -= avg_ser;
    if (std::abs(std_ser) > 1e-4) {
      series(i) /= std_ser;
    }
  }
  return series;
}

ENTROPY_CONT_TEMPLATE
real ENTROPY_CONT::Entanglement(int m, data r, const Col<data> &series) {
  real s(0);
  Mat<data> v_s = Series2Mat(m, series);
  int n = v_s.n_cols;
  for (int i = 0; i < n; ++i) {
    real p(0);
    for (int j = 0; j < n; ++j) {
      if (abs(v_s.col(i) - v_s.col(j)).max() < r) {
        p++;
      }
    }
    s += log(p / n);
  }
  return s / n;
}

ENTROPY_CONT_TEMPLATE
real ENTROPY_CONT::ComputeEntropy(const Col<data> &seq, int cond_len) {
  Col<data> series = Preprocess(seq);
  return series.empty() ?
         real(NAN) :
         Entanglement(cond_len, 0.5, series) - Entanglement(cond_len + 1, 0.5, series);
}

ENTROPY_CONT_TEMPLATE
data ENTROPY_CONT::Predict(const Col<data> &seq, int cond_len) {
  Col<data> series = Preprocess(seq);
  Col<data> cond = series.tail(cond_len);

  data forecast(0), threshold(0.5);
  int num_cond(0), m(cond.n_elem);
  for (int i = 0; i < m; ++i) {
    if (std::isnan(cond(i))) {
      return data(NAN);
    }
  }

  for (int i = 0; i + m < series.n_elem; ++i) {
    Col<data> v(m);
    int j(0);
    for (; j < m; ++j) {
      if (std::isnan(series(i + j))) {
        i = i + j;
        break;
      }
      v(j) = series(i + j);
    }
    if (j == m && abs(v - cond).max() < threshold) {
      num_cond++;
      forecast += seq(i + m);
    }
  }

  return (num_cond == 0) ? data(NAN) : forecast / num_cond;
}

template class EntropyModelCont<float, float>;
template class EntropyModelCont<float, double>;

template class EntropyModelCont<double, float>;
template class EntropyModelCont<double, double>;


}

#undef ENTROPY_CONT
#undef ENTROPY_CONT_TEMPLATE
