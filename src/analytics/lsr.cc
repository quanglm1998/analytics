/*********************************************************
 * File       : lsr.cc
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 10 Mar 2017 10:58:52 AM SGT
 * Revision   : none
 *
 *********************************************************/

#include "analytics/lsr.h"

namespace analytics {

template<typename real>
void LeastSquareRegressor<real>::FitImpl(const Mat<real> &trains, const Mat<real> &target) {
  Mat<real> trains_ext = has_inter_ ?
                         join_rows(trains, arma::ones<Col<real>>(trains.n_rows)) :
                         trains;

  Mat<real> x;
  success_ = arma::solve(x, trains_ext, target, arma::solve_opts::fast);

  if (success_) {
    coeff_ = x.head_rows(trains.n_cols);
    inter_ = has_inter_ ? x.row(x.n_rows - 1) : Row<real>(arma::zeros<Row<real>>(x.n_cols));
  } else {
    ANALYTICS_LOG_ERROR("Fitting failed");
  }
}

template<typename real>
void LeastSquareRegressor<real>::PredictImpl(const Mat<real> &samples, Mat<real> &results) {
  if (!success_ || coeff_.empty())
    ANALYTICS_THROW("Coefficients are empty. Possible reason: fitting failed");

  int n = samples.n_rows;
  results = samples * coeff_;
  if (has_inter_)
    results += arma::ones<Col<real>>(n) * inter_;
}

template class LeastSquareRegressor<float>;
template class LeastSquareRegressor<double>;

}
