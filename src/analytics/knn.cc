/*********************************************************
 * File       : knn.cc
 * Author     : jhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Tue 11 Apr 2017 11:47:11 AM SGT
 * Revision   : none
 *
 *********************************************************/


#include "analytics/knn.h"
#include <vector>
#include <unordered_map>
using namespace std;


namespace analytics {

template <typename real>
void KNN<real>::FitImpl(const Mat<real> &x, const Mat<uword> &y) {
  typedef RTree<int, float, float> MyTree;
  numdis_ = x.n_cols;
  if (x.n_rows != y.n_rows) {
    ANALYTICS_THROW("the number of rows in x and y is not the same");
  }
  maxnodes_ = 10;
  minnodes_ = 5;
  ntree_.reset(new MyTree(numdis_, maxnodes_, minnodes_));
  vector<float> axs(numdis_);
  for (unsigned i = 0; i < x.n_rows; i++) {
    for (unsigned j = 0; j < x.n_cols; j++) {
      axs[j] = x(i, j);
    }
    /* cout << "insert " << i << endl; */
    ntree_->Insert(axs, axs, i);
  }
  resp_ = y;
}

template<typename real>
void KNN<real>::PredictImpl(const Mat<real> &samples, Mat<uword> &results) {
  if (numdis_ < 0) {
    ANALYTICS_THROW("model haven't been trained");
  }
  if (samples.n_cols != numdis_) {
    ANALYTICS_THROW("the feature size conflict");
  }
  results.set_size(samples.n_rows, resp_.n_cols);
  vector<float> axs(numdis_);
  unordered_map<uword, int> rspcnt;
  for (unsigned i = 0; i < samples.n_rows; i++) {
    for (unsigned j = 0; j < samples.n_cols; j++) {
      axs[j] = samples(i, j);
    }
    vector<int> ans = ntree_->FindTopK(axs, topk_);
    for (unsigned sty = 0; sty < resp_.n_cols; sty++) {
      rspcnt.clear();
      for (unsigned ansidx = 0; ansidx < ans.size(); ansidx++) {
        uword tmprsp = resp_(ans[ansidx], sty);
        rspcnt[tmprsp] += 1;
      }
      uword bestrsp = 0;
      int bestcnt = -1;
      for (auto &it : rspcnt) {
        if (it.second > bestcnt) {
          bestcnt = it.second;
          bestrsp = it.first;
        }
      }
      results(i, sty) = bestrsp;
    }
  }
}

  template class KNN<float>;
  template class KNN<double>;


}
