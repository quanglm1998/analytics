/*********************************************************
 * File       : mlp.cc
 * Author     : yzhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 13 Mar 2017 11:17:08 AM
 * Revision   : none
 *
 *********************************************************/
#include "analytics/mlp.h"

namespace analytics {


template<typename real>
void MLPRegressor<real>::FitImpl(const Mat<real> &x, const Mat<real> &y) {
  if (layer_sz_ <= 1 || layer_[0] != x.n_cols || layer_.back() != y.n_cols) {  // did not set layer size
    layer_ = std::vector<uword>{x.n_cols, x.n_cols, y.n_cols};
    layer_sz_ = layer_.size();
  }
  Init();
  assert(x.n_rows == y.n_rows);
  batch_sz_ = std::min(static_cast<int>(x.n_rows), batch_sz_);
  cat_sz_ = y.n_cols;
  for (int z = 0; z < epoch_; ++z) {
    int numbatches = x.n_rows / batch_sz_;
    for (int j = 2; j <= layer_sz_; ++j) {
      for (auto &it : mean_sigma2_[j - 1]) {
        it = 0;
      }
      for (auto &it : mean_mu_[j - 1]) {
        it = 0;
      }
    }
    for (int i = 0; i < numbatches; ++i) {
      Mat<real> batch_x = x.rows(i * batch_sz_, (i + 1) * batch_sz_ - 1);
      Mat<real> batch_y = y.rows(i * batch_sz_, (i + 1) * batch_sz_ - 1);
      BuildOne(batch_x, batch_y);
      for (int j = 2; j <= layer_sz_; ++j) {
        for (uword s = 0; s < mean_sigma2_[j - 1].size(); ++s) {
          mean_sigma2_[j - 1][s] = mean_sigma2_[j - 1][s] + sigma2_[j - 1][s];
          mean_mu_[j - 1][s] = mean_mu_[j - 1][s] + mu_[j - 1][s];
        }
      }
    }
    for (int j = 2; j <= layer_sz_; ++j) {
      for (uword s = 0; s < mean_sigma2_[j - 1].size(); ++s) {
        mean_sigma2_[j - 1][s] = mean_sigma2_[j - 1][s] / (numbatches - 1);
        mean_mu_[j - 1][s] = mean_mu_[j - 1][s] / numbatches;
      }
    }
    learning_rate_ = learning_rate_ * scaling_learning_rate_;
    if (learning_rate_ < min_learning_rate_) {
      learning_rate_ = min_learning_rate_;
    }
  }
}

template<typename real>
void MLPRegressor<real>::PredictImpl(const Mat<real> &x, Mat<real> &y) {
  y = arma::zeros<Mat<real>>(x.n_rows, cat_sz_);
  int local_batch_sz = std::min(static_cast<int>(x.n_rows), batch_sz_);
  for (uword i = 0; i < x.n_rows; i += local_batch_sz) {
    int start = i;
    int end = std::min(i + local_batch_sz - 1, static_cast<uword>(x.n_rows) - 1);
    Mat<real> batch_x = x.rows(start, end);
    Mat<real> batch_y = y.rows(start, end);
    Forward(batch_x, batch_y, true);  // 1 means testing = 1
    y.rows(start, end) = cache_a_[layer_sz_];
  }
}


template<typename real>
void MLPRegressor<real>::BuildOne(Mat<real> &batch_x, Mat<real> &batch_y) {
  Forward(batch_x, batch_y, false);
  Backward();
  ApplyGrads();
}


template<typename real>
void MLPRegressor<real>::Init() {
  assert(output_function_ == "linear" || output_function_ == "relu" || output_function_ == "sigm");
  assert(activation_function_ == "linear" || activation_function_ == "relu" || activation_function_ == "sigm");
  layer_sz_ = layer_.size();
  cat_sz_ = -1;
  assert(layer_sz_ > 0);
  relu_ra_ = arma::zeros<Row<real>>(layer_sz_ + 1);
  relu_da_ = arma::zeros<Row<real>>(layer_sz_ + 1);
  relu_va_ = arma::zeros<Row<real>>(layer_sz_ + 1);
  cache_a_.resize(layer_sz_ + 1);
  cache_w_.resize(layer_sz_ + 1);
  cache_dw_.resize(layer_sz_ + 1);
  cache_vw_.resize(layer_sz_ + 1);
  cache_rw_.resize(layer_sz_ + 1);
  cache_apre_.resize(layer_sz_ + 1);
  sigma2_.resize(layer_sz_ + 1);
  gamma_.resize(layer_sz_ + 1);
  cache_ahat_.resize(layer_sz_ + 1);
  mu_.resize(layer_sz_ + 1);
  beta_.resize(layer_sz_ + 1);
  vbn_.resize(layer_sz_ + 1);
  rbn_.resize(layer_sz_ + 1);
  mean_sigma2_.resize(layer_sz_ + 1);
  mean_mu_.resize(layer_sz_ + 1);
  dbn_.resize(layer_sz_ + 1);
  d_sigma_.resize(layer_sz_ + 1);

  Random gen;
  for (int i = 2; i <= layer_sz_; ++i) {
    relu_ra_[i - 1] = 0.2;  // prelu
    relu_va_[i - 1] = 0;
    int sz0 = layer_[i - 1];
    int sz1 = layer_[i - 2] + 1;
    cache_w_[i - 1] = arma::zeros<Mat<real>>(sz0, sz1);
    real mul = sqrt(2.0 / (real)(sz0 + sz1) / (real)(1 + relu_ra_[i - 1] * relu_ra_[i - 1]));
    for (int ii = 0; ii < sz0; ii++) {
      for (int jj = 0; jj < sz1; jj++) {
        real number = gen.randn();
        cache_w_[i - 1](ii, jj) = number * mul;
      }
    }
    cache_vw_[i - 1] = arma::zeros<Mat<real>>(sz0, sz1);
    cache_rw_[i - 1] = arma::zeros<Mat<real>>(sz0, sz1);
    beta_[i - 1] = arma::zeros<Row<real>>(sz0);
    gamma_[i - 1] = arma::ones<Row<real>>(sz0);
    sigma2_[i - 1] = arma::ones<Row<real>>(sz0);
    mu_[i - 1] = arma::zeros<Row<real>>(sz0);
    vbn_[i - 1] = arma::zeros<Row<real>>(sz0 * 2);
    rbn_[i - 1] = arma::zeros<Row<real>>(sz0 * 2);
    mean_sigma2_[i - 1] = arma::zeros<Row<real>>(sz0);
    mean_mu_[i - 1] = arma::zeros<Row<real>>(sz0);
  }
}

template<typename real>
void MLPRegressor<real>::ApplyGrads() {
  for (int i = 1; i <= (layer_sz_ - 1); ++i) {
    Mat<real> local_dW = cache_dw_[i];
    if (weight_penalty_l2_ > 0) local_dW += weight_penalty_l2_ * cache_w_[i];
    cache_rw_[i] = 0.9 * cache_rw_[i] + 0.1 * (local_dW % local_dW);
    for (uword ii = 0; ii < local_dW.n_rows; ++ii) {
      for (uword jj = 0; jj < local_dW.n_cols; ++jj) {
        local_dW(ii, jj) = learning_rate_ * local_dW(ii, jj) / (sqrt(cache_rw_[i](ii, jj)) + epsilon_);
      }
    }
    if (momentum_ > 0) {
      cache_vw_[i] = momentum_ * cache_vw_[i] + local_dW;
      local_dW = cache_vw_[i];
    }
    cache_w_[i] -= local_dW;
  }
  if (batch_normalization_) {
    for (int i = 1; i <= (layer_sz_ - 2); ++i) {
      for (uword z = 0; z < rbn_[i].size(); ++z) {
        rbn_[i][z] = 0.9 * rbn_[i][z] + 0.1 * dbn_[i][z] * dbn_[i][z];
      }
      for (uword z = 0; z < vbn_[i].size(); ++z) {
        vbn_[i][z] =  momentum_ * vbn_[i][z] + learning_rate_ * dbn_[i][z] / (sqrt(rbn_[i][z]) + epsilon_);
      }
      for (uword z = 0; z < gamma_[i].size(); ++z) {
        gamma_[i][z] -= vbn_[i][z];
        beta_[i][z] -= vbn_[i][z + gamma_[i].size()];
      }
    }
  }
  if (activation_function_ == "relu") {
    relu_da_ *= learning_rate_;
    relu_va_ = momentum_ * relu_va_ + relu_da_;
    relu_ra_ = relu_ra_ - relu_va_;
  }
}
template<typename real>
void MLPRegressor<real>::Backward() {
  int m = cache_a_[1].n_rows;
  std::vector<Mat<real>> d(layer_sz_ + 1);
  Mat<real> d_act;
  if (output_function_ == "sigm") {
    d[layer_sz_] = -(e_ % SigmDot(cache_a_[layer_sz_]));
  } else if (output_function_ == "softmax" || output_function_ == "linear" || output_function_ == "relu") {
    d[layer_sz_] = -e_;
  }

  for (int i = layer_sz_ - 1; i >= 2; i--) {
    if (i + 1 == layer_sz_) {
      d[i] = d[i + 1] * cache_w_[i];
    } else {
      d[i] = d[i + 1].submat(0, 1, d[i + 1].n_rows - 1, d[i + 1].n_cols- 1) * cache_w_[i];
    }
    //% Derivative of the activation function
    if (activation_function_ == "sigm") {
      d_act = SigmDot(cache_a_[i]);
    } else if (activation_function_ == "relu") {
      d_act = cache_a_[i];
      for (uword ii = 0; ii < d_act.n_rows; ++ii) {
        for (uword jj = 0; jj < d_act.n_cols; ++jj) {
          if (d_act(ii, jj) > 0) {
            d_act(ii, jj) = 1.0;
          } else {
            d_act(ii, jj) = relu_ra_[i - 1];
          }
        }
      }
      real sumtt = 0;
      for (uword ii = 0; ii < d[i].n_rows; ++ii) {
        for (uword jj = 0; jj < d[i].n_cols; ++jj) {
          if (cache_a_[i](ii, jj) < 0) sumtt += d[i](ii, jj) * cache_a_[i](ii, jj);
        }
      }
      relu_da_[i - 1] = sumtt / relu_ra_[i - 1] / cache_a_[i].n_rows;
    } else if (activation_function_ == "linear") {
      d_act = arma::ones<Mat<real>>(cache_a_[i].n_rows, cache_a_[i].n_cols);
    }
    d[i] = (d[i] % d_act);
    if (batch_normalization_) {
      Mat<real> subd = d[i].submat(0, 1, d[i].n_rows - 1, d[i].n_cols- 1);
      Mat<real> d_xhat = Bsxfun(subd, gamma_[i - 1], '*');
      x_mu_ = Bsxfun(cache_apre_[i], mu_[i - 1], '-');
      Row<real> inv_sqrt_sigma = arma::zeros<Row<real>>(sigma2_[i - 1].n_cols);
      for (uword z = 0; z < sigma2_[i - 1].size(); ++z) {
        inv_sqrt_sigma[z] = 1.0 / sqrt(sigma2_[i - 1][z] + epsilon_);
      }
      Row<real> d_sigma2 = arma::zeros<Row<real>>(inv_sqrt_sigma.n_cols);
      Row<real> tmpsum = SumMul(d_xhat, x_mu_);
      for (uword z = 0; z < d_sigma2.size(); ++z) {
        d_sigma2[z] = -0.5 * tmpsum[z] * inv_sqrt_sigma[z] * inv_sqrt_sigma[z] * inv_sqrt_sigma[z];
      }
      Mat<real> d_mu_0 = Bsxfun(d_xhat, inv_sqrt_sigma, '*');
      Row<real> d_mu = d_sigma2 % mean(x_mu_, 0);
      Row<real> sum_d_mu = sum(d_mu_0, 0);
      for (uword z = 0; z < d_mu.size(); ++z) d_mu[z] = -sum_d_mu[z] - 2.0 * d_mu[z];
      Row<real> d_gamma = mean((subd % cache_ahat_[i]), 0);
      Row<real> d_beta = mean(subd, 0);
      Mat<real> di1 = Bsxfun(d_xhat, inv_sqrt_sigma, '*');
      Mat<real> di2 = Bsxfun(x_mu_, d_sigma2, '*');
      for (uword jj = 0; jj < di1.n_cols; ++jj) {
        real tmp = d_mu[jj] / (real)m;
        for (uword ii = 0; ii < di1.n_rows; ++ii) {
          d[i](ii, 1 + jj) = di1(ii, jj) + di2(ii, jj) * 2.0 / (real)m + tmp;
        }
      }
      dbn_[i - 1] = d_gamma;
      dbn_[i - 1].insert_cols(d_gamma.n_cols, d_beta);
      d_sigma_[i - 1] = d_sigma2;
    }
  }
  for (int i = 1; i <= (layer_sz_ - 1); ++i) {
    if (i + 1 == layer_sz_) {
      cache_dw_[i] = d[i + 1].t() * cache_a_[i] / (real)d[i + 1].n_rows;
    } else {
      Mat<real> subd = d[i + 1].submat(0, 1, arma::size(d[i + 1].n_rows, d[i + 1].n_cols- 1));
      cache_dw_[i] = (subd.t() * cache_a_[i]) / (real)d[i + 1].n_rows;
    }
  }
}

template<typename real>
void MLPRegressor<real>::Forward(const Mat<real> &x, Mat<real> &batch_y, bool is_testing) {
  int n = layer_sz_;
  cache_a_[1] = Append(x);
  for (int i = 2; i <= n - 1; ++i) {
    if (batch_normalization_) {
      cache_apre_[i] = cache_a_[i - 1] * cache_w_[i - 1].t();
      mu_[i - 1] = mean(cache_apre_[i], 0);
      if (is_testing) {
        Row<real> norm_factor = arma::zeros<Row<real>> (sigma2_[i - 1].n_cols);
        for (uword z = 0; z < norm_factor.size(); ++z) {
          norm_factor[z] = gamma_[i - 1][z] / sqrt(mean_sigma2_[i - 1][z] + epsilon_);
        }
        cache_ahat_[i] = Bsxfun(cache_apre_[i], norm_factor, '*');
        cache_ahat_[i] = Bsxfun(cache_ahat_[i], beta_[i - 1] - (norm_factor % mean_mu_[i - 1]), '+');
      } else {
        x_mu_ = Bsxfun(cache_apre_[i], mu_[i - 1], '-');
        sigma2_[i - 1] = Mean2(x_mu_);
        Row<real> norm_factor = arma::zeros<Row<real>>(sigma2_[i - 1].n_cols);
        for (uword z = 0; z < norm_factor.size(); ++z) {
          norm_factor[z] = gamma_[i - 1][z] / sqrt(sigma2_[i - 1][z] + epsilon_);
        }
        cache_ahat_[i] = Bsxfun(cache_apre_[i], norm_factor, '*');
        cache_ahat_[i] = Bsxfun(cache_ahat_[i], beta_[i - 1] - (norm_factor % mu_[i - 1]), '+');
      }
    } else {
      cache_ahat_[i] = cache_a_[i - 1] * cache_w_[i - 1].t();
    }
    if (activation_function_ == "sigm") {
      cache_a_[i] = Sigm(cache_ahat_[i]);
    } else if (activation_function_ == "relu") {
      cache_a_[i] = cache_ahat_[i];
      for (uword ii = 0; ii < cache_a_[i].n_rows; ++ii) {
        for (uword jj = 0; jj < cache_a_[i].n_cols; ++jj) {
          if (cache_a_[i](ii, jj) < 0) cache_a_[i](ii, jj) *= relu_ra_[i - 1];
        }
      }
    } else if (activation_function_ == "linear") {
      cache_a_[i] = cache_ahat_[i];
    }
    cache_a_[i] = Append(cache_a_[i]);
  }
  if (output_function_ == "sigm") {
    cache_a_[n] = Sigm(cache_a_[n - 1] * cache_w_[n - 1].t());
  } else if (output_function_ == "linear") {
    cache_a_[n] = cache_a_[n - 1] * cache_w_[n - 1].t();
  } else if (output_function_ == "softmax") {
    // nn.a{n} = nn.a{n - 1} * nn.cache_w_{n - 1}';
    // nn.a{n} = exp(Bsxfun(@minus, nn.a{n}, max(nn.a{n},[],2)));
    // nn.a{n} = Bsxfun(@rdivide, nn.a{n}, sum(nn.a{n}, 2));
  } else if (output_function_ == "relu") {
    cache_a_[n] = Max(cache_a_[n - 1] * cache_w_[n - 1].t(), 0);
  }
  if (output_function_ == "sigm" || output_function_ == "linear" || output_function_ == "relu") {
    e_ = batch_y - cache_a_[n];
  } else if (output_function_ == "softmax") {
    // nn.e = y - nn.a{n};
    // nn.L = -sum(sum(y .* log(nn.a{n}+1e-10))) / m;
  }
}

template class MLPRegressor<float>;
template class MLPRegressor<double>;

}

