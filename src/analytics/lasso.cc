/*********************************************************
 * File       : lasso.cc
 * Author     : jhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 10 Mar 2017 03:29:45 PM SGT
 * Revision   : none
 *
 *********************************************************/

#include "analytics/lasso.h"


namespace analytics {

template <typename real>
void Lasso<real>::FitImpl(const Mat<real> &x, const Mat<real> &y) {
    if (lambda_ == 0) {
      ANALYTICS_LOG_WARN("UserWarning: Coordinate descent with lambda=0 may lead to unexpected results and "
               "is discouraged. lambda=0 is equivalent to an ordinary least square, solved by the "
               "LinearRegression. using lambda = 0 with lasso is not advised.Given this, you "
               "should use the linearRegression method.");
    }
    Mat<real> X_ext = has_inter_? join_rows(arma::ones<Mat<real>>(x.n_rows, 1), x) : x;
    Mat<real> XTY = X_ext.t() * y;
    Mat<real> XTX = X_ext.t() * X_ext;
    /* coeff_.ones(XTX.n_rows, XTY.n_cols); */
    Mat<real> tmpcoef = arma::ones<Mat<real>>(XTX.n_rows, XTY.n_cols);

    for (unsigned j = 0; j < XTY.n_cols; j++) {
      for (int iter_time = 0; iter_time < max_iter_; iter_time++) {
        real maxchange = 0;
        for (unsigned i = 0; i < XTX.n_rows; i++) {
          real precoef = tmpcoef(i, j);
          /* if XTX(i,i)==0, all ith feathre of the train data is all zero
           * now the derivative is just the derivative of |coeff_|
           * which is constant, the minimum value of the function is zero
           */
          if (XTX(i, i) == 0) {
            tmpcoef(i, j) = 0;
            continue;
          }
          real nsum = arma::as_scalar(XTX.row(i) * tmpcoef.col(j)) - XTX(i, i) * tmpcoef(i, j);
          if (has_inter_ && i == 0) {
            real tmp = (XTY(i, j)-nsum)/XTX(i, i);
            tmpcoef(i, j) = tmp;
          } else {
            real tmp = (XTY(i, j) - nsum- x.n_rows * lambda_) / XTX(i, i);
            if (tmp < 0) {
              tmp = std::min((XTY(i, j) - nsum + x.n_rows * lambda_) / XTX(i, i), (real)0);
            }
            tmpcoef(i, j) = tmp;
          }
          maxchange = std::max(std::abs(tmpcoef(i, j)-precoef)/std::abs(precoef), maxchange);
        }
        if (maxchange < tol_) break;
      }
    }
    success_ = true;
    coeff_ = tmpcoef.tail_rows(x.n_cols);
    inter_ = has_inter_ ? tmpcoef.row(0) : Row<real>(arma::zeros<Row<real>>(y.n_cols));
  }

template<typename real>
  void Lasso<real>::PredictImpl(const Mat<real> &samples, Mat<real> &results) {
    // make predictions and fill them into results
    int n = samples.n_rows;
    results = samples * coeff_;
    if (has_inter_) {
      results += arma::ones<Col<real>>(n) * inter_;
    }
  }

  template class Lasso<float>;
  template class Lasso<double>;


}
