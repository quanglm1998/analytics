/*********************************************************
 * File       : lts.cpp
 * Author     : xzhuo * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Sun 19 Aug 2018 04:08:16 PM +08
 * Revision   : none
 *
 *********************************************************/

#include "analytics/ltsr.h"
#include "analytics/lsr.h"

#define LTSR_TEMPLATE template<typename real>
#define LTSR LeastTrimmedSquareRegressor<real>

namespace analytics {

LTSR_TEMPLATE
void LTSR::AddRand(Mat<real> &X, Mat<real> &Y) {
  size_t p = X.n_cols;
  real tmp = det(X.t() * X);
  while (abs(tmp) < eps_) {
    Mat<real> rdx = arma::randn<Mat<real>>(1, p);
    Mat<real> rdy = arma::randn<Mat<real>>(1, 1);
    X = join_cols(X, rdx);
    Y = join_cols(Y, rdy);
    tmp = det(X.t() * X);
  }
}

LTSR_TEMPLATE
Mat<real> LTSR::Initialize(const Mat<real>& X, const Mat<real>& Y, size_t p, int seed) {
  Random rng(seed);
  size_t n(X.n_rows);
  Mat<real> X_train(p, p), Y_train(p, 1);

  Col<uword> order = rng.shuffled_seq(n);
  for (size_t i = 0; i < p; ++i) {
    X_train.row(i) = X.row(order(i));
    Y_train.row(i) = Y.row(order(i));
  }
  AddRand(X_train, Y_train);

  LeastSquareRegressor<real> ls;
  ls.set_intercept(false);
  ls.Fit(X_train, Y_train);
  Mat<real> params = ls.coefficients();
  return params;
}

LTSR_TEMPLATE
real LTSR::Cstep(const Mat<real>& X, const Mat<real>& Y, size_t h, Mat<real>& params) {
  Mat<real> data = join_rows(Y, X);
  Mat<real> resid = Y - X * params;
  Mat<uword> indices = sort_index(resid);
  data = data.rows(indices.rows(0, h - 1));

  Mat<real> Y_train = data.cols(0, 0);
  Mat<real> X_train = data.cols(1, data.n_cols - 1);
  AddRand(Y_train, X_train);

  LeastSquareRegressor<real> ls;
  ls.set_intercept(false);
  ls.Fit(X_train, Y_train);
  params = ls.coefficients();

  resid = Y - X * params;
  indices = sort_index(resid);
  Mat<real> tmp = resid.rows(indices.rows(0, h - 1));
  tmp = tmp.t() * tmp;

  real Q = tmp(0);
  return Q;
}

LTSR_TEMPLATE
Mat<real> LTSR::Regress(const Mat<real>& X, const Mat<real>& Y) {
  size_t p(X.n_cols), n(Y.n_rows), h((n + p + 1) / 2);
  Mat<real> Q_mat(1, path_num_), param_mat(p, path_num_);

  for (size_t i = 0; i < path_num_; ++i) {
    Mat<real> params = Initialize(X, Y, p, i);
    real Q = Cstep(X, Y, h, params);
    Q = Cstep(X, Y, h, params);
    Q_mat(0, i) = Q;
    param_mat.col(i) = params;
  }

  Mat<uword> indices = sort_index(Q_mat);
  param_mat = param_mat.cols(indices.rows(0, select_num_));
  Q_mat = Q_mat.cols(indices.rows(0, select_num_));
  for (size_t i = 0; i < select_num_; ++i) {
    Mat<real> param = param_mat.col(i);
    real Q0(100), Q1(Cstep(X, Y, h, param));
    while (abs(Q1 - Q0) > eps_) {
      Q0 = Q1;
      Q1 = Cstep(X, Y, h, param);
    }
    param_mat.col(i) = param;
    Q_mat(0, i) = Q1;
  }

  coeff_ = param_mat.col(Q_mat.cols(0, select_num_).index_min());
  return coeff_;
}

LTSR_TEMPLATE
void LTSR::FitImpl(const Mat<real> &trains, const Mat<real> &target) {
  Mat<real> trains_ext = has_inter_ ?
                         join_rows(trains, arma::ones<Col<real>>(trains.n_rows)) :
                         trains;

  Mat<real> x;
  if (rank(trains_ext, eps_) < trains_ext.n_cols) {
    LeastSquareRegressor<real> ls;
    ls.set_intercept(false);
    ls.Fit(trains_ext, target);
    x = ls.coefficients();
  } else {
    x = Regress(trains_ext, target);
  }

  success_ = true;
  coeff_ = x.head_rows(trains.n_cols);
  inter_ = has_inter_ ? x.row(x.n_rows - 1) : Row<real>(arma::zeros<Row<real>>(x.n_cols));
  resid_ = target - trains_ext * x;
}

LTSR_TEMPLATE
void LTSR::PredictImpl(const Mat<real> &samples, Mat<real> &results) {
  if (!success_ || coeff_.empty())
    ANALYTICS_THROW("Coefficients are empty. Possible reason: fitting failed");

  int n = samples.n_rows;
  results = samples * coeff_;
  if (has_inter_) {
    results += arma::ones<Col<real>>(n) * inter_;
  }
}

template class LeastTrimmedSquareRegressor<float>;
template class LeastTrimmedSquareRegressor<double>;

}

#undef LTSR
#undef LTSR_TEMPLATE
