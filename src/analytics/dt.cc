/*********************************************************
 * File       : dt.cc
 * Author     : yzhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 13 Mar 2017 11:17:08 AM
 * Revision   : none
 *
 *********************************************************/
#include "analytics/dt.h"

namespace analytics {


template <typename real>
DecisionTree<real>::DecisionTree(const Mat<real> &features,
             const Mat<real> &results,
             const DesisionTreeConfig &config,
             const int seed,
             const int use_weights,
             const int scoring_function) {
  // 0.prepare random sample as input
  Random gen(seed);
  if (config.compute_importances) {
    importances_.zeros(features.n_cols);
  }
  std::vector<int> chosen_groups(features.n_rows, 0);
  if (config.group_feature == -1 && config.groups.n_rows == 0) {
    for (uword i = 0; i < features.n_rows * config.bag_size; ++i) {
      chosen_groups[gen.randi(features.n_rows)]++;
    }
  } else if (config.group_feature != -1) {
    assert(config.group_feature >= 0 && config.group_feature < static_cast<int>(features.n_rows));
    std::unordered_map<real, int> groups_mp;
    int num_groups = 0;
    for (uword i = 0; i < features.n_rows; ++i) {
      if (!groups_mp.count(features(i, config.group_feature))) {
        groups_mp[features(i, config.group_feature)] = num_groups++;
      }
    }
    std::vector<int> group_size(num_groups, 0);
    for (uword i = 0; i < num_groups * config.bag_size; ++i) {
      group_size[gen.randi(num_groups)]++;
    }
    for (uword i = 0; i < features.n_rows; ++i) {
      chosen_groups[i] = group_size[groups_mp[features(i, config.group_feature)]];
    }
  } else {
    assert(config.groups.n_rows == features.n_rows);
    int num_groups = 0;
    for (int x : config.groups) num_groups = std::max(num_groups, x + 1);
    std::vector<int> group_size(num_groups, 0);
    for (uword i = 0; i < num_groups * config.bag_size; ++i) {
      group_size[gen.randi(num_groups)]++;
    }
    for (uword i = 0; i < features.n_rows; ++i) {
      chosen_groups[i] = group_size[config.groups(i)];
    }
  }
  int bag_size = 0;
  for (uword i = 0; i < features.n_rows; ++i) {
    if (chosen_groups[i]) bag_size++;
  }
  std::vector<int> bag(bag_size, 0);
  std::vector<int> weight(features.n_rows, 0);
  int pos = 0;
  for (uword i = 0; i < features.n_rows; ++i) {
    weight[i] = config.use_bootstrapping ? chosen_groups[i] : std::min(1, chosen_groups[i]);
    if (chosen_groups[i]) bag[pos++] = i;
  }

  // 1. start to build one tree
  TreeNode root;
  root.level = 0;
  root.left = 0;
  root.right = pos;
  nodes_.push_back(root);
  std::queue<int> stack;
  stack.push(0);
  while (!stack.empty()) {
    bool equal = true;
    int cur_node = stack.front();
    stack.pop();

    // 2. for each node, calculate its overall information first
    int b_left = nodes_[cur_node].left;
    int b_right = nodes_[cur_node].right;
    int b_size = b_right - b_left;
    int total_weight = 0;
    real total_sum = 0;
    real total2_sum = 0;
    for (int i = b_left; i < b_right; ++i) {
      if (use_weights) {
        total_sum += results(bag[i], 0) * weight[bag[i]];
        total_weight += weight[bag[i]];
        if (scoring_function == DesisionTreeConfig::MSE) {
          total2_sum += results(bag[i], 0) * results(bag[i], 0) * weight[bag[i]];
        }
      } else {
        total_sum += results(bag[i], 0);
        if (scoring_function == DesisionTreeConfig::MSE) total2_sum += results(bag[i], 0) * results(bag[i], 0);
      }
    }
    assert(b_size > 0);
    if (!use_weights) total_weight = b_size;
    for (int i = b_left + 1; i < b_right; ++i) {
      if (results(bag[i], 0) != results(bag[i - 1], 0)) {
        equal = false;
        break;
      }
    }
    if (equal ||
        b_size <= config.max_node_size ||
        nodes_[cur_node].level >= config.max_level ||
        (config.max_nodes && static_cast<int>(nodes_.size()) >= config.max_nodes)) {
      nodes_[cur_node].result = total_sum / total_weight;
      continue;
    }

    // 3. for current depth, only check 'random_features' random features
    int best_feature = -1;  // best feature id
    int best_left = 0;  // best left tree size
    int best_right = 0;  // best right tree size
    real best_value = 0;  // best split value
    real best_loss = 1e99;  // best loss
    int is_terminate = 0;  // terminate searching
    int feature_offset = std::min(nodes_[cur_node].level, static_cast<int>(config.random_features.n_rows) - 1);
    const int num_random_features = config.random_features[feature_offset];
    for (int i = 0; i < num_random_features; ++i) {  // each time only pick random_features for checking
      int feature_id = config.features_ignored + gen.randi(features.n_cols - config.features_ignored);
      real vlo, vhi;
      vlo = vhi = features(bag[b_left], feature_id);
      for (int j = b_left + 1; j < b_right; ++j) {
        vlo = std::min(vlo, features(bag[j], feature_id));
        vhi = std::max(vhi, features(bag[j], feature_id));
      }
      if (vlo == vhi) continue;
      int position_offset = std::min(nodes_[cur_node].level, static_cast<int>(config.random_positions.n_rows) - 1);
      const int random_positions = config.random_positions[position_offset];
      for (int j = 0; j < random_positions; ++j) {  // for a specific feature, only check random_positions split_value
        real split_value = features(bag[b_left + gen.randi(b_size)], feature_id);
        if (split_value == vlo) {
          j--;
          continue;
        }
        if (scoring_function == DesisionTreeConfig::MSE) {
          real sum_left = 0;
          real sum2_left = 0;
          int total_left = 0;
          for (int k = b_left; k < b_right; ++k) {
            int p = bag[k];
            if (features(p, feature_id) < split_value) {
              if (use_weights) {
                sum_left += results(p, 0) * weight[p];
                sum2_left += results(p, 0) * results(p, 0) * weight[p];
              } else {
                sum_left += results(p, 0);
                sum2_left += results(p, 0) * results(p, 0);
              }
              total_left++;
            }
          }
          real sum_right = total_sum - sum_left;
          real sum2_right = total2_sum - sum2_left;
          int total_right = b_size - total_left;
          if (total_left == 0 || total_right == 0) {
            continue;  // continue if no sample on the left or right
          }
          real loss = sum2_left - sum_left * sum_left / total_left + sum2_right - sum_right * sum_right / total_right;
          if (loss < best_loss) {  // update information if current loss is better
            best_loss = loss;
            best_value = split_value;
            best_feature = feature_id;
            best_left = total_left;
            best_right = total_right;
            if (loss == 0) {
              is_terminate = 1;
              break;
            }
          }
        } else {
          real sum_left = 0;
          int total_left = 0;
          int weight_left = 0;
          for (int k = b_left; k < b_right; ++k) {
            int p = bag[k];
            if (features(p, feature_id) < split_value) {
              if (use_weights) {
                sum_left += results(p, 0) * weight[p];
                weight_left += weight[p];
              } else {
                sum_left += results(p, 0);
              }
              total_left++;
            }
          }
          if (!use_weights) weight_left = total_left;
          real sum_right = total_sum - sum_left;
          int weight_right = total_weight - weight_left;
          int total_right = b_size - total_left;
          if (total_left == 0 || total_right == 0) {
            continue;
          }
          real mean_left = sum_left / weight_left;
          real mean_right = sum_right / weight_right;
          real loss = 0;
          if (scoring_function == DesisionTreeConfig::MCE) {
            for (int k = b_left; k < b_right; ++k) {
              int p = bag[k];
              if (features(p, feature_id) < split_value) {
                real diff = results(p, 0) - mean_left;
                loss += abs(diff)  * diff  * diff  * weight[p];
              } else {
                real diff = results(p, 0) - mean_right;
                loss += abs(diff) * diff * diff * weight[p];
              }
              if (loss > best_loss) break;  // OPTIONAL
            }
          } else if (scoring_function == DesisionTreeConfig::MAE) {
            for (int k = b_left; k < b_right; ++k) {
              int p = bag[k];
              if (features(p, feature_id) < split_value) {
                loss += abs(results(p, 0) - mean_left)  * weight[p];
              } else {
                loss += abs(results(p, 0) - mean_right) * weight[p];
              }
              if (loss > best_loss) break;  // OPTIONAL
            }
          } else if (scoring_function == DesisionTreeConfig::CUSTOM) {
            for (int k = b_left; k < b_right; ++k) {
              int p = bag[k];
              if (features(p, feature_id) < split_value) {
                loss += CustomLoss(results(p, 0) - mean_left)  * weight[p];
              } else {
                loss += CustomLoss(results(p, 0) - mean_right) * weight[p];
              }
              if (loss > best_loss) break;  // OPTIONAL
            }
          }
          if (loss < best_loss) {
            best_loss = loss;
            best_value = split_value;
            best_feature = feature_id;
            best_left = total_left;
            best_right = total_right;
            if (loss == 0) {
              is_terminate = 1;
              break;
            }
          }
        }
      }
      if (is_terminate) break;
    }

    if (best_left == 0 || best_right == 0) {
      nodes_[cur_node].result = total_sum / total_weight;
      continue;
    }
    if (config.compute_importances) {
      importances_(best_feature) += b_right - b_left;  // use the size of the subtree as a measurement of importances_
    }
    real next_value = -1e99;
    for (int i = b_left; i < b_right; ++i) {
      if (features(bag[i], best_feature) < best_value) {
        next_value = std::max(next_value, features(bag[i], best_feature));
      }
    }

    // 4. after finding the best split point, split the samples in that node to left and right
    TreeNode left;
    TreeNode right;
    left.level = right.level = nodes_[cur_node].level + 1;
    nodes_[cur_node].feature = best_feature;  // best feature id
    nodes_[cur_node].value = (best_value + next_value) / 2.0;  // best split value, (use median)
    if (!(nodes_[cur_node].value > next_value)) nodes_[cur_node].value = best_value;
    nodes_[cur_node].left = nodes_.size();  //  left son's index in the array nodes_[]
    nodes_[cur_node].right = nodes_.size() + 1;  //  right son's index in the array nodes_[]
    int b_middle = b_right;
    for (int i = b_left; i < b_middle; ++i) {
      if (features(bag[i], nodes_[cur_node].feature) >= nodes_[cur_node].value) {
        std::swap(bag[i], bag[--b_middle]);
        i--;
        continue;
      }
    }
    assert(best_left == b_middle - b_left);
    assert(best_right == b_right - b_middle);
    left.left = b_left;
    left.right = b_middle;
    right.left = b_middle;
    right.right = b_right;
    stack.push(nodes_.size());
    stack.push(nodes_.size() + 1);
    nodes_.push_back(left);  // push left son into the queue
    nodes_.push_back(right);  // push right son into the queue and continue bfs
  }
  nodes_.shrink_to_fit();
}

template <typename real>
real DecisionTree<real>::Predict(const Row<real> &features) const {  // predict for one sample
  const TreeNode *p_node = &nodes_[0];
  while (true) {
    if (p_node->feature < 0) {  // p_node->feature < 0 means it is the leaf of that tree
      return p_node->result;  // return the result
    }
    p_node = &nodes_[features(p_node->feature) < p_node->value ? p_node->left : p_node->right];  // go left or right?
  }
}


template<typename real>
void RandomForestRegressor<real>::FitImpl(const Mat<real> &trains, const Mat<real> &targets) {
  int num_trees = this->config_.num_trees;
  for (int i = 0; i < num_trees; ++i) {
    this->trees_.push_back(this->CreateTree(trains, targets, this->config_, this->RngNext()));
  }
  if (this->config_.compute_importances) {
    this->importances_ = arma::zeros<Col<real>>(trains.n_cols);
    for (DecisionTree<real> &tree : this->trees_) {
      this->importances_ += tree.importance();
    }
    real sum = arma::accu(this->importances_);
    this->importances_ /= sum;
  }
}

template<typename real>
void RandomForestRegressor<real>::PredictImpl(const Mat<real> &samples, Mat<real> &results) {
  results = Mat<real>(samples.n_rows, 1);
  for (uword j = 0; j < samples.n_rows; ++j) {
    results(j, 0) = 0;
    const Row<real> sample_row = samples.row(j);
    for (uword i = 0; i < this->trees_.size(); ++i) {
      results(j, 0) += this->trees_[i].Predict(sample_row);
    }
    results(j, 0) /= this->trees_.size();
  }
}

template class RandomForestRegressor<float>;
template class RandomForestRegressor<double>;

}

