/*********************************************************
 * File       : hmm.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Tue 18 Apr 2017 06:37:54 PM
 * Revision   : none
 *
 *********************************************************/

#include "analytics/hmm.h"

#define HMMB_TEMPLATE template<typename real, typename ObsType, typename HidType>
#define HMMB HiddenMarkovModelBase<real, ObsType, HidType>

namespace analytics {

HMMB_TEMPLATE
bool HMMB::CheckMatrix(const Mat<real> &matrix, int row, int col) {
  bool row_valid = (matrix.n_rows == row);
  bool col_valid = (matrix.n_cols == col);
  bool ele_valid = all(abs(sum(matrix, 1) - arma::ones<Col<real>>(matrix.n_rows)) < 1e-4);

  return row_valid && col_valid && ele_valid;
}

HMMB_TEMPLATE
void HMMB::ResetMatrix(Mat<real> &matrix, int row, int col) {
  matrix.resize(row, col);
  matrix.fill(1 / real(col));
}

}

#undef HMMB
#undef HMMB_TEMPLATE

//////////////////////////////////////////////////////////////////////


#define HMM_TEMPLATE template<typename real>
#define HMM HiddenMarkovModel<real>

namespace analytics {

HMM_TEMPLATE
void HMM::ForwardBackwardProb(const Mat<int> &observ) {
  int len = observ.n_rows;
  int num = observ.n_cols;

  alpha_.set_size(len, n_hid_state_, num);
  beta_.set_size(len, n_hid_state_, num);
  c_.set_size(len, num);

  Mat<real> alpha_k(len, n_hid_state_);
  Mat<real> beta_k(len, n_hid_state_);
  Col<real> c_k(len);

  for (int k = 0; k < num; ++k) {
    alpha_k.row(0) = p_ % B_.col(observ(0, k)).t();
    c_k(0) = sum(alpha_k.row(0));
    alpha_k.row(0) /= c_k(0);

    for (int t = 1; t < len; ++t) {
      int idx = observ(t, k);
      alpha_k.row(t) = (alpha_k.row(t - 1) * A_) % B_.col(idx).t();
      c_k(t) = sum(alpha_k.row(t));
      alpha_k.row(t) /= c_k(t);
    }

    beta_k.row(len - 1).fill(1);
    beta_k.row(len - 1) /= c_k(len - 1);

    for (int t = len - 2; t >= 0; --t) {
      int idx = observ(t + 1, k);
      beta_k.row(t) = (B_.col(idx).t() % beta_k.row(t + 1)) * A_.t();
      beta_k.row(t) /= c_k(t);
    }

    alpha_.slice(k) = alpha_k;
    beta_.slice(k) = beta_k;
    c_.col(k) = c_k;
  }
}

HMM_TEMPLATE
void HMM::ConditionalProb() {
  log_prob_ = sum(log(c_));
}

HMM_TEMPLATE
void HMM::InitParameters() {
  CheckEmissionMatrix();

  if (!this->CheckMatrix(A_, n_hid_state_, n_hid_state_))
    this->ResetMatrix(A_, n_hid_state_, n_hid_state_);

  if (!this->CheckMatrix(p_, 1, n_hid_state_))
    this->ResetMatrix(p_, 1, n_hid_state_);
}

HMM_TEMPLATE
void HMM::CheckEmissionMatrix() {
  n_hid_state_ = B_.n_rows;
  if (n_hid_state_ <= 0) ANALYTICS_THROW("Invalid row number of emission matrix");

  n_obs_state_ = B_.n_cols;
  if (n_obs_state_ <= 0) ANALYTICS_THROW("Invalid colume number of emission matrix");

  Col<real> row_sum_B = sum(B_, 1);

  bool sum_is_zero = any(abs(row_sum_B) < this->tol_);
  if (sum_is_zero) ANALYTICS_THROW("The sum of one or more rows in emission matrix is zero");

  bool sum_is_one = all(abs(row_sum_B - arma::ones<Col<real>>(B_.n_rows)) < this->tol_);
  if (!sum_is_one) {
    ANALYTICS_LOG_WARN("The sum of every row in emission matrix must be one");
    ANALYTICS_LOG_WARN("Rescaling ... ");

    B_ /= repmat(sum(B_, 1), 1, B_.n_cols);
  }
}

HMM_TEMPLATE
bool HMM::EstimateParameters(const Mat<int> &observ) {
  // Baum-Welch algorithm
  // https://en.wikipedia.org/wiki/Baum%E2%80%93Welch_algorithm

  bool success(true);
  int len = observ.n_rows;
  int num = observ.n_cols;

  Mat<real> A_1(A_), B_1(B_), p_1(p_);
  Mat<real> A_nmrtor(size(A_)), A_dnmtor(size(A_));
  Mat<real> B_nmrtor(size(B_)), B_dnmtor(size(B_));

  Cube<real> gamma(len, n_hid_state_, num);
  Cube<real> sum_gamma(1, n_hid_state_, num);

  ForwardBackwardProb(observ);
  ConditionalProb();

  for (int iter_time = 0; iter_time < this->max_iter_; ++iter_time) {
    Row<real> prev_log_prob = log_prob_;

    A_nmrtor.fill(0);
    A_dnmtor.fill(0);
    B_nmrtor.fill(0);
    B_dnmtor.fill(0);
    p_.fill(0);

    for (int k = 0; k < num; ++k) {
      const Mat<real> &alpha_k = alpha_.slice(k);
      const Mat<real> &beta_k = beta_.slice(k);
      const Col<real> &c_k = c_.col(k);

      Mat<real> &gamma_k = gamma.slice(k);
      Mat<real> &sum_gamma_k = sum_gamma.slice(k);

      gamma_k = alpha_k % beta_k % repmat(c_k, 1, n_hid_state_);
      sum_gamma_k = sum(gamma_k);


      // Update transition probability matrix
      for (int i = 0; i < n_hid_state_; ++i) {
        real scl = sum_gamma_k(0, i) - gamma_k(len - 1, i);
        for (int j = 0; j < n_hid_state_; ++j) {
          real xi(0);
          for (int t = 0; t < len - 1; ++t) {
            xi += alpha_k(t, i) * A_1(i, j) * B_1(j, observ(t + 1, k)) * beta_k(t + 1, j);
          }

          A_nmrtor(i, j) += xi;
          A_dnmtor(i, j) += scl;
        }
      }

      // Update emission probability matrix
      for (int i = 0; i < n_hid_state_; ++i) {
        for (int j = 0; j < n_obs_state_; ++j) {
          real xi(0);
          for (int t = 0; t < len; ++t) {
            if (observ(t, k) != j) continue;
            xi += gamma_k(t, i);
          }

          B_nmrtor(i, j) += xi;
          B_dnmtor(i, j) += sum_gamma_k(0, i);
        }
      }

      // Update initial distribution
      p_ += gamma_k.row(0) / sum(gamma_k.row(0));
    }

    A_ = A_nmrtor / A_dnmtor;
    B_ = B_nmrtor / B_dnmtor;
    p_ /= num;

    ForwardBackwardProb(observ);
    ConditionalProb();

    if (!A_.is_finite() || !B_.is_finite() || !p_.is_finite()) {
      success = false;
      break;
    }

    if (norm(prev_log_prob - log_prob_, 1) / (num * len) < this->tol_) break;

    A_1 = A_;
    B_1 = B_;
    p_1 = p_;
  }

  return success;
}

HMM_TEMPLATE
bool HMM::Fit(const Mat<int> &observ) {
  if (observ.n_rows <= 0) {
    ANALYTICS_THROW("Invalid length of observed sequence");
  }

  InitParameters();

  bool success = EstimateParameters(observ);

  return success;
}

HMM_TEMPLATE
bool HMM::EstimateHiddenSequence(const Mat<int> &observ, Mat<int> &hidden) {
  // Viterbi algorithm
  // https://en.wikipedia.org/wiki/Viterbi_algorithm

  bool success(true);
  int len = observ.n_rows;
  int num = observ.n_cols;

  hidden.set_size(len, num);

  Mat<real> log_A_ = log(A_);
  Mat<real> log_B_ = log(B_);
  Row<real> log_p_ = log(p_);

  for (int k = 0; k < num; ++k) {
    Mat<real> delta(len, n_hid_state_);
    Mat<arma::uword> phi(len, n_hid_state_);

    delta.row(0) = log_p_ + log_B_.col(observ(0, k)).t();
    phi.row(0).fill(0);

    for (int t = 1; t < len; ++t) {
      for (int i = 0; i < n_hid_state_; ++i) {
        Row<real> tmp = delta.row(t - 1) + A_.col(i).t();
        delta(t, i) = tmp.max() + B_(i, observ(t, k));
        phi(t, i) = tmp.index_max();
      }
    }

    hidden(len - 1, k) = delta.row(len - 1).index_max();
    for (int t = len - 2; t >= 0; --t) {
      hidden(t, k) = phi(t + 1, hidden(t + 1, k));
    }
  }

  if (!hidden.is_finite()) success = false;

  return success;
}

HMM_TEMPLATE
bool HMM::Decode(const Mat<int> &observ, Mat<int> &hidden, bool is_fit) {
  if (observ.n_rows <= 0)
    ANALYTICS_THROW("Invalid length of observed sequence");

  InitParameters();

  bool success(true);

  if (is_fit) {
    success = Fit(observ);
  }

  if (success) {
    success = EstimateHiddenSequence(observ, hidden);
  }

  return success;
}

HMM_TEMPLATE
bool HMM::Predict(const Mat<int> &observ, Mat<int> &hidden,
                  Cube<real> &out_observ, Cube<real> &out_hidden,
                  int step, bool is_decode, bool is_fit) {
  if (observ.n_rows <= 0)
    ANALYTICS_THROW("Invalid length of observed sequence");

  if (step <= 0)
    ANALYTICS_THROW("The prediction step must be positive integer");

  bool success(true);

  if (is_decode || (observ.n_rows != hidden.n_rows)) {
    success = Decode(observ, hidden, is_fit);
  } else if (is_fit) {
    success = Fit(observ);
  }

  if (success) {
    int len = observ.n_rows;
    int num = observ.n_cols;

    out_observ.resize(step, n_obs_state_, num);
    out_hidden.resize(step, n_hid_state_, num);

    for (int k = 0; k < num; ++k) {
      Mat<real> out_observ_k(step, n_obs_state_);
      Mat<real> out_hidden_k(step, n_hid_state_);

      out_hidden_k.row(0) = A_.row(hidden(len - 1, k));
      out_observ_k.row(0) = out_hidden_k.row(0) * B_;

      for (int i = 1; i < step; ++i) {
        out_hidden_k.row(i) = out_hidden_k.row(i - 1) * A_;
        out_observ_k.row(i) = out_hidden_k.row(i) * B_;
      }

      out_hidden.slice(k) = out_hidden_k;
      out_observ.slice(k) = out_observ_k;
    }
  }

  if (!out_hidden.is_finite()) success = false;
  if (!out_observ.is_finite()) success = false;

  return success;
}

template class HiddenMarkovModel<float>;
template class HiddenMarkovModel<double>;

}

#undef HMM
#undef HMM_TEMPLATE

//////////////////////////////////////////////////////////////////////


#define HMM_TEMPLATE_CONT template<typename real>
#define HMMC HiddenMarkovModelCont<real>
#define PI atan(1.0) * 4

namespace analytics {

HMM_TEMPLATE_CONT
void HMMC::GaussianProb(const Mat<real> &observ, const Mat<real> &mu, const Mat<real> &sigma_2) {
  int len = observ.n_rows;
  int num = observ.n_cols;
  B_.set_size(n_hid_state_, len, num);
  for (int k = 0; k < num; ++k) {
    for (int i = 0; i < n_hid_state_; ++i) {
      for (int j = 0; j < len; ++j) {
        B_(i, j, k) = 1 / sqrt(2 * PI * sigma_2(k, i)) *
                      exp(- pow(observ(j, k) - mu(k, i), 2) / (2 * sigma_2(k, i)));
      }
    }
  }
}


HMM_TEMPLATE_CONT
void HMMC::ForwardBackwardProb(const Mat<real> &observ) {
  GaussianProb(observ, mu_, sigma_2_);
  int len = observ.n_rows;
  int num = observ.n_cols;

  alpha_.set_size(len, n_hid_state_, num);
  beta_.set_size(len, n_hid_state_, num);
  c_.set_size(len, num);

  Mat<real> alpha_k(len, n_hid_state_);
  Mat<real> beta_k(len, n_hid_state_);
  Col<real> c_k(len);

  for (int k = 0; k < num; ++k) {
    alpha_k.row(0) = p_ % B_.slice(k).col(0).t();
    Row<real> alpha_r0 = alpha_k.row(0);
    c_k(0) = sum(alpha_r0);
    alpha_k.row(0) /= c_k(0);

    for (int t = 1; t < len; ++t) {
      int idx = t;
      alpha_k.row(t) = (alpha_k.row(t - 1) * A_) % B_.slice(k).col(idx).t();
      Row<real> alpha_rt = alpha_k.row(t);
      c_k(t) = sum(alpha_rt);
      alpha_k.row(t) /= c_k(t);
    }

    beta_k.row(len - 1).fill(1);

    for (int t = len - 2; t >= 0; --t) {
      int idx = t + 1;
      beta_k.row(t) = (B_.slice(k).col(idx).t() % beta_k.row(t + 1)) * A_.t();

      Row<real> beta_rt = beta_k.row(t);
      beta_k.row(t) /= sum(beta_rt);
    }

    alpha_.slice(k) = alpha_k;
    beta_.slice(k) = beta_k;
    c_.col(k) = c_k;
  }
}


HMM_TEMPLATE_CONT
void HMMC::ConditionalProb() {
  log_prob_ = sum(log(c_));  // 1 * num
}


HMM_TEMPLATE_CONT
void HMMC::InitParameters() {
  CheckEmissionMu();

  if (!CheckEmissionSigma(sigma_2_, n_obs_state_, n_hid_state_)) {
    sigma_2_.print("sigma_2_0:");
    ResetSigma(sigma_2_, n_obs_state_, n_hid_state_);
    sigma_2_.print("sigma_2_1:");
    std::cout << "Reset sigma_2_!" << std::endl;
  }

  if (!this->CheckMatrix(A_, n_hid_state_, n_hid_state_)) {
    A_.print("A_0: ");
    this->ResetMatrix(A_, n_hid_state_, n_hid_state_);
    A_.print("A_1: ");
    std::cout << "Reset A_!" << std::endl;}

  if (!this->CheckMatrix(p_, 1, n_hid_state_)) {
    p_.print("p_0: ");
    this->ResetMatrix(p_, 1, n_hid_state_);
    p_.print("p_1: ");
    std::cout << "Reset p_!" << std::endl;
  }
}

HMM_TEMPLATE_CONT
void HMMC::CheckEmissionMu() {
  n_hid_state_ = mu_.n_cols;
  if (n_hid_state_ <= 0) ANALYTICS_THROW("Invalid column number of emission mu");
  n_obs_state_ = mu_.n_rows;  // dimension of observation series (n_obs_state_ = obs.n_cols)
  if (n_obs_state_ <= 0) ANALYTICS_THROW("Invalid row number of emission mu");
}

HMM_TEMPLATE_CONT
bool HMMC::CheckEmissionSigma(const Mat<real> &matrix, int row, int col) {
  bool row_valid = (matrix.n_rows == row);
  bool col_valid = (matrix.n_cols == col);
  bool ele_valid = all(all(matrix > 1e-6));  // sigma^2 may be very small

  return row_valid && col_valid && ele_valid;
}

HMM_TEMPLATE_CONT
void HMMC::ResetMu(Mat<real> &matrix, int row, int col) {
  matrix.resize(row, col);
  matrix.fill(1 / real(col));
}

HMM_TEMPLATE_CONT
void HMMC::ResetSigma(Mat<real> &matrix, int row, int col) {
  matrix.resize(row, col);
  matrix.fill(1 / real(col));
}

HMM_TEMPLATE_CONT
bool HMMC::EstimateParameters(const Mat<real> &observ) {
  // Baum-Welch algorithm
  // https://en.wikipedia.org/wiki/Baum%E2%80%93Welch_algorithm

  bool success(true);
  int len = observ.n_rows;
  int num = observ.n_cols;

  Mat<real> A_1(A_), p_1(p_), mu_1(mu_), sigma_2_1(sigma_2_);
  Mat<real> A_nmrtor(size(A_)), A_dnmtor(size(A_));
  Mat<real> mu_nmrtor(size(mu_)), mu_dnmtor(size(mu_));
  Mat<real> sigma_2_nmrtor(size(sigma_2_)), sigma_2_dnmtor(size(sigma_2_));

  Cube<real> gamma(len, n_hid_state_, num);
  Cube<real> sum_gamma(1, n_hid_state_, num);

  ForwardBackwardProb(observ);
  Cube<real> B_1(B_);

  ConditionalProb();

  for (int iter_time = 0; iter_time < this->max_iter_; ++iter_time) {
    Row<real> prev_log_prob = log_prob_;
    A_nmrtor.fill(0);
    A_dnmtor.fill(0);
    p_.fill(0);
    mu_nmrtor.fill(0);
    sigma_2_nmrtor.fill(0);
    mu_dnmtor.fill(0);
    sigma_2_dnmtor.fill(0);

    for (int k = 0; k < num; ++k) {
      const Mat<real> &alpha_k = alpha_.slice(k);
      const Mat<real> &beta_k = beta_.slice(k);
      const Col<real> &c_k = c_.col(k);

      Mat<real> &gamma_k = gamma.slice(k);
      Mat<real> &sum_gamma_k = sum_gamma.slice(k);

      gamma_k = alpha_k % beta_k;
      for (int iter_len = 0; iter_len < len; iter_len++) {
        Row<real> temp_gamma = gamma_k.row(iter_len);
        gamma_k.row(iter_len) = gamma_k.row(iter_len) /
                                sum(temp_gamma);
      }

      sum_gamma_k = sum(gamma_k);

      // Update transition probability matrix

      Cube <real> xi_ele(n_hid_state_, n_hid_state_, len-1);
      xi_ele.zeros();
      Mat <real> sum_xi(len-1, 1, arma::fill::zeros);
      for (int t = 0; t < len-1; t ++) {
        for (int i = 0; i < n_hid_state_; ++i) {
          for (int j = 0; j < n_hid_state_; ++j) {
            xi_ele(i, j, t) = alpha_k(t, i) * A_1(i, j) * B_1(j, t + 1, k) * beta_k(t + 1, j);
          }
        }
        Mat<real> temp_xi = xi_ele.slice(t);
        for (int i = 0; i < n_hid_state_; ++i) {
          Row <real> xi_row = temp_xi.row(i);
          sum_xi[t] += sum(xi_row);
        }
        for (int i = 0; i < n_hid_state_; ++i) {
          for (int j = 0; j < n_hid_state_; ++j) {
            xi_ele(i, j, t) = xi_ele(i, j, t) / sum_xi[t];
          }
        }
      }

      for (int i = 0; i < n_hid_state_; ++i) {
        real scl = sum_gamma_k(0, i) - gamma_k(len - 1, i);
        for (int j = 0; j < n_hid_state_; ++j) {
          real xi(0);
          for (int t = 0; t < len - 1; ++t) {
            if (!std::isfinite(xi_ele(i, j, t))) continue;
            xi += xi_ele(i, j, t);
          }

          if (!std::isfinite(xi) || !std::isfinite(scl)) continue;
          A_nmrtor(i, j) += xi;
          A_dnmtor(i, j) += scl;
        }
      }

      // Update parameters
      for (int i = 0; i < n_hid_state_; ++i) {
        real scl = sum_gamma_k(0, i) - gamma_k(len - 1, i);
        real xi(0);
        for (int t = 0; t < len - 1; ++t) {
          real temp_value = gamma_k(t, i) * observ(t, k);
          if (!std::isfinite(temp_value)) continue;
          xi += temp_value;
        }
        if (!std::isfinite(xi) || !std::isfinite(scl)) continue;
        mu_nmrtor(k, i) += xi;
        mu_dnmtor(k, i) += scl;
      }

      for (int i = 0; i < n_hid_state_; ++i) {
        real xi(0);
        for (int t = 0; t < len; ++t) {
          real temp_value = gamma_k(t, i) * (observ(t, k) - mu_1(k, i)) *
                            (observ(t, k) - mu_1(k, i));
          if (!std::isfinite(temp_value)) continue;
          xi += temp_value;
        }

        if (!std::isfinite(xi) || !std::isfinite(sum_gamma_k(0, i))) continue;
        sigma_2_nmrtor(k, i) += xi;
        sigma_2_dnmtor(k, i) += sum_gamma_k(0, i);
      }

      // Update initial distribution
      Row<real> temp_p = gamma_k.row(0) / sum(gamma_k.row(0));
      if (!temp_p.is_finite()) continue;
      p_ += temp_p;
      mu_.row(k) = mu_nmrtor.row(k) / mu_dnmtor.row(k);
      sigma_2_.row(k) = sigma_2_nmrtor.row(k) / sigma_2_dnmtor.row(k);
    }

    A_ = A_nmrtor / A_dnmtor;
    p_ /= num;

    ForwardBackwardProb(observ);
    ConditionalProb();

    A_.print("A_");

    if (!A_.is_finite() || !mu_.is_finite() || !sigma_2_.is_finite() || !p_.is_finite()) {
      success = false;
      break;
    }

    if (norm(prev_log_prob - log_prob_, 1) / (num * len) < this->tol_) break;

    A_1 = A_;
    B_1 = B_;
    p_1 = p_;
    mu_1 = mu_;
    sigma_2_1 = sigma_2_;
  }

  log_likelihood_.set_size(observ.n_rows, observ.n_cols);
  log_likelihood_ = log(c_);
  return success;
}

HMM_TEMPLATE_CONT
bool HMMC::Fit(const Mat<real> &observ) {
  if (observ.n_rows <= 0) {
    ANALYTICS_THROW("Invalid length of observed sequence");
  }

  InitParameters();

  if (observ.n_cols != n_obs_state_) {
    ANALYTICS_THROW("Dimension of observed sequence should match the row number of matrix mu!");
  }

  bool success = EstimateParameters(observ);
  return success;
}

HMM_TEMPLATE_CONT
bool HMMC::EstimateHiddenSequence(const Mat<real> &observ, Mat<int> &hidden) {
  // Viterbi algorithm
  // https://en.wikipedia.org/wiki/Viterbi_algorithm

  bool success(true);
  int len = observ.n_rows;
  int num = observ.n_cols;

  hidden.set_size(len, num);

  GaussianProb(observ, mu_, sigma_2_);  // compute emission probability
  Mat<real> log_A_ = log(A_);
  Cube<real> log_B_ = log(B_);
  Row<real> log_p_ = log(p_);

  for (int k = 0; k < num; ++k) {
    Mat<real> delta(len, n_hid_state_);
    Mat<arma::uword> phi(len, n_hid_state_);

    delta.row(0) = log_p_ + log_B_.slice(k).col(0).t();
    phi.row(0).fill(0);

    for (int t = 1; t < len; ++t) {
      for (int i = 0; i < n_hid_state_; ++i) {
        Row<real> tmp = delta.row(t - 1) + log_A_.col(i).t();
        delta(t, i) = tmp.max() + log_B_(i, t, k);
        phi(t, i) = tmp.index_max();
      }
    }

    hidden(len - 1, k) = delta.row(len - 1).index_max();
    for (int t = len - 2; t >= 0; --t) {
      hidden(t, k) = phi(t + 1, hidden(t + 1, k));
    }
  }

  if (!hidden.is_finite()) success = false;

  return success;
}

HMM_TEMPLATE_CONT
bool HMMC::Decode(const Mat<real> &observ, Mat<int> &hidden, bool is_fit) {
  if (observ.n_rows <= 0)
    ANALYTICS_THROW("Invalid length of observed sequence");

  InitParameters();

  bool success(true);

  if (is_fit) {
    success = Fit(observ);
  }

  if (success) {
    success = EstimateHiddenSequence(observ, hidden);
  }

  return success;
}

HMM_TEMPLATE_CONT
bool HMMC::Predict(const Mat<real> &observ, Mat<int> &hidden,
                   Cube<real> &out_observ, Cube<real> &out_hidden,
                   int step, bool is_decode, bool is_fit) {
  if (observ.n_rows <= 0)
    ANALYTICS_THROW("Invalid length of observed sequence");

  if (step <= 0)
    ANALYTICS_THROW("The prediction step must be positive integer");

  bool success(true);

  if (is_decode || (observ.n_rows != hidden.n_rows)) {
    success = Decode(observ, hidden, is_fit);
  } else if (is_fit) {
    success = Fit(observ);
  }

  if (success) {
    int len = observ.n_rows;
    int num = observ.n_cols;

    int n_obs_ = 1;  // conditional expectation
    out_observ.resize(step, n_obs_, num);
    out_hidden.resize(step, n_hid_state_, num);

    for (int k = 0; k < num; ++k) {
      Mat<real> out_observ_k(step, n_obs_);
      Mat<real> out_hidden_k(step, n_hid_state_);

      out_hidden_k.row(0) = A_.row(hidden(len - 1, k));
      out_observ_k.row(0) = out_hidden_k.row(0) * mu_.row(k).t();

      for (int i = 1; i < step; ++i) {
        out_hidden_k.row(i) = out_hidden_k.row(i - 1) * A_;
        out_observ_k.row(i) = out_hidden_k.row(i) * mu_.row(k).t();
      }

      out_hidden.slice(k) = out_hidden_k;
      out_observ.slice(k) = out_observ_k;
    }
  }

  if (!out_hidden.is_finite()) success = false;
  if (!out_observ.is_finite()) success = false;

  return success;
}

template class HiddenMarkovModelCont<float>;
template class HiddenMarkovModelCont<double>;

}

#undef PI
#undef HMMC
#undef HMM_TEMPLATE_CONT
