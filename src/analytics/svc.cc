/*********************************************************
 * File       : svc.cc
 * Author     : yliu
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 14 Jul 2017 05:19:08 PM SGT
 * Revision   : none
 *
 *********************************************************/

#include "analytics/svc.h"

namespace analytics {


template<typename real>
void SVMClassifier<real>::FitImpl(const Mat<real> &train, const Mat<uword> &targets) {
  const Mat<real> &x = train;
  const Mat<uword> &y0 = targets;

  l_ = x.n_rows;
  w_size_ = x.n_cols;
  w_ = arma::zeros<Col<real>>(w_size_);
  Col<real> x2_ = arma::zeros<Col<real>>(l_);
  for (int i = 0; i < l_; i++) {
    for (int j = 0; j < w_size_; j++) {
      x2_(i) += x(i, j) * x(i, j);
    }
  }

  int i, s, iter = 0;
  real C, d, G;

  Col<real> QD(l_);
  Col<int> index(l_);
  Col<real> alpha(l_);
  Col<int> y(l_);
  int active_size = l_;

  // PG: projected gradient, for shrinking and stopping
  real PG;
  real PGmax_old = std::numeric_limits<real>::max();
  real PGmin_old = -std::numeric_limits<real>::max();
  real PGmax_new, PGmin_new;

  // default solver type: L2R_L2LOSS_SVC_DUAL
  real diag[3] = {real(0.5/Cn_), real(0), real(0.5/Cp_)};
  real upper_bound[3] = {std::numeric_limits<real>::max(), real(0), std::numeric_limits<real>::max()};
  // l2: L2R_L2LOSS_SVC_DUAL, l1: L2R_L1LOSS_SVC_DUAL
  if (type_ == "l1") {
    diag[0] = 0;
    diag[2] = 0;
    upper_bound[0] = Cn_;
    upper_bound[2] = Cp_;
  }

  for (i = 0; i < l_; i++) {
    if (y0(i, 0) > 0) {
      y(i) = +1;
    } else {
      y(i) = -1;
    }
  }

  // Initial alpha can be set here. Note that
  // 0 <= alpha[i] <= upper_bound[GETI(i)]
  alpha.zeros();
  w_.zeros();
  for (i = 0; i < l_; i++) {
    QD(i) = diag[y(i) + 1];
    QD(i) += x2_(i);
    for (int z = 0; z < w_size_; z++) {
      w_(z) += y(i) * alpha(i) * x(i, z);
    }
    index(i) = i;
  }

  while (iter < num_iterations_) {
    PGmax_new = -std::numeric_limits<real>::max();
    PGmin_new = std::numeric_limits<real>::max();

    for (i = 0; i < active_size; i++) {
      int j = i + RngNext(active_size - i);
      std::swap(index(i), index(j));
    }

    for (s = 0; s < active_size; s++) {
      i = index(s);
      const signed yi = y(i);
      real dot = 0.0;
      dot = arma::dot(w_, x.row(i));
      G = yi * dot - 1;
      C = upper_bound[y(i) + 1];
      G += alpha(i) * diag[y(i) + 1];
      PG = 0;
      if (alpha(i) == 0) {
        if (G > PGmax_old) {
          active_size--;
          std::swap(index(s), index(active_size));
          s--;
          continue;
        } else if (G < 0) {
          PG = G;
        }
      } else if (alpha(i) == C) {
        if (G < PGmin_old) {
          active_size--;
          std::swap(index(s), index(active_size));
          s--;
          continue;
        } else if (G > 0) {
          PG = G;
        }
      } else {
        PG = G;
      }
      PGmax_new = std::max(PGmax_new, PG);
      PGmin_new = std::min(PGmin_new, PG);

      if (fabs(PG) > 1.0e-12) {
        real alpha_old = alpha(i);
        alpha(i) = std::min(std::max(alpha(i) - G / QD(i), real(0.0)), C);
        d = (alpha(i) - alpha_old) * yi;
        w_ += d * x.row(i).t();
      }
    }

    iter++;
    if (PGmax_new - PGmin_new <= eps_) {
      if (active_size == l_) {
        break;
      } else {
        active_size = l_;
        PGmax_old = std::numeric_limits<real>::max();
        PGmin_old = -std::numeric_limits<real>::max();
        continue;
      }
    }
    PGmax_old = PGmax_new;
    PGmin_old = PGmin_new;
    if (PGmax_old <= 0) PGmax_old = std::numeric_limits<real>::max();
    if (PGmin_old >= 0) PGmin_old = -std::numeric_limits<real>::max();
  }

  // calculate objective value
  /*
  real v = 0;
  int nSV = 0;

  for (i = 0; i < w_size_; i++) {
    v += w_(i) * w_(i);
  }
  for (i = 0; i < l_; i++) {
    v += alpha(i) * (alpha(i) * diag[y(i) + 1] - 2);
    if (alpha(i) > 0) nSV++;
  }
  */

  // return w_;
}

template<typename real>
void SVMClassifier<real>::PredictImpl(const Mat<real> &samples, Mat<uword> &results) {
  if (samples.n_rows <= 0) {
    results.clear();
    return;
  }
  if (w_.n_rows == 0) {  // forget to train?
    return;
  }
  assert(samples.n_cols == w_.n_rows);
  Mat<real> y = samples * w_;
  results = arma::zeros<Mat<uword>>(samples.n_rows);
  for (unsigned i = 0; i < samples.n_rows; i++) {
    if (y(i, 0) > 0) {
      results(i, 0) = 1;
    } else {
      results(i, 0) = 0;
    }
  }
}

template class SVMClassifier<float>;
template class SVMClassifier<double>;

}
