/*********************************************************
 * File       : rr.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 10 Mar 2017 11:40:47 AM
 * Revision   : none
 *
 *********************************************************/

#include "analytics/rr.h"

namespace analytics {

template<typename real>
void RidgeRegressor<real>::FitImpl(const Mat<real> &trains, const Mat<real> &target) {
  Mat<real> trains_ext = has_inter_ ?
                         join_rows(trains, arma::ones<Col<real>>(trains.n_rows)) :
                         trains;

  int n = trains_ext.n_cols;
  Mat<real> penalty = lambda_ * arma::eye<Mat<real>>(n, n);
  if (has_inter_) penalty(n - 1, n - 1) = 0;

  Mat<real> A = trains_ext.t() * trains_ext + penalty;
  Mat<real> b = trains_ext.t() * target;

  Mat<real> x;
  success_ = arma::solve(x, A, b, arma::solve_opts::fast);

  if (success_) {
    coeff_ = x.head_rows(trains.n_cols);
    inter_ = has_inter_ ? x.row(x.n_rows - 1) : Row<real>(arma::zeros<Row<real>>(x.n_cols));
  } else {
    ANALYTICS_LOG_ERROR("Fitting failed");
  }
}

template<typename real>
void RidgeRegressor<real>::PredictImpl(const Mat<real> &samples, Mat<real> &results) {
  if (!success_ || coeff_.empty())
    ANALYTICS_THROW("Coefficients are empty. Possible reason: fitting failed");

  int n = samples.n_rows;
  results = samples * coeff_;
  if (has_inter_)
    results += arma::ones<Col<real>>(n) * inter_;
}

template class RidgeRegressor<float>;
template class RidgeRegressor<double>;

}
