/*********************************************************
 * File       : logging.cc
 * Author     : myang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Fri 24 Feb 2017 05:19:24 PM SGT
 * Revision   : none
 *
 *********************************************************/

#include <log4cxx/basicconfigurator.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/patternlayout.h>
#include <log4cxx/consoleappender.h>

#include "analytics/logging.h"

using namespace std;
using namespace log4cxx;
using namespace log4cxx::helpers;

LoggerPtr analytics::logger(Logger::getLogger("analytics"));

void analytics::ConfigLogger(
    const string &name, const string &config_file, const string &level) {
  if (!name.empty()) {
    logger = Logger::getLogger(name.c_str());
  }
  if (!config_file.empty()) {
    PropertyConfigurator::configure(config_file.c_str());
  } else {
    BasicConfigurator::configure();;
    LoggerPtr root = log4cxx::Logger::getRootLogger();
    root->setLevel(log4cxx::Level::toLevel(level));
    root->removeAllAppenders();
    const LogString TTCC_CONVERSION_PATTERN(LOG4CXX_STR("[%p] %d [%t] %l [%c{1}] %m"));
    LayoutPtr layout(new PatternLayout(TTCC_CONVERSION_PATTERN));
    AppenderPtr appender(new ConsoleAppender(layout));
    root->addAppender(appender);
  }
}
