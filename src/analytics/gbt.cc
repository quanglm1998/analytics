/*********************************************************
 * File       : dt.cc
 * Author     : yzhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 13 Mar 2017 11:17:08 AM
 * Revision   : none
 *
 *********************************************************/
#include "analytics/gbt.h"
#include <unordered_set>

namespace analytics {

template<typename real>
void GradientBoostingTreeRegressor<real>::BuildOne(const Mat<real> &trains) {
  real _lambda = lambda_ * sz_;
  for (int i = 0; i < sz_; ++i) {
    g_[i] = (-y_[i] + curpred_[i]);
    h_[i] = 1.0;
  }
  const real INF = 1e30;
  int all_node_sz = (1 << (max_level_ + 1)) - 1;
  for (int i = 0; i < all_node_sz; ++i) {
    global_best_gain_[i] = -INF;
    global_best_dim_[i] = -1;
    global_best_v_[i] = 0;
    global_best_lw_[i] = 0;
    global_best_rw_[i] = 0;
  }

  for (int fid = 0; fid < dim_; ++fid) {
    int data_offset = fid * sz_;
    int node_offset = fid * all_node_sz;
    for (int j = 0; j < sz_; ++j) {
      dim_nid_[data_offset + j] = 0;
      is_last_[data_offset + j] = 0;
    }
    int dsz = dim_sz_[fid];  // value number in that dim_
    for (int j = 0; j < all_node_sz; ++j) {
      global_all_g_[j + node_offset] = 0;
      global_all_h_[j + node_offset] = 0;
    }
    for (int j = 0; j < dsz; ++j) {  // for all the value in that dim_
      int start = pos_value_offset_[data_offset + j];
      int end = (j == dsz - 1)?sz_:pos_value_offset_[data_offset + j + 1];
      std::set<int> nidset;
      for (int z = end - 1; z >= start; z--) {
        int nid = dim_nid_[data_offset + pos_[data_offset + z]];
        global_all_g_[nid + node_offset] += g_[pos_[data_offset + z]];
        global_all_h_[nid + node_offset] += h_[pos_[data_offset + z]];
        if (nidset.count(nid) == 0) {
          nidset.insert(nid);
          if (j != dsz -  1) {
            // is last one?, if j == ds - 1, will not check the last value
            is_last_[data_offset + pos_[data_offset + z]] = 1;
          }
        }
      }
    }
  }

  for (int i = 0; i < max_level_; ++i) {
    std::vector<int> md(dim_, 0);
    while (true) {
      int find = 0;
      for (int j = 0; j < dim_; ++j) {
        if (gen.randu() < fp_) {
          find = 1;
          md[j] = 1;
        } else {
          md[j] = 0;
        }
      }
      if (find) break;
    }
    // 1. find best_gain_, best_d_, best_v_, best_lw_, best_rw_ for each dim_
    int level_offset = (1 << i) - 1;
    int level_node_sz = 1 << i;

    for (int fid = 0; fid < dim_; ++fid) {
      int data_offset = fid * sz_;
      int node_offset = fid * all_node_sz;
      for (int j = level_offset; j < level_offset + level_node_sz; ++j) {
        int local_offset = node_offset + j;
        best_gain_[local_offset] = -INF;
        best_d_[local_offset] = -1;
        best_v_[local_offset] = 0;
        best_lw_[local_offset] = 0;
        best_rw_[local_offset] = 0;
      }
      for (int j = 0; j < level_node_sz; ++j) {
        global_tmp_g_[node_offset + j + level_offset] = 0;
        global_tmp_h_[node_offset + j + level_offset] = 0;
      }
      for (int j = 0; j < sz_; ++j) {
        int index = pos_[data_offset + j];
        int rid = dim_nid_[data_offset + index];

        int local_node_offset = node_offset + rid;
        int local_data_offset = data_offset + index;

        global_tmp_h_[local_node_offset] += h_[index];
        global_tmp_g_[local_node_offset] += g_[index];
        if (is_last_[local_data_offset]) {  // if it is a last index
          real g_l = global_tmp_g_[local_node_offset];
          real h_l = global_tmp_h_[local_node_offset];
          real g_all = global_all_g_[local_node_offset];
          real h_all = global_all_h_[local_node_offset];
          real g_r = g_all - g_l;
          real h_r = h_all - h_l;

          real tmp_gain = g_l * g_l / (h_l + _lambda) +
              g_r * g_r / (h_r + _lambda) -
              g_all * g_all / (h_all + _lambda) - gamma_;

          real ratio = std::min(h_l / global_all_h_[local_node_offset],
                                1 - h_l / global_all_h_[local_node_offset]);

          if (i <= fp_level_ && !md[fid]) continue;
          if (tmp_gain > best_gain_[local_node_offset] && ratio > min_ratio_) {
            best_gain_[local_node_offset] = tmp_gain;
            best_d_[local_node_offset] = fid;  // feature id
            best_v_[local_node_offset] = x_[local_data_offset];
            best_lw_[local_node_offset] = -(g_l) / (h_l + _lambda);
            best_rw_[local_node_offset] = -(g_all - g_l) / (h_all - h_l + _lambda);
          }
        }
      }
    }

    // 2. find best_gain_, best_d_, best_v_, best_lw_, best_rw_ for each dim_
    int start = level_offset;
    int end = level_offset + level_node_sz;
    for (int z = start; z < end; ++z) {
      int global_offset = z;
      global_best_gain_[global_offset] = best_gain_[z];
      int best_dim = 0;
      for (int d = 0; d < dim_; ++d) {
        if (best_gain_[d * all_node_sz + z] > global_best_gain_[global_offset]) {
          global_best_gain_[global_offset] = best_gain_[d * all_node_sz + z];
          best_dim = d;
        }
      }
      global_best_v_[global_offset] = best_v_[best_dim * all_node_sz + z];
      global_best_lw_[global_offset] = best_lw_[best_dim * all_node_sz + z];
      global_best_rw_[global_offset] = best_rw_[best_dim * all_node_sz + z];
      global_best_dim_[global_offset] = best_dim;
    }
    // 3. reprocess the data
    for (int fid = 0; fid < dim_; ++fid) {
      int data_offset = fid * sz_;
      for (int j = 0; j < sz_; ++j) {
        int index = pos_[data_offset + j];
        int nid = dim_nid_[data_offset + index];
        int _offset = nid;
        int _best_dim = global_best_dim_[_offset];
        real _best_split = global_best_v_[_offset];
        if (x_[_best_dim * sz_ + index] <= _best_split) {  // go left
          dim_nid_[data_offset + index] = (nid * 2) + 1;
        } else {  // go right
          dim_nid_[data_offset + index] = (nid * 2) + 2;
        }
      }
    }
    for (int fid = 0; fid < dim_; ++fid) {
      int data_offset = fid * sz_;
      int node_offset = fid * all_node_sz;
      int dsz = dim_sz_[fid];  // value number in that dim_
      for (int j = 0; j < dsz; ++j) {  // for all the value in that dim_
        int start = pos_value_offset_[data_offset + j];
        int end = (j == dsz - 1) ? sz_:pos_value_offset_[data_offset + j + 1];
        std::unordered_set<int> nidset;
        for (int z = end - 1; z >= start; z--) {
          int local_pos_z = pos_[data_offset + z];
          int nid = dim_nid_[data_offset + local_pos_z];
          global_all_g_[node_offset + nid] += g_[local_pos_z];
          global_all_h_[node_offset + nid] += h_[local_pos_z];
          if (nidset.count(nid) == 0) {
            nidset.insert(nid);
            // is last one?, if j == ds - 1, will not check the last value
            if (j != dsz -  1) is_last_[data_offset + local_pos_z] = 1;
          }
        }
      }
    }
  }
  std::vector<real> stop_value(all_node_sz, 0);
  for (int i = 0; i < max_level_; ++i) {
    int level_offset = (1 << i) - 1;
    int level_node_sz = 1 << i;
    for (int p = level_offset; p < level_offset + level_node_sz; ++p) {
      stop_value[p * 2 + 1] = global_best_lw_[p];
      stop_value[p * 2 + 2] = global_best_rw_[p];
    }
  }
  forest_.emplace_back();
  auto &trees = *forest_.rbegin();
  trees.emplace_back(global_best_dim_[0], global_best_v_[0], 0);
  for (uword i = 0; i < trees.size(); ++i) {
    int nid = trees[i].left_id;
    int lid = nid * 2 + 1;
    int rid = lid + 1;
    if (global_best_dim_[nid] == -1 || global_best_gain_[nid] < 0.0) {  // if end
      trees[i].vle = stop_value[nid];
      trees[i].pid = -1;  // end early
      trees[i].left_id = -1;
    } else {
      trees[i].left_id = trees.size();
      trees.emplace_back(SNode(global_best_dim_[lid], global_best_v_[lid], lid));
      trees.emplace_back(SNode(global_best_dim_[rid], global_best_v_[rid], rid));
    }
  }

  if (null_zero_) {
    int now_node = 0;
    while (trees[now_node].pid >= 0) {  // not end
      if (0.0 <= trees[now_node].vle) {
        now_node = trees[now_node].left_id;  // go left
      } else {
        now_node = trees[now_node].left_id + 1;  // go right
      }
    }
    trees[now_node].vle = 0;  // force 0.0
  }
  for (int i = 0; i < sz_; ++i) {
    const Row<real> sample_row = trains.row(i);
    real py = PredictOne(trees, sample_row);
    curpred_[i] += py * shrinkage_;
  }
  if (verbose_) {
    real error = 0;
    real esum = 0;
    real emean = 0;
    for (int i = 0; i < sz_; ++i) emean += y_[i];
    emean /= sz_;
    for (int i = 0; i < sz_; ++i) {
      assert(!std::isnan(y_[i]) && !std::isinf(y_[i]));
      error += (y_[i] - curpred_[i]) * (y_[i] - curpred_[i]);
      esum += (y_[i] - emean) * (y_[i] - emean);
    }
    real current_error = (esum <= 0) ? 0 : error / esum;
    ANALYTICS_LOG_INFO("current error:" << current_error);
  }
}

template<typename real>
void GradientBoostingTreeRegressor<real>::FitImpl(const Mat<real> &trains, const Mat<real> &targets) {
  sz_ = trains.n_rows;
  dim_ = trains.n_cols;
  x_.assign(sz_ * dim_, 0);
  y_.assign(sz_, 0);
  pos_.assign(sz_ * dim_, 0);
  dim_sz_.assign(dim_, 0);
  pos_value_.assign(sz_ * dim_, 0);
  pos_value_offset_.assign(sz_ * dim_, 0);
  for (int i = 0; i < sz_; ++i) {
    for (int j = 0; j < dim_; ++j) {
      x_[j * sz_ + i] = trains(i, j);
    }
  }
  for (int j = 0; j < dim_; ++j) {
    std::map<real, std::vector<int> > mp;
    for (int i = 0; i < sz_; ++i) {
      mp[trains(i, j)].push_back(i);
    }
    std::vector<std::pair<real, std::vector<int> > > pr(mp.begin(), mp.end());
    //  do not need to sort std::sort(pr.begin(), pr.end(), std::less<std::pair<real, std::vector<int> > >());
    int off = 0;
    int vl_off = 0;
    for (uword s = 0; s < pr.size(); ++s) {
      pos_value_[j * sz_ + vl_off] = pr[s].first;
      pos_value_offset_[j * sz_ + (vl_off++)] = off;
      for (uword t = 0; t < pr[s].second.size(); ++t) {
        pos_[j * sz_ + off] = pr[s].second[t];
        off++;
      }
    }
    assert(off == sz_);
    dim_sz_[j] = pr.size();
  }
  for (int i = 0; i < sz_; ++i) {
    y_[i] = targets[i];
    y_mean_ += targets[i];
  }
  y_mean_ /= sz_;
  for (int i = 0; i < sz_; ++i) {
    y_[i] -= y_mean_;
  }
  int batch_node_sz = (1 << (max_level_ + 1)) - 1;
  global_best_gain_.assign(batch_node_sz, 0);
  global_best_v_.assign(batch_node_sz, 0);
  global_best_lw_.assign(batch_node_sz, 0);
  global_best_rw_.assign(batch_node_sz, 0);
  global_best_dim_.assign(batch_node_sz, 0);
  curpred_.assign(sz_, 0);
  g_.assign(sz_, 0);
  h_.assign(sz_, 0);
  dim_nid_.assign(sz_ * dim_, 0);
  is_last_.assign(sz_ * dim_, 0);
  best_gain_.assign(dim_ * batch_node_sz, 0);
  best_v_.assign(dim_ * batch_node_sz, 0);
  best_d_.assign(dim_ * batch_node_sz, 0);
  best_lw_.assign(dim_ * batch_node_sz, 0);
  best_rw_.assign(dim_ * batch_node_sz, 0);
  best_sz_.assign(dim_ * batch_node_sz, 0);
  global_all_g_.assign(dim_ * batch_node_sz, 0);
  global_all_h_.assign(dim_ * batch_node_sz, 0);
  global_tmp_g_.assign(dim_ * batch_node_sz, 0);
  global_tmp_h_.assign(dim_ * batch_node_sz, 0);

  for (int i = 0; i < num_trees_; ++i) {
    BuildOne(trains);
  }
}

template class GradientBoostingTreeRegressor<float>;
template class GradientBoostingTreeRegressor<double>;

}

