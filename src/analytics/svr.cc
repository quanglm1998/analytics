/*********************************************************
 * File       : dt.cc
 * Author     : yzhan
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 13 Mar 2017 11:17:08 AM
 * Revision   : none
 *
 *********************************************************/
#include "analytics/svr.h"

namespace analytics {

template<typename real>
void SVR<real>::FitImpl(const Mat<real> &train, const Mat<real> &targets) {
  iter_ = 0;
  m_ = train.n_cols;  // m_ = train[0].size();
  w0_.zeros(m_, 1);  // weight
  l_ = train.n_rows;
  assert(m_ > 0 && l_ > 0);
  active_size_ = l_;
  index_.zeros(l_);
  w_.zeros(m_);
  gmax_old_ = HUGE_VAL;
  gnorm1_init_ = 0;
  beta_.zeros(l_);
  qd_.zeros(l_);
  lambda_ = 0.5 / C_;
  upper_bound_ = HUGE_VAL;
  if (type_ == "l1") {
    lambda_ = 0;
    upper_bound_ = C_;
  }
  // Initial beta_ can be set here. Note that
  // -upper_bound_ <= beta_[i] <= upper_bound_
  for (int i = 0; i < l_; i++) {
    qd_(i) = 0;
    for (int j = 0; j < m_; ++j) {
      real val = train(i, j);
      qd_(i) += val * val;
      w_(j) += beta_(i) * val;
    }
    index_[i] = i;
  }
  for (int i = 0; i < num_iterations_; ++i) {
    BuildOne(train, targets);
  }
}

template<typename real>
void SVR<real>::PredictImpl(const Mat<real> &samples, Mat<real> &results) {
  if (samples.n_rows <= 0) {
    results.clear();
    return;
  }
  if (w0_.n_rows == 0) {  // forget to train?
    return;
  }
  assert(samples.n_cols == w0_.n_rows);
  results = samples * w0_;
}

template<typename real>
void SVR<real>::BuildOne(const Mat<real> &train, const Mat<real> &targets) {
  while (true) {
    gmax_new_ = 0;
    gnorm1_new_ = 0;
    for (int i = 0; i < active_size_; i++) {
      int j = i + RngNext(active_size_ - i);
      std::swap(index_[i], index_[j]);
    }
    for (int s = 0; s < active_size_; s++) {
      int i = index_[s];
      G_ = -targets[i] + lambda_ * beta_(i);
      H_ = qd_(i) + lambda_;
      G_ += Mat<real>(train.row(i) * w_)(0, 0);
      real Gp = G_ + p_;
      real Gn = G_ - p_;
      real violation = 0;
      if (beta_(i) == 0) {
        if (Gp < 0) {
          violation = -Gp;
        } else if (Gn > 0) {
          violation = Gn;
        } else if (Gp > gmax_old_ && Gn < -gmax_old_) {
          active_size_--;
          std::swap(index_[s], index_[active_size_]);
          s--;
          continue;
        }
      } else if (beta_(i) >= upper_bound_) {
        if (Gp > 0) {
          violation = Gp;
        } else if (Gp < -gmax_old_) {
          active_size_--;
          std::swap(index_[s], index_[active_size_]);
          s--;
          continue;
        }
      } else if (beta_(i) <= -upper_bound_) {
        if (Gn < 0) {
          violation = -Gn;
        } else if (Gn > gmax_old_) {
          active_size_--;
          std::swap(index_[s], index_[active_size_]);
          s--;
          continue;
        }
      } else if (beta_(i) > 0) {
        violation = fabs(Gp);
      } else {
        violation = fabs(Gn);
      }
      gmax_new_ = std::max(gmax_new_, violation);
      gnorm1_new_ += violation;
      if (Gp < H_ * beta_(i)) {
        d_ = -Gp / H_;
      } else if (Gn > H_ * beta_(i)) {
        d_ = -Gn / H_;
      } else {
        d_ = -beta_(i);
      }
      if (fabs(d_) < 1.0e-12) continue;
      real beta_old = beta_(i);
      beta_(i) = std::min(std::max(beta_(i) + d_, -upper_bound_), upper_bound_);
      d_ = beta_(i) - beta_old;
      if (d_ != 0) {
        w_ += d_ * train.row(i).t();
      }
    }
    if (iter_ == 0) gnorm1_init_ = gnorm1_new_;
    iter_++;
    if (gnorm1_new_ <= eps_ * gnorm1_init_) {
      if (active_size_ == l_) {
        break;
      } else {
        active_size_ = l_;
        gmax_old_ = HUGE_VAL;
        continue;
      }
    }
    gmax_old_ = gmax_new_;
    break;
  }
  real v = arma::accu(w_ % w_);
  int nSV = 0;
  v = 0.5 * v;
  for (int i = 0; i < l_; i++) {
    v += p_ * fabs(beta_(i)) - targets[i] * beta_(i) + 0.5 * lambda_ * beta_(i) * beta_(i);
    if (beta_(i) != 0) nSV++;
  }
  w0_ = w_;
  iter_++;
}

template class SVR<float>;
template class SVR<double>;

}

