/*********************************************************
 * File       : emd.cc
 * Author     : ftang
 * Copyright  : Dynamic Technology Lab Pte Ltd
 * Description: TODO
 * Created    : Mon 06 Aug 2018 11:02:44 AM SGT
 * Revision   : none
 *
 *********************************************************/

#include "analytics/emd.h"

#define EMD_TEMPLATE template<typename real>
#define EMD EmpiricalModeDecomposition<real>

namespace analytics {

EMD_TEMPLATE
void EMD::Init(int order, int max_iter, int locality) {
  data_.Init(order);
  max_iter_ = max_iter;
  order_ = order;
  locality_ = locality;
}

EMD_TEMPLATE
void EMD::Decompose(const Row<real>& signal) {
  int size = signal.n_elem;
  data_.Resize(size);

  data_.imfs.row(0) = signal;
  data_.residue = signal;

  for (int i = 0; i < order_ - 1; ++i) {
    Row<real> cur_imf = data_.imfs.row(i);
    for (int j = 0; j < max_iter_; ++j) {
      MakeExtrema(cur_imf);
      if (data_.min_size < 4 || data_.max_size < 4) {
        break;
      }
      Interpolate(cur_imf, data_.min_curve, data_.min_point, data_.min_size);
      Interpolate(cur_imf, data_.max_curve, data_.max_point, data_.max_size);
      UpdateImf(cur_imf);
    }
    MakeResidue(cur_imf);
    data_.imfs.row(i) = cur_imf;
    data_.imfs.row(i + 1) = data_.residue;
  }
}

// Currently, extrema within (locality) of the boundaries are not allowed.
// A better algorithm might be to collect all the extrema, and then assume
// that extrema near the boundaries are valid, working toward the center.

EMD_TEMPLATE
void EMD::MakeExtrema(const Row<real>& cur_imf) {
  int last_min(0), last_max(0);
  data_.min_size = 0;
  data_.max_size = 0;
  for (int i = 1; i < data_.size - 1; ++i) {
    if (cur_imf(i - 1) < cur_imf(i)) {
      if (cur_imf(i) > cur_imf(i + 1) && (i - last_max) > locality_) {
        data_.max_point(data_.max_size++) = i;
        last_max = i;
      }
    } else {
      if (cur_imf(i) < cur_imf(i + 1) && (i - last_min) > locality_) {
        data_.min_point(data_.min_size++) = i;
        last_min = i;
      }
    }
  }
}

EMD_TEMPLATE
void EMD::Interpolate(const Row<real>& in,
                      Row<real>& out,
                      const Row<int>& points,
                      int points_size) {
  for (int i = -1; i < points_size; ++i) {
    int i0 = points(MirrorIndex(i - 1, points_size));
    int i1 = points(MirrorIndex(i, points_size));
    int i2 = points(MirrorIndex(i + 1, points_size));
    int i3 = points(MirrorIndex(i + 2, points_size));

    real y0 = in(i0);
    real y1 = in(i1);
    real y2 = in(i2);
    real y3 = in(i3);

    real a0 = y3 - y2 - y0 + y1;
    real a1 = y0 - y1 - a0;
    real a2 = y2 - y0;
    real a3 = y1;

    int start, end;

    // left boundary
    if (i == -1) {
      start = 0;
      i1 = -i1;
    } else {
      start = i1;
    }

    // right boundary
    if (i == points_size - 1) {
      end = data_.size;
      i2 = data_.size + data_.size - i2;
    } else {
      end = i2;
    }

    real mu_scale = 1.f / (i2 - i1);
    for (int j = start; j < end; ++j) {
      real mu = (j - i1) * mu_scale;
      out(j) = ((a0 * mu + a1) * mu + a2) * mu + a3;
    }
  }
}

EMD_TEMPLATE
void EMD::UpdateImf(Row<real>& imf) {
  imf -= (data_.min_curve + data_.max_curve) * .5f;
}

EMD_TEMPLATE
void EMD::MakeResidue(const Row<real>& cur) {
  data_.residue -= cur;
}

EMD_TEMPLATE
const Mat<real>& EMD::Imf() const {
  return data_.imfs;
}

EMD_TEMPLATE
Row<real> EMD::Imf(int i) const {
  return data_.imfs.row(i);
}


template class EmpiricalModeDecomposition<float>;
template class EmpiricalModeDecomposition<double>;

}

#undef EMD
#undef EMD_TEMPLATE
