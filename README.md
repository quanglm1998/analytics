DTL Analytics Library
=================

User Guide
----------
See [usage.md](docs/usage.md).


Style Guide
-----------
See [style.md](docs/style.md).


Logging
------
```cpp
#include "analytics/logging.h"

void Foo() {
  ANALYTICS_LOG_INFO("hello world: " << 12321);
}
```


Regression/Classification Interface
-----------
```cpp
SVMRegressor<fmat> svm;
svm.set_C(1.0);
svm.set_kernel(SVM::SIGMOID);

svm.Fit(train, targets);

fmat results = svm.Predict(samples);
```

How to Implement a Regressor/Classifier
------------
You regressor/classifier should inherit the base `Regressor`/`Classifier` class and
implement `FitImpl` and `PredictImpl`.

NOTE: You usually don't need to check the validity of the inputs - the base classes will
do that for you.

```cpp
template <typename real = float>
class SampleRegressor : public Regressor<real> {
 protected:
  void FitImpl(const Mat<real> &, const Mat<real> &) override {
    // train your model here
  }

  void PredictImpl(const Mat<real> &samples, Mat<real> &results) const override {
    // make predictions and fill them into results
  }
};

// classifier implemention is very similar
template <typename real = float>
class SampleClassifier : public Classifier<real> {
 protected:
  void FitImpl(const Mat<real> &, const Mat<uword> &) override {
  }

  void PredictImpl(const Mat<real> &samples, Mat<uword> &results) const override {
  }
};
```

Exception Handling
------------
When a runtime error occurs, you should:
1. Use `ANALYTICS_LOG_ERROR` to output details of the error.
2. Throw an `analytics::AnalyticsError` (defined in [`analytics/common.h`](include/analytics/common.h)).

```cpp
// You can use the ANALYTICS_THROW macro to throw the error
ANALYTICS_THROW("this is " << 1 << " error");
```

Writing Tests
----------
- Create a `.cc` file in the `test` folder.

- Write your tests in the file:

```cpp
// include the test framework
#include "catch.hpp"

// and other headers
#include "analytics/ml.h"

TEST_CASE("svm test 1", "[svm]") {
  // write your test here

  // use REQUIRE for assertion
  REQUIRE(1 + 1 == 2);

  // say you have a regressor r, use TEST_REGRESSOR to run some predefined tests on this regressor
  SimpleRegressor r;
  TEST_REGRESSOR(r);

  // similarly for classifier we have TEST_CLASSIFIER
  SimpleClassifier c;
  TEST_CLASSIFIER(c);
}

TEST_CASE("svm test 2", "[svm]") {
  // another test case
}
```

- Add a line in `CMakeLists.txt` to include your file:

```diff
add_executable(unit_test
  test/main.cc
+  test/sample.cc
)
```

- Run `bin/run_test.sh`

### Debugging Tests
By default the test framework will catch all exceptions so it can run all tests even if some of them fail.
But consequently we cannot easily use `gdb` to trace exceptions since the program does not break.

To force the program to break on exception:
1. Compile the tests using `bin/build.sh -a`. You can also specify the `-d` option to get a debug build.
2. Use `gdb` to execute `build/unit_test`.

### References
- [Sample test](test/sample.cc)
- [Catch tutorial](https://github.com/philsquared/Catch/blob/master/docs/tutorial.md)


Armadillo
---------
Fixed at `7.500.2`.


Installation and Usage in SIMA
---------
See [install.md](docs/install.md).
